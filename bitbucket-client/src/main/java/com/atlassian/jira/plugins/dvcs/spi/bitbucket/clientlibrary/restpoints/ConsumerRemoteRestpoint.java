package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints;

import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConsumer;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.RemoteRequestor;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.gson.reflect.TypeToken;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.ClientUtils.fromJson;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * REST service to retrieve information about the OAUTH consumers in Bitbucket.
 */
public class ConsumerRemoteRestpoint {
    public static final String BB_NEW_CONSUMER_URL_TEMPLATE = "/account/user/%s/oauth-consumers/new";
    public static final String BB_CONSUMERS_ENDPOINT_TEMPLATE = "/users/%s/consumers";
    public static final String BB_CONSUMER_ENDPOINT_TEMPLATE = "/users/%s/consumers/%s";
    public static final Type BB_CONSUMER_TYPE = new TypeToken<List<BitbucketConsumer>>() {
    }.getType();
    private static final Logger log = LoggerFactory.getLogger(ConsumerRemoteRestpoint.class);
    private static final Set<String> FULL_ACCESS_SCOPES = ImmutableSet.of(
            "webhook",
            "account:write",
            "team:write",
            "repository:admin",
            "pullrequest:write",
            "issue:write",
            "wiki",
            "snippet:write");
    private final RemoteRequestor requestor;

    public ConsumerRemoteRestpoint(@Nonnull final RemoteRequestor remoteRequestor) {
        this.requestor = Preconditions.checkNotNull(remoteRequestor);
    }

    /**
     * Get the list of registered OAuth consumers for the given account
     *
     * @param owner The account to retrieve consumers for
     * @return The list of consumers
     */
    public List<BitbucketConsumer> getConsumers(@Nonnull final String owner) {
        checkArgument(isNotBlank(owner), "Mandatory owner parameter is undefined!");

        final String uri = URLPathFormatter.format(BB_CONSUMERS_ENDPOINT_TEMPLATE, owner);

        return requestor.get(uri, null, response -> fromJson(response.getResponse(), BB_CONSUMER_TYPE));
    }

    /**
     * DELETE the registered OAuth consumer with the given ID
     *
     * @param owner         The account to delete the consumer from
     * @param applicationId The ID of the consumer to delete
     * @return <code>true</code> if the delete succeeded
     */
    public boolean deleteConsumer(@Nonnull final String owner, final String applicationId) {
        checkArgument(isNotBlank(owner));
        checkArgument(isNotBlank(applicationId));

        final String uri = URLPathFormatter.format(BB_CONSUMER_ENDPOINT_TEMPLATE, owner, applicationId);

        return requestor.delete(uri, null, response -> {
            if (response.getHttpStatusCode() > 204) {
                log.info("Error while removing OAuth consumer '{}'. Got response '{}'",
                        applicationId, response.getHttpStatusCode());
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        });
    }

    /**
     * Create a new OAuth consumer with an auto-generated name and all permissions.
     *
     * @param owner The account to create the consumer in
     * @return The details of the created consumer
     */
    public BitbucketConsumer createConsumer(@Nonnull final String owner) {
        final String name = "Test_OAuth_" + System.currentTimeMillis();
        final String description = "Test OAuth Description [" + name + "]";
        return createConsumer(owner, name, description, null);
    }

    /**
     * Create a new OAuth consumer with full access permissions
     *
     * @param owner       The account to create the consumer in
     * @param name        The name for the consumer
     * @param description The OAuth consumer description
     * @param callbackUrl The OAuth callback URL for this consumer
     * @return The details of the created consumer
     */
    public BitbucketConsumer createConsumer(@Nonnull final String owner,
                                            @Nonnull final String name, @Nullable final String description,
                                            @Nullable final String callbackUrl) {
        return createConsumer(owner, name, description, callbackUrl, FULL_ACCESS_SCOPES);
    }

    /**
     * Create a new OAuth consumer with the provided details
     *
     * @param owner       The account to create the consumer in
     * @param name        The name for the consumer
     * @param description The OAuth consumer description
     * @param callbackUrl The OAuth callback URL for this consumer
     * @param scopes      The set of scopes to associate with this consumer
     * @return The details of the created consumer
     */
    public BitbucketConsumer createConsumer(@Nonnull final String owner,
                                            @Nonnull final String name, @Nullable final String description,
                                            @Nullable final String callbackUrl, @Nullable Set<String> scopes) {
        checkArgument(isNotBlank(owner));
        checkArgument(isNotBlank(name));

        final String uri = URLPathFormatter.format(BitbucketDetails.getHostUrl() + BB_NEW_CONSUMER_URL_TEMPLATE, owner);

        final List<NameValuePair> body = new ArrayList<>();
        body.add(new BasicNameValuePair("name", name));
        if (isNotBlank(description)) {
            body.add(new BasicNameValuePair("description", description));
        }
        if (isNotBlank(callbackUrl)) {
            body.add(new BasicNameValuePair("callback_url", callbackUrl));
        }
        if (scopes != null) {
            body.addAll(scopes.stream().map(scope -> new BasicNameValuePair("scope", scope)).collect(toList()));
        }

        return requestor.post(uri, URLEncodedUtils.format(body, "UTF-8"), ContentType.APPLICATION_FORM_URLENCODED, response -> {
            if (response.getHttpStatusCode() >= 400) {
                log.info("Create consumer '{}' request failed - HTTP {}", name, response.getHttpStatusCode());
                throw new IllegalArgumentException("Unable to create consumer with name " + name);
            }

            Optional<BitbucketConsumer> result = getConsumers(owner)
                    .stream()
                    .filter(c -> c.getName().equals(name))
                    .findFirst();

            if (!result.isPresent()) {
                throw new IllegalStateException("Consumer not found: " + name);
            }
            return result.get();
        });
    }
}
