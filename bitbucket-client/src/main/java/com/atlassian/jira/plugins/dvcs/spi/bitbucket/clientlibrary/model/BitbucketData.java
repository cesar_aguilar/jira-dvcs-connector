package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.concurrent.Immutable;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Immutable
public final class BitbucketData implements Serializable {
    private static final long serialVersionUID = -6007798761161581169L;

    private final List<String> shas;

    @JsonCreator
    public BitbucketData(@JsonProperty("shas") final List<String> shas) {
        this.shas = shas == null ? Collections.emptyList() : Collections.unmodifiableList(shas);
    }

    public List<String> getShas() {
        return shas;
    }
}
