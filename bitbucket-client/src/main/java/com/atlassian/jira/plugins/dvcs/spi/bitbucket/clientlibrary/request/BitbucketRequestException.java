package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request;

public class BitbucketRequestException extends RuntimeException {

    private static final long serialVersionUID = 2834085757547295408L;

    private final String httpResponseContent;

    public BitbucketRequestException() {
        super();
        httpResponseContent = null;
    }

    public BitbucketRequestException(String message) {
        super(message);
        httpResponseContent = null;
    }

    public BitbucketRequestException(final String message, final String httpResponseContent) {
        super(message);
        this.httpResponseContent = httpResponseContent;
    }

    public BitbucketRequestException(String message, Throwable cause) {
        super(message, cause);
        httpResponseContent = null;
    }

    public String getHttpResponseContent() {
        return httpResponseContent;
    }

    /**
     * Marker interface for {@link BitbucketRequestException}. All requests that throws an exception marked by this
     * interface will be retried
     */
    public interface RetryableRequestException {
    }

    public static final class Other extends BitbucketRequestException implements RetryableRequestException {
        private static final long serialVersionUID = 7381031365271416368L;

        public Other() {
        }

        public Other(String message) {
            super(message);
        }
    }

    public static final class BadRequest_400 extends BitbucketRequestException implements RetryableRequestException {
        private static final long serialVersionUID = -590186673151437704L;
    }

    public static final class Unauthorized_401 extends BitbucketRequestException {
        private static final long serialVersionUID = -6670696366559673165L;
    }

    public static final class Forbidden_403 extends BitbucketRequestException {
        private static final long serialVersionUID = -6165445333913608678L;
    }

    public static final class NotFound_404 extends BitbucketRequestException {
        private static final long serialVersionUID = 7065454033384883507L;

        public NotFound_404() {
            super();
        }

        public NotFound_404(String message) {
            super(message);
        }

        public NotFound_404(String message, String httpResponseContent) {
            super(message, httpResponseContent);
        }
    }

    public static final class InternalServerError_500 extends BitbucketRequestException {
        private static final long serialVersionUID = 1478907069444546575L;

        public InternalServerError_500() {
            super();
        }

        public InternalServerError_500(String message, String sentryId) {
            super(message + " (Sentry: " + sentryId + ")");
        }

    }

    public static final class ServiceUnAvailable_503 extends BitbucketRequestException {
        private static final long serialVersionUID = 6015884591229605581L;

        public ServiceUnAvailable_503(String message) {
            super(message);
        }
    }
}

