package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.v2.linker;

import java.util.Objects;

/**
 * Java representation of JSON sent to / received from Bitbucket
 */
public class ConnectLinkerValue {

    private String value;
    private Links links;

    public ConnectLinkerValue() {
    }

    public ConnectLinkerValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConnectLinkerValue that = (ConnectLinkerValue) o;
        return Objects.equals(value, that.value) &&
                Objects.equals(links, that.links);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, links);
    }

    private static class Links {
        private SelfLink self;

        public SelfLink getSelf() {
            return self;
        }

        public void setSelf(SelfLink self) {
            this.self = self;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Links links = (Links) o;
            return Objects.equals(self, links.self);
        }

        @Override
        public int hashCode() {
            return Objects.hash(self);
        }
    }

    private static class SelfLink {
        private String href;

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SelfLink selfLink = (SelfLink) o;
            return Objects.equals(href, selfLink.href);
        }

        @Override
        public int hashCode() {
            return Objects.hash(href);
        }
    }
}
