package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request;

import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.BadRequestRetryer;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.util.SystemUtils;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.regex.Pattern;

import static java.util.Arrays.stream;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.MULTILINE;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class BaseRemoteRequestor implements RemoteRequestor {

    private static final String SENTRY_ID_HEADER = "X-Sentry-ID";
    private static final Pattern HTML_PATTERN =
            Pattern.compile("(^\\w?<html?.*>)|(^\\w?<!DOCTYPE html>)", CASE_INSENSITIVE | MULTILINE);

    @VisibleForTesting
    static final String HTML_CONTENT_REPLACEMENT = "[HTML content]";

    protected final ApiProvider apiProvider;

    private final Logger log = LoggerFactory.getLogger(BaseRemoteRequestor.class);
    private final HttpClientProvider httpClientProvider;

    public BaseRemoteRequestor(ApiProvider apiProvider, HttpClientProvider httpClientProvider) {
        this.apiProvider = apiProvider;
        this.httpClientProvider = httpClientProvider;
    }

    private static String encode(String str) {
        if (str == null) {
            return null;
        }

        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new BitbucketRequestException("Required encoding not found", e);
        }
    }

    @Override
    public <T> T get(String uri, Map<String, String> parameters, ResponseCallback<T> callback) {
        return getWithRetry(uri, parametersToListParams(parameters), callback);
    }

    private Map<String, List<String>> parametersToListParams(Map<String, String> parameters) {
        if (parameters == null) {
            return null;
        }
        return Maps.transformValues(parameters, Collections::singletonList);
    }

    @Override
    public <T> T getWithMultipleVals(String uri, Map<String, List<String>> parameters, ResponseCallback<T> callback) {
        return getWithRetry(uri, parameters, callback);
    }

    @Override
    public <T> T delete(String uri, Map<String, String> parameters, ResponseCallback<T> callback) {
        return deleteWithRetry(uri, parametersToListParams(parameters), callback);
    }

    @Override
    public <T> T post(String uri, Map<String, ?> parameters, ResponseCallback<T> callback) {
        return postWithRetry(uri, parameters, callback);
    }

    @Override
    public <T> T post(final String uri, final String body, final ContentType contentType, final ResponseCallback<T> callback) {
        return requestWithBody(new HttpPost(), uri, body, contentType, callback);
    }

    @Override
    public <T> T put(String uri, Map<String, String> parameters, ResponseCallback<T> callback) {
        return putWithRetry(uri, parameters, callback);
    }

    @Override
    public <T> T put(String uri, String body, ContentType contentType, ResponseCallback<T> callback) {
        return requestWithBody(new HttpPut(), uri, body, contentType, callback);
    }

    // --------------------------------------------------------------------------------------------------
    // Retryers...
    // --------------------------------------------------------------------------------------------------

    private <T> T getWithRetry(final String uri, final Map<String, List<String>> parameters, final ResponseCallback<T> callback) {
        return new BadRequestRetryer<T>().retry(() -> requestWithoutPayload(new HttpGet(), uri, parameters, callback));
    }

    private <T> T deleteWithRetry(final String uri, final Map<String, List<String>> parameters, final ResponseCallback<T> callback) {
        return new BadRequestRetryer<T>().retry(() -> requestWithoutPayload(new HttpDelete(), uri, parameters, callback));
    }

    private <T> T postWithRetry(final String uri, final Map<String, ?> parameters, final ResponseCallback<T> callback) {
        return new BadRequestRetryer<T>().retry(() -> requestWithPayload(new HttpPost(), uri, parameters, callback));
    }

    private <T> T putWithRetry(final String uri, final Map<String, String> parameters, final ResponseCallback<T> callback) {
        return new BadRequestRetryer<T>().retry(() -> requestWithPayload(new HttpPut(), uri, parameters, callback));
    }

    // --------------------------------------------------------------------------------------------------
    // extension hooks
    // --------------------------------------------------------------------------------------------------

    /**
     * E.g. append basic auth headers ...
     */
    protected void onConnectionCreated(HttpClient client, HttpRequestBase method, Map<String, ?> params) throws IOException {

    }

    // --------------------------------------------------------------------------------------------------
    // Helpers
    // --------------------------------------------------------------------------------------------------

    /**
     * E.g. append oauth params ...
     */
    protected String afterFinalUriConstructed(HttpRequestBase method, String finalUri, Map<String, ?> params) {
        return finalUri;
    }

    protected void logRequest(HttpRequestBase method, String finalUrl, Map<String, ?> params) {
        final StringBuilder sb = new StringBuilder("{");
        processParams(params, (key, value) -> {
            if (sb.length() > 1) {
                sb.append(",");
            }
            sb.append(key).append("=").append(value);
        });

        sb.append("}");

        if (log.isDebugEnabled()) {
            log.debug("[REST call {} {}, Params: {} \nHeaders: {}]",
                    new Object[]{method.getMethod(), finalUrl, sb.toString(), sanitizeHeadersForLogging(method.getAllHeaders())});
        }
    }

    private <T> T requestWithPayload(final HttpEntityEnclosingRequestBase method, final String uri,
                                     final Map<String, ?> params, final ResponseCallback<T> callback) {
        return request(
                method,
                client -> {
                    createConnection(client, method, uri, params);
                    setPayloadParams(method, params);
                },
                callback,
                false);
    }

    private <T> T requestWithBody(final HttpEntityEnclosingRequestBase method, final String uri, final String body, final ContentType contentType,
                                  ResponseCallback<T> callback) {
        return request(
                method,
                client -> {
                    createConnection(client, method, uri, null);
                    setBody(method, body, contentType);
                },
                callback,
                false);
    }

    private <T> T requestWithoutPayload(final HttpRequestBase method, final String uri, final Map<String, List<String>> parameters, final ResponseCallback<T> callback) {
        return request(
                method,
                client -> createConnection(client, method, uri + multiParamsToString(parameters, uri.contains("?")), parameters),
                callback,
                apiProvider.isCached());
    }

    private <T, U extends HttpRequestBase> T request(U method, ClientConfigurator c, ResponseCallback<T> callback, boolean cached) {
        HttpClient client = httpClientProvider.getHttpClient(cached);
        RemoteResponse response = null;

        HttpResponse httpResponse = null;
        try {
            c.configureClient(client);

            httpResponse = client.execute(method);
            response = checkAndCreateRemoteResponse(method, httpResponse);

            return callback.onResponse(response);
        } catch (IOException | URISyntaxException e) {
            final String errorMessage = "Failed to execute request: " + method.getURI();
            if (log.isDebugEnabled()) {
                log.info(errorMessage, e);
            } else {
                log.info(errorMessage + " - " + e.getMessage());
            }
            throw new BitbucketRequestException(errorMessage, e);
        } finally {
            closeResponse(response);
            SystemUtils.releaseConnection(method, httpResponse);
            if (apiProvider.isCloseIdleConnections()) {
                httpClientProvider.closeIdleConnections();
            }
        }
    }

    private void closeResponse(RemoteResponse response) {
        if (response != null) {
            response.close();
        }
    }

    @VisibleForTesting
    @ParametersAreNonnullByDefault
    RemoteResponse checkAndCreateRemoteResponse(final HttpRequestBase httpRequest,
                                                final HttpResponse httpResponse) throws IOException {

        final int statusCode = httpResponse.getStatusLine().getStatusCode();
        if (statusCode >= 400) {
            final String content = logRequestAndResponse(httpRequest, httpResponse, statusCode);
            switch (statusCode) {
                case HttpStatus.SC_BAD_REQUEST:
                    throw new BitbucketRequestException.BadRequest_400();
                case HttpStatus.SC_UNAUTHORIZED:
                    throw new BitbucketRequestException.Unauthorized_401();
                case HttpStatus.SC_FORBIDDEN:
                    throw new BitbucketRequestException.Forbidden_403();
                case HttpStatus.SC_NOT_FOUND:
                    throw new BitbucketRequestException.NotFound_404(httpRequest.getMethod() + " " + httpRequest.getURI(), content);
                case HttpStatus.SC_INTERNAL_SERVER_ERROR:
                    throw new BitbucketRequestException.InternalServerError_500(content, getSentryId(httpResponse).orElse("N/A"));
                case HttpStatus.SC_SERVICE_UNAVAILABLE:
                    throw new BitbucketRequestException.ServiceUnAvailable_503(content);
                default:
                    throw new BitbucketRequestException.Other("Error response code during the request : " + statusCode);
            }
        }

        final RemoteResponse result = new RemoteResponse();
        result.setHttpStatusCode(statusCode);
        if (httpResponse.getEntity() != null) {
            result.setResponse(httpResponse.getEntity().getContent());
        }
        return result;
    }

    private String logRequestAndResponse(final HttpRequestBase httpRequest,
                                         final HttpResponse httpResponse,
                                         final int statusCode) throws IOException {
        final String respString = responseAsString(httpResponse);

        if (log.isDebugEnabled()) {
            log.debug("Failed to properly execute request [{} {}]\nHeaders: {}\nParams: {}\nResponse code: {}\nResponse headers: {}\nResponse: {}",
                    httpRequest.getMethod(),
                    httpRequest.getURI(),
                    sanitizeHeadersForLogging(httpRequest.getAllHeaders()),
                    httpRequest.getParams(),
                    statusCode,
                    sanitizeHeadersForLogging(httpResponse.getAllHeaders()),
                    respString
            );
        }
        else if (log.isWarnEnabled()) {
            log.warn("Failed to properly execute request [{} {}]\nHeaders: {}\nParams: {}\nResponse code: {}\nResponse headers: {}",
                    httpRequest.getMethod(),
                    httpRequest.getURI(),
                    sanitizeHeadersForLogging(httpRequest.getAllHeaders()),
                    httpRequest.getParams(),
                    statusCode,
                    sanitizeHeadersForLogging(httpResponse.getAllHeaders())
            );
        }

        return respString;
    }

    private String responseAsString(final HttpResponse httpResponse) throws IOException {
        if (httpResponse.getEntity() != null) {
            return sanitizeContentForLogging(IOUtils.toString(httpResponse.getEntity().getContent(), "UTF-8"));
        }

        return null;
    }

    @VisibleForTesting
    String sanitizeContentForLogging(final String responseContent) {
        if (responseContent == null) {
            return null;
        }
        if (HTML_PATTERN.matcher(responseContent).find()) {
            return HTML_CONTENT_REPLACEMENT;
        }
        return responseContent;
    }

    @VisibleForTesting
    Header[] sanitizeHeadersForLogging(Header[] headers) {
        return stream(headers)
                .filter(h -> !containsIgnoreCase(h.getName(), "authorization"))
                .toArray(Header[]::new);
    }

    @VisibleForTesting
    Optional<String> getSentryId(final HttpResponse response) {
        final Header header = response.getFirstHeader(SENTRY_ID_HEADER);
        return header == null ? empty() : of(header.getValue());
    }

    protected String paramsToString(Map<String, String> parameters, boolean urlAlreadyHasParams) {
        return multiParamsToString(parametersToListParams(parameters), urlAlreadyHasParams);
    }

    protected String multiParamsToString(Map<String, List<String>> parameters, boolean urlAlreadyHasParams) {
        StringBuilder queryStringBuilder = new StringBuilder();

        if (parameters != null && !parameters.isEmpty()) {
            if (!urlAlreadyHasParams) {
                queryStringBuilder.append("?");
            } else {
                queryStringBuilder.append("&");
            }

            paramsMapToString(parameters, queryStringBuilder);
        }
        return queryStringBuilder.toString();
    }

    private void paramsMapToString(Map<String, List<String>> parameters, StringBuilder builder) {
        builder.append(Joiner.on("&").join(Iterables.concat(Iterables.transform(parameters.entrySet(), new Function<Entry<String, List<String>>, Iterable<String>>() {
            @Override
            public Iterable<String> apply(@Nullable final Entry<String, List<String>> entry) {
                return Iterables.transform(Iterables.filter(entry.getValue(), StringUtils::isNotEmpty), new Function<String, String>() {
                    @Override
                    public String apply(@Nullable String entryValue) {
                        return encode(entry.getKey()) + "=" + encode(entryValue);
                    }
                });
            }
        }))));
    }

    private void createConnection(HttpClient client, HttpRequestBase method, String uri, Map<String, ?> params)
            throws IOException, URISyntaxException {
        String remoteUrl;
        if (uri.startsWith("http:/") || uri.startsWith("https:/")) {
            remoteUrl = uri;
        } else {
            String apiUrl = uri.startsWith("/api/") ? apiProvider.getHostUrl() : apiProvider.getApiUrl();
            remoteUrl = apiUrl + uri;
        }

        String finalUrl = afterFinalUriConstructed(method, remoteUrl, params);
        method.setURI(new URI(finalUrl));
        logRequest(method, finalUrl, params);
        onConnectionCreated(client, method, params);
    }

    private void setPayloadParams(HttpEntityEnclosingRequestBase method, Map<String, ?> params) throws IOException {
        if (params != null) {
            final List<NameValuePair> formparams = new ArrayList<>();
            processParams(params, (key, value) -> formparams.add(new BasicNameValuePair(key, value)));

            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, "UTF-8");
            method.setEntity(entity);
        }
    }

    private void setBody(HttpEntityEnclosingRequestBase method, String body, ContentType contentType) {
        if (body != null) {
            StringEntity entity = new StringEntity(body, contentType);
            method.setEntity(entity);
        }
    }

    protected void processParams(Map<String, ?> params, ParameterProcessor processParameter) {
        if (params == null) {
            return;
        }

        for (Entry<String, ?> entry : params.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof Collection) {
                for (Object v : (Collection<?>) value) {
                    if (v != null) {
                        processParameter.process(entry.getKey(), v.toString());
                    }
                }
            } else {
                if (value != null) {
                    processParameter.process(entry.getKey(), value.toString());
                }
            }
        }
    }

    private interface ClientConfigurator {

        void configureClient(HttpClient client) throws IOException, URISyntaxException;
    }

    protected interface ParameterProcessor {
        void process(String key, String value);
    }
}
