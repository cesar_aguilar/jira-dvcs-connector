package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary;

import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Metadata about Bitbucket.
 * <p>
 * The Bitbucket host URL can be overridden with the system property <code>{@value #BITBUCKET_URL_OVERRIDE}</code> set at startup,
 * or by calling {@link #setHostUrl(String)} at runtime (e.g. during test setup).
 * <p>
 * Note that the host URL will be returned in a normalised form with no trailing slash.
 */
public class BitbucketDetails {

    /**
     * This legacy property will be removed in future versions.
     *
     * @deprecated Use {@link #setHostUrl} instead of manually setting system properties.
     */
    @Deprecated
    @VisibleForTesting
    final static String BITBUCKET_TEST_URL_LEGACY = "bitbucket_testing_url";

    @VisibleForTesting
    final static String BITBUCKET_TEST_URL = "bitbucket.url.test";

    @VisibleForTesting
    final static String BITBUCKET_URL_OVERRIDE = "bitbucket.url";

    @VisibleForTesting
    final static String PRODUCTION_BITBUCKET_URL = "https://bitbucket.org";

    /**
     * @return the Host URL to use when contacting Bitbucket, normalised to remove trailing "/"
     */
    @Nonnull
    public static String getHostUrl() {
        String url = System.getProperty(BITBUCKET_TEST_URL, System.getProperty(BITBUCKET_TEST_URL_LEGACY));
        if (!isBlank(url)) {
            return normalise(url);
        }
        return normalise(System.getProperty(BITBUCKET_URL_OVERRIDE, PRODUCTION_BITBUCKET_URL));
    }

    /**
     * Override the host URL used for bitbucket.
     *
     * @param hostUrl The URL to set
     */
    public static void setHostUrl(final String hostUrl) {
        System.setProperty(BITBUCKET_TEST_URL, hostUrl);
    }

    private static String normalise(final String url) {
        if (url.endsWith("/")) {
            return url.substring(0, url.length() - 1);
        }
        return url;
    }

    /**
     * Reset the host URL used for bitbucket.
     */
    public static void resetHostUrl() {
        System.clearProperty(BITBUCKET_TEST_URL);
    }

    /**
     * @return If the referenced Bitbucket instance is a test configuration
     */
    public static boolean isTestConfiguration() {
        return System.getProperty(BITBUCKET_TEST_URL) != null || System.getProperty(BITBUCKET_TEST_URL_LEGACY) != null;
    }

    /**
     * @return If the referenced Bitbucket instance is the production instance or not
     */
    public static boolean isProductionInstance() {
        return getHostUrl().equals(PRODUCTION_BITBUCKET_URL);
    }
}
