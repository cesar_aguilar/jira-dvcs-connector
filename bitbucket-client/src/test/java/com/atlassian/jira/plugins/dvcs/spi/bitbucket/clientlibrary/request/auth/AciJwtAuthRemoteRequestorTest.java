package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fusion.aci.api.model.ConnectApplication;
import com.atlassian.fusion.aci.api.model.RemoteApplication;
import com.atlassian.fusion.aci.api.service.ACIJwtService;
import com.atlassian.fusion.aci.api.service.ACIRegistrationService;
import com.atlassian.fusion.aci.api.service.exception.UninstalledException;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.ApiProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.HttpClientProvider;
import com.google.common.collect.ImmutableMap;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.URI;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID;
import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth.ACIJwtAuthRemoteRequestor.UninstallationReminderEvent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class AciJwtAuthRemoteRequestorTest {
    private static final String ADDON_KEY = "addon key";
    private static final String DESCRIPTOR = "descriptor";

    private static final String PRINCIPAL_UUID = "Principal UUID";
    private static final String POST = "POST";
    private static final String DUMMY_JWT = "12345.12345.12345";
    private static final ConnectApplication CONNECT_APPLICATION = new ConnectApplication(
            BITBUCKET_CONNECTOR_APPLICATION_ID, ADDON_KEY, DESCRIPTOR, RemoteApplication.BITBUCKET);
    @Mock
    HttpClientProvider httpClientProvider;
    private URI uri;
    @Mock
    private ApiProvider apiProvider;
    @Mock
    private ACIRegistrationService registrationService;

    @Mock
    private ACIJwtService jwtService;

    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private HttpRequestBase httpRequestBase;

    @Mock
    private HttpClient httpClient;

    private ACIJwtAuthRemoteRequestor aciJwtAuthRemoteRequestor;

    @BeforeMethod
    public void setup() throws Exception {
        initMocks(this);

        uri = new URI(BitbucketDetails.getHostUrl());

        when(httpRequestBase.getMethod()).thenReturn(POST);
        when(httpRequestBase.getURI()).thenReturn(uri);

        when(jwtService.generateJwtToken(BITBUCKET_CONNECTOR_APPLICATION_ID, PRINCIPAL_UUID, POST, uri.toURL()))
                .thenReturn(DUMMY_JWT);

        when(registrationService.get(BITBUCKET_CONNECTOR_APPLICATION_ID)).thenReturn(CONNECT_APPLICATION);

        aciJwtAuthRemoteRequestor = new ACIJwtAuthRemoteRequestor(
                apiProvider, PRINCIPAL_UUID, registrationService, jwtService, httpClientProvider, eventPublisher);
    }

    @Test
    public void onConnectionCreatedShouldRaiseAnUninstallationReminderEventIfInstallationIsUninstalled()
            throws Exception {
        // setup
        final UninstallationReminderEvent expectedEvent =
                new ACIJwtAuthRemoteRequestor.UninstallationReminderEvent(PRINCIPAL_UUID, CONNECT_APPLICATION);
        when(jwtService.generateJwtToken(BITBUCKET_CONNECTOR_APPLICATION_ID, PRINCIPAL_UUID, POST, uri.toURL()))
                .thenThrow(new UninstalledException(BITBUCKET_CONNECTOR_APPLICATION_ID, PRINCIPAL_UUID));

        // execute
        aciJwtAuthRemoteRequestor.onConnectionCreated(httpClient, httpRequestBase, ImmutableMap.of());

        // check
        verify(eventPublisher).publish(expectedEvent);
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void onConnectionCreatedShouldThrowAnIllegalStateExceptionIfJwtCreationFails() throws Exception {
        // setup
        final Exception expectedException = new RuntimeException();
        when(jwtService.generateJwtToken(BITBUCKET_CONNECTOR_APPLICATION_ID, PRINCIPAL_UUID, POST, uri.toURL()))
                .thenThrow(expectedException);

        // execute
        try {
            aciJwtAuthRemoteRequestor.onConnectionCreated(httpClient, httpRequestBase, ImmutableMap.of());
        } catch (IllegalStateException e) {
            assertThat(e.getCause(), equalTo(expectedException));
            throw e;
        }
    }

    @Test
    public void onConnectionCreatedShouldSetTheAuthHeaderOfTheRequestToTheJwtToken() throws Exception {
        // execute
        aciJwtAuthRemoteRequestor.onConnectionCreated(httpClient, httpRequestBase, ImmutableMap.of());

        // check
        verify(httpRequestBase).setHeader("Authorization", "JWT " + DUMMY_JWT);
    }
}
