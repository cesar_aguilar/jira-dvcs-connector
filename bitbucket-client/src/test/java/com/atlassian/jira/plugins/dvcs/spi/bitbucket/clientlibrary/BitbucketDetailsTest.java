package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class BitbucketDetailsTest {

    @AfterMethod
    public void teardown() {
        System.clearProperty(BitbucketDetails.BITBUCKET_TEST_URL);
        System.clearProperty(BitbucketDetails.BITBUCKET_TEST_URL_LEGACY);
        System.clearProperty(BitbucketDetails.BITBUCKET_URL_OVERRIDE);
    }

    @Test
    public void testWithNoOverride() {
        assertEquals(BitbucketDetails.getHostUrl(), BitbucketDetails.PRODUCTION_BITBUCKET_URL);
        assertEquals(BitbucketDetails.isProductionInstance(), true);
        assertEquals(BitbucketDetails.isTestConfiguration(), false);
    }

    @Test
    public void testGetHostWithStartupOverride() {
        System.setProperty(BitbucketDetails.BITBUCKET_URL_OVERRIDE, "http://www.example.com");

        assertEquals(BitbucketDetails.getHostUrl(), "http://www.example.com");
        assertEquals(BitbucketDetails.isProductionInstance(), false);
        assertEquals(BitbucketDetails.isTestConfiguration(), false);
    }

    @Test
    public void testGetHostWithLegacyTestUrlOverride() {
        System.setProperty(BitbucketDetails.BITBUCKET_TEST_URL_LEGACY, "http://www.example.com");

        assertEquals(BitbucketDetails.getHostUrl(), "http://www.example.com");
        assertEquals(BitbucketDetails.isProductionInstance(), false);
        assertEquals(BitbucketDetails.isTestConfiguration(), true);
    }

    @Test
    public void testGetHostWithTestUrlOverride() {
        System.setProperty(BitbucketDetails.BITBUCKET_TEST_URL, "http://www.example.com");

        assertEquals(BitbucketDetails.getHostUrl(), "http://www.example.com");
        assertEquals(BitbucketDetails.isProductionInstance(), false);
        assertEquals(BitbucketDetails.isTestConfiguration(), true);
    }

    @Test
    public void testGetHostWithTestUrlAndStartupOverride() {
        System.setProperty(BitbucketDetails.BITBUCKET_TEST_URL, "http://www.example.com");
        System.setProperty(BitbucketDetails.BITBUCKET_URL_OVERRIDE, "http://www.another-example.com");

        assertEquals(BitbucketDetails.getHostUrl(), "http://www.example.com");
        assertEquals(BitbucketDetails.isProductionInstance(), false);
        assertEquals(BitbucketDetails.isTestConfiguration(), true);
    }

    @Test
    public void testGetHostWithAllOverrides() {
        System.setProperty(BitbucketDetails.BITBUCKET_TEST_URL, "http://www.one.com");
        System.setProperty(BitbucketDetails.BITBUCKET_TEST_URL_LEGACY, "http://www.two.com");
        System.setProperty(BitbucketDetails.BITBUCKET_URL_OVERRIDE, "http://www.three.com");

        assertEquals(BitbucketDetails.getHostUrl(), "http://www.one.com");
        assertEquals(BitbucketDetails.isProductionInstance(), false);
        assertEquals(BitbucketDetails.isTestConfiguration(), true);
    }

    @Test
    public void testGetHostWithTrailingSlash() {
        System.setProperty(BitbucketDetails.BITBUCKET_URL_OVERRIDE, "http://www.example.com/");

        assertEquals(BitbucketDetails.getHostUrl(), "http://www.example.com");
        assertEquals(BitbucketDetails.isProductionInstance(), false);
        assertEquals(BitbucketDetails.isTestConfiguration(), false);
    }

}