package com.atlassian.jira.plugins.dvcs.util;

import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class SystemUtilsTest {
    @Test
    public void testValidateUrlWithNull() {
        assertFalse(SystemUtils.isValid(null));
    }

    @Test
    public void testValidateUrlWithEmpty() {
        assertFalse(SystemUtils.isValid(""));
    }

    @Test
    public void testValidateUrlWithValid() {
        assertTrue(SystemUtils.isValid("https://staging.bitbucket.com/!api/2.0/repositories"));
    }

    @Test
    public void testValidateUrlWithInvalid() {
        assertFalse(SystemUtils.isValid("not/a/url"));
    }
}