package com.atlassian.jira.plugins.dvcs.model.credential;

/**
 * Visitor for {@link Credential} that returns a {@link T}
 *
 * @param <T> The type of object that this visitor produces based on the given credential
 */
public interface CredentialVisitor<T> {
    T visit(BasicAuthCredential credential);

    T visit(PrincipalIDCredential credential);

    T visit(TwoLeggedOAuthCredential credential);

    T visit(ThreeLeggedOAuthCredential credential);

    T visit(UnauthenticatedCredential credential);
}
