package com.atlassian.jira.plugins.dvcs.model.credential;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Credentials for 3LO authentication.
 * <p>
 * Provides key, secret and access tokens.
 * <p>
 * <em>Note:</em> The {@code oauthKey} and {@code oauthSecret} are allowed to be empty to support legacy organizations
 * that may not have a client key and secret set. In this scenario only the stored access token is used for authentication.
 * Users are still able to reset their credentials and provide a key and secret
 * (which should be considered required fields going forward).
 */
public class ThreeLeggedOAuthCredential implements OAuthCredential {
    private final String oauthKey;
    private final String oauthSecret;

    private final String accessToken;

    public ThreeLeggedOAuthCredential(@Nullable final String oauthKey, @Nullable final String oauthSecret, @Nonnull final String accessToken) {
        checkArgument(isNotBlank(accessToken));
        this.accessToken = accessToken;
        this.oauthKey = oauthKey;
        this.oauthSecret = oauthSecret;
    }

    public static CredentialVisitor<Optional<ThreeLeggedOAuthCredential>> visitor() {
        return new ThreeLeggedOauthCredentialVisitor();
    }

    @Override
    public String getKey() {
        return oauthKey;
    }

    @Override
    public String getSecret() {
        return oauthSecret;
    }

    @Nonnull
    public String getAccessToken() {
        return accessToken;
    }

    @Override
    @Nonnull
    public CredentialType getType() {
        return CredentialType.THREE_LEGGED_OAUTH;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ThreeLeggedOAuthCredential that = (ThreeLeggedOAuthCredential) o;
        return Objects.equals(accessToken, that.accessToken) &&
                Objects.equals(getKey(), that.getKey()) &&
                Objects.equals(getSecret(), that.getSecret());
    }

    @Override
    public int hashCode() {
        return Objects.hash(accessToken, getKey(), getSecret());
    }

    @Override
    public <T> T accept(CredentialVisitor<T> visitor) {
        return visitor.visit(this);
    }

    private static class ThreeLeggedOauthCredentialVisitor
            extends AbstractOptionalCredentialVisitor<ThreeLeggedOAuthCredential> {
        @Override
        public Optional<ThreeLeggedOAuthCredential> visit(final ThreeLeggedOAuthCredential credential) {
            return Optional.ofNullable(credential);
        }
    }
}
