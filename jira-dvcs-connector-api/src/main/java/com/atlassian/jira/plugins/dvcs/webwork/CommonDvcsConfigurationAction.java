package com.atlassian.jira.plugins.dvcs.webwork;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsConfigAddEndedAnalyticsEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsConfigAddStartedAnalyticsEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType;
import com.atlassian.jira.plugins.dvcs.analytics.event.FailureReason;
import com.atlassian.jira.plugins.dvcs.analytics.event.Source;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;

import static com.atlassian.jira.plugins.dvcs.util.CustomStringUtils.encode;
import static com.google.common.base.Preconditions.checkNotNull;

@Scanned
public class CommonDvcsConfigurationAction extends JiraWebActionSupport {
    public static final String DEFAULT_SOURCE = "unknown";
    private static final long serialVersionUID = 8695500426304238626L;
    private String autoLinking = "";
    private String autoSmartCommits = "";
    private String source;

    private EventPublisher eventPublisher;

    public CommonDvcsConfigurationAction(@ComponentImport @Nonnull final EventPublisher eventPublisher) {
        this.eventPublisher = checkNotNull(eventPublisher);
    }

    protected boolean hadAutolinkingChecked() {
        return StringUtils.isNotBlank(autoLinking);
    }

    protected boolean hadAutoSmartCommitsChecked() {
        return StringUtils.isNotBlank(autoSmartCommits);
    }

    public String getAutoLinking() {
        return autoLinking;
    }

    public void setAutoLinking(String autoLinking) {
        this.autoLinking = autoLinking;
    }

    public String getAutoSmartCommits() {
        return autoSmartCommits;
    }

    public void setAutoSmartCommits(String autoSmartCommits) {
        this.autoSmartCommits = autoSmartCommits;
    }

    protected void triggerAddStartedEvent(final DvcsType type) {
        eventPublisher.publish(new DvcsConfigAddStartedAnalyticsEvent(getSourceOrDefault(), type));
    }

    protected void triggerAddSucceededEvent(final DvcsType type, final Organization org) {
        eventPublisher.publish(DvcsConfigAddEndedAnalyticsEvent.createSucceeded(getSourceOrDefault(), org));
    }

    protected void triggerAddFailedEvent(final DvcsType type, final FailureReason reason) {
        eventPublisher.publish(DvcsConfigAddEndedAnalyticsEvent.createFailed(getSourceOrDefault(), type, reason));
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Source getSourceOrDefault() {
        if (StringUtils.isNotBlank(source)) {
            return Source.valueOf(source.toUpperCase());
        } else {
            return Source.UNKNOWN;
        }
    }

    /**
     * Calculate the url parameter string of the form "&amp;source=xxx" for source parameter.
     * <p>
     * If source is null, empty string "" is returned.
     *
     * @return
     */
    protected String getSourceAsUrlParam() {
        return getSourceAsUrlParam("&");
    }

    /**
     * Calculate the url parameter string of the form "&amp;source=xxx" for source parameter.
     * <p>
     * If source is null, empty string "" is returned.
     *
     * @param paramSeparator either "&amp;" or "?" depending on whether there are other url params.
     * @return
     */
    protected String getSourceAsUrlParam(String paramSeparator) {
        return StringUtils.isNotEmpty(source) ? (paramSeparator + "source=" + encode(source)) : "";
    }
}
