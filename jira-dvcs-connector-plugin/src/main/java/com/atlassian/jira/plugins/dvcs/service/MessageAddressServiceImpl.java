package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettings;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.jira.plugins.dvcs.service.message.HasProgress;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddress;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddressService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

import static org.slf4j.LoggerFactory.getLogger;

@Named
@ParametersAreNonnullByDefault
public class MessageAddressServiceImpl implements MessageAddressService {
    private static final Logger log = getLogger(MessageAddressServiceImpl.class);

    private static final CacheSettings CACHE_SETTINGS = new CacheSettingsBuilder().local().build();

    private final Cache<IdKey<?>, MessageAddress<?>> idToMessageAddress;

    @Inject
    public MessageAddressServiceImpl(@ComponentImport final CacheManager cacheManager) {
        idToMessageAddress = cacheManager.getCache(getClass().getName() + ".idToMessageAddress",
                new MessageAddressLoader(),
                CACHE_SETTINGS);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <P extends HasProgress> MessageAddress<P> get(final Class<P> payloadType, final String id) {
        final IdKey<P> key = new IdKey<>(id, payloadType);
        return (MessageAddress<P>) idToMessageAddress.get(key);
    }

    /**
     * Loads the <code>idToMessageAddress</code> cache upon a miss.
     *
     * @param <P> the type of payload
     */
    @VisibleForTesting
    public static class MessageAddressLoader<P extends HasProgress> implements CacheLoader<IdKey<P>, MessageAddress<P>> {
        @Override
        @Nonnull
        public MessageAddress<P> load(@Nonnull final IdKey<P> key) {
            log.debug("idToMessageAddress loading new item for key id: {} payloadType: {} ", key.id, key.payloadType);
            return new MessageAddress<P>() {
                @Override
                public String getId() {
                    return key.id;
                }

                @Override
                public Class<P> getPayloadType() {
                    return key.payloadType;
                }
            };
        }
    }

    @VisibleForTesting
    public static class IdKey<P extends HasProgress> implements Serializable {
        private static final long serialVersionUID = 7389777255355593862L;
        private final String id;
        private final Class<P> payloadType;

        @VisibleForTesting
        public IdKey(final String id, final Class<P> payloadType) {
            this.id = id;
            this.payloadType = payloadType;
        }

        @Override
        public int hashCode() {
            return id.hashCode();
        }

        @Override
        public boolean equals(@Nullable final Object obj) {
            //noinspection unchecked
            return (this == obj) ||
                    (obj != null && obj instanceof IdKey && StringUtils.equals(id, ((IdKey<P>) obj).id));
        }
    }
}
