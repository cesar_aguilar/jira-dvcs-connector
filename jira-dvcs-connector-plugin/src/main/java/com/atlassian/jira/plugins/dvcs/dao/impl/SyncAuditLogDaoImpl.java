package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService;
import com.atlassian.jira.plugins.dvcs.dao.SyncAuditLogDao;
import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import io.atlassian.fugue.Option;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping.REPO_ID;
import static com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping.START_DATE;
import static com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping.SYNC_STATUS;
import static com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping.SYNC_STATUS_FAILED;
import static com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping.SYNC_STATUS_RUNNING;
import static com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping.SYNC_STATUS_SLEEPING;
import static com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping.SYNC_STATUS_SUCCESS;
import static com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping.SYNC_TYPE;
import static com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping.TOTAL_ERRORS;
import static com.google.common.base.Preconditions.checkNotNull;
import static io.atlassian.fugue.Option.option;
import static io.atlassian.fugue.Unit.Unit;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace;

@Named
public class SyncAuditLogDaoImpl implements SyncAuditLogDao {

    private static final int BIG_DATA_PAGESIZE = 200;
    private static final int ROTATION_PERIOD = 1000 * 60 * 60 * 24 * 7;

    private static final Logger log = LoggerFactory.getLogger(SyncAuditLogDaoImpl.class);

    private final ActiveObjects ao;
    private final AnalyticsService analyticsService;

    @Inject
    @ParametersAreNonnullByDefault
    public SyncAuditLogDaoImpl(
            @ComponentImport final ActiveObjects ao,
            final AnalyticsService analyticsService) {
        this.ao = checkNotNull(ao);
        this.analyticsService = checkNotNull(analyticsService);
    }

    private static Query pageQuery(Query q, Integer page) {
        q.setLimit(BIG_DATA_PAGESIZE);
        if (page == null) {
            q.setOffset(0);
        } else {
            q.setOffset(BIG_DATA_PAGESIZE * page);
        }
        return q;
    }

    @Override
    public SyncAuditLogMapping newSyncAuditLog(final int repoId, final Set<SynchronizationFlag> flags, final Date startDate) {
        return ao.executeInTransaction(() -> {
            rotate(repoId);

            final Map<String, Object> data = new HashMap<>();
            data.put(REPO_ID, repoId);
            data.put(SYNC_TYPE, toSyncTypeString(flags));
            data.put(START_DATE, startDate);
            data.put(SYNC_STATUS, SYNC_STATUS_RUNNING);
            data.put(TOTAL_ERRORS, 0);
            return ao.create(SyncAuditLogMapping.class, data);
        });
    }

    private void rotate(final int repoId) {
        final Date maxStartDate = new Date(System.currentTimeMillis() - ROTATION_PERIOD);
        ao.deleteWithSQL(SyncAuditLogMapping.class, REPO_ID + " = ? AND " + START_DATE + " < ?", repoId, maxStartDate);
    }

    private String toSyncTypeString(final Iterable<SynchronizationFlag> flags) {
        final StringBuilder bld = new StringBuilder();
        for (final SynchronizationFlag flag : flags) {
            switch (flag) {
                case SOFT_SYNC:
                    bld.append(SyncAuditLogMapping.SYNC_TYPE_SOFT).append(" ");
                    break;
                case SYNC_CHANGESETS:
                    bld.append(SyncAuditLogMapping.SYNC_TYPE_CHANGESETS).append(" ");
                    break;
                case SYNC_PULL_REQUESTS:
                    bld.append(SyncAuditLogMapping.SYNC_TYPE_PULLREQUESTS).append(" ");
                    break;
                case WEBHOOK_SYNC:
                    bld.append(SyncAuditLogMapping.SYNC_TYPE_WEBHOOKS).append(" ");
                    break;
                default: // Do nothing.
                    break;
            }
        }
        return bld.toString();
    }

    @Override
    public void finish(final int syncId, final Date firstRequestDate, final int numRequests,
                                      final int flightTimeMs, final Date finishDate) {
        ao.executeInTransaction(() -> {
            find(syncId).forEach(mapping -> {
                mapping.setFirstRequestDate(firstRequestDate);
                mapping.setEndDate(finishDate);
                mapping.setNumRequests(numRequests);
                mapping.setFlightTimeMs(flightTimeMs);

                if (isNotBlank(mapping.getExcTrace())) {
                    mapping.setSyncStatus(SYNC_STATUS_FAILED);
                } else {
                    mapping.setSyncStatus(SYNC_STATUS_SUCCESS);
                }

                mapping.save();

                analyticsService.publishRepositorySyncEnd(mapping.getID(), mapping.getEndDate(),
                        mapping.getEndDate().getTime() - mapping.getStartDate().getTime());
            });
            return Unit();
        });
    }

    @Override
    public void pause(final int syncId) {
        ao.executeInTransaction(() -> {
            find(syncId).forEach(mapping -> {
                mapping.setSyncStatus(SYNC_STATUS_SLEEPING);
                mapping.save();
            });
            return Unit();
        });
    }

    @Override
    public void resume(final int syncId) {
        ao.executeInTransaction(() -> {
            find(syncId)
                    .filter(mapping -> SYNC_STATUS_SLEEPING.equals(mapping.getSyncStatus()))
                    .forEach(mapping -> {
                        mapping.setSyncStatus(SYNC_STATUS_RUNNING);
                        mapping.save();
                    });
            return Unit();
        });
    }

    @Override
    public int removeAllForRepo(final int repoId) {
        return ao.deleteWithSQL(SyncAuditLogMapping.class, REPO_ID + " = ?", repoId);
    }

    @Override
    public void setException(final int syncId, final Throwable throwable, final boolean overwriteOld) {
        ao.executeInTransaction(() ->
            find(syncId).fold(
                    () -> {
                        log.warn("No SyncAuditLogMapping with ID = {}", syncId);
                        return Unit();
                    },
                    mapping -> {
                        final boolean noExceptionYet = isBlank(mapping.getExcTrace());
                        if (throwable != null && (overwriteOld || noExceptionYet)) {
                            mapping.setExcTrace(getStackTrace(throwable));
                        }
                        mapping.setTotalErrors(mapping.getTotalErrors() + 1);
                        mapping.save();
                        return Unit();
                    }
            )
        );
    }

    @Override
    public SyncAuditLogMapping[] getAllForRepo(final int repoId, final Integer page) {
        return ao.executeInTransaction(() ->
                ao.find(SyncAuditLogMapping.class, repoQuery(repoId).page(page).order(START_DATE + " DESC")));
    }

    @Override
    public SyncAuditLogMapping[] getAll(final Integer page) {
        return ao.executeInTransaction(() ->
                ao.find(SyncAuditLogMapping.class, pageQuery(Query.select().order(START_DATE + " DESC"), page)));
    }

    private Option<SyncAuditLogMapping> find(final int syncId) {
        return option(ao.get(SyncAuditLogMapping.class, syncId));
    }

    private PageableQuery repoQuery(final int repoId) {
        return new PageableQuery(repoId);
    }

    class PageableQuery {

        private Query q;

        private PageableQuery(int repoId) {
            this.q = Query.select().from(SyncAuditLogMapping.class).where(REPO_ID + " = ?", repoId);
        }

        Query page(Integer page) {
            pageQuery(q, page);
            return q;
        }
    }
}
