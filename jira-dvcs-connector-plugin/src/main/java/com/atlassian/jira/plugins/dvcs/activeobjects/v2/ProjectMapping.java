package com.atlassian.jira.plugins.dvcs.activeobjects.v2;

import net.java.ao.Entity;
import net.java.ao.schema.Table;

import java.util.Date;

@Table("ProjectMappingV2")
public interface ProjectMapping extends Entity {
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String ADMIN_PASSWORD = "ADMIN_PASSWORD";
    public static final String ADMIN_USERNAME = "ADMIN_USERNAME";
    public static final String REPOSITORY_TYPE = "REPOSITORY_TYPE";
    public static final String PROJECT_KEY = "PROJECT_KEY";
    public static final String REPOSITORY_URL = "REPOSITORY_URL";
    public static final String REPOSITORY_NAME = "REPOSITORY_NAME";

    String getRepositoryName();

    void setRepositoryName(String repositoryName);

    String getRepositoryType();

    void setRepositoryType(String repositoryType);

    String getRepositoryUrl();

    void setRepositoryUrl(String repositoryUrl);

    String getProjectKey();

    void setProjectKey(String projectKey);

    String getAdminPassword();

    void setAdminPassword(String pasword);

    String getAdminUsername();

    void setAdminUsername(String username);

    String getAccessToken();

    void setAccessToken(String accessToken);

    Date getLastCommitDate();

    void setLastCommitDate(Date lastCommitDate);

}
