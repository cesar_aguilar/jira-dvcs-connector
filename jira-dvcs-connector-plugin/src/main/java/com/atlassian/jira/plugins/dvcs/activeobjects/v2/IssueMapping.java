package com.atlassian.jira.plugins.dvcs.activeobjects.v2;

import net.java.ao.Entity;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import java.util.Date;

@Table("IssueMappingV2")
public interface IssueMapping extends Entity {
    public static final String REPOSITORY_ID = "REPOSITORY_ID";
    public static final String ISSUE_ID = "ISSUE_ID";
    public static final String NODE = "NODE";
    public static final String RAW_AUTHOR = "RAW_AUTHOR";
    public static final String AUTHOR = "AUTHOR";
    public static final String DATE = "DATE";
    public static final String RAW_NODE = "RAW_NODE";
    public static final String BRANCH = "BRANCH";
    public static final String MESSAGE = "MESSAGE";
    public static final String PARENTS_DATA = "PARENTS_DATA";
    public static final String FILES_DATA = "FILES_DATA";
    public static final String VERSION = "VERSION";
    /**
     * Rows at the table can contain data loaded by previous versions of this plugin. Some column data maybe missing
     * because previous versions of plugin was not loading them. To get the updated version of changeset we need
     * to reload the data from the BB/GH servers. This flag marks the row data as latest.
     */
    public static final int LATEST_VERSION = 2;

    int getRepositoryId();

    void setRepositoryId(int repositoryId);

    String getNode();

    void setNode(String node);

    String getIssueId();

    void setIssueId(String issueId);

    String getRawAuthor();

    void setRawAuthor(String rawAuthor);

    String getAuthor();

    void setAuthor(String author);

    Date getDate();

    void setDate(Date date);

    String getRawNode();

    void setRawNode(String rawNode);

    String getBranch();

    void setBranch(String branch);

    @StringLength(StringLength.UNLIMITED)
    String getMessage();

    @StringLength(StringLength.UNLIMITED)
    void setMessage(String message);

    @StringLength(StringLength.UNLIMITED)
    String getFilesData();

    @StringLength(StringLength.UNLIMITED)
    void setFilesData(String files);

    String getParentsData();

    void setParentsData(String parents);

    Integer getVersion();

    void setVersion(Integer version);
}
