package com.atlassian.jira.plugins.dvcs.rest.internal.v1;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.plugins.dvcs.model.Progress;
import com.atlassian.jira.plugins.dvcs.rest.security.AdminOnly;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.EnumSet;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

@AdminOnly
@Internal
@Path("/repository")
@Consumes(MediaType.APPLICATION_JSON)
@ParametersAreNonnullByDefault
public class RepositoryResource {

    private final RepositoryService repositoryService;

    @Inject
    public RepositoryResource(final RepositoryService repositoryService) {
        this.repositoryService = checkNotNull(repositoryService);
    }

    /**
     * Perform a soft sync of the repository and block until the sync completes.
     * <p>
     * This resource should not be used in production as syncs can take a long time and the request may time out.
     *
     * @param repositoryId The ID of the repository to sync
     * @return HTTP.404 if no repository with the given ID exists; HTTP.204 when the sync completes successfully.
     * @response.representation.204.doc Returned when the sync has completed successfully.
     * @response.representation.404.doc Returned if the provided ID is not a valid repository ID
     */
    @POST
    @Path("/{id}/syncAndWait")
    public Response syncRepositorySynchronously(@PathParam("id") final int repositoryId) {

        final Optional<Progress> progress = repositoryService.sync(repositoryId, EnumSet.of(
                SynchronizationFlag.SOFT_SYNC,
                SynchronizationFlag.SYNC_CHANGESETS,
                SynchronizationFlag.SYNC_PULL_REQUESTS));

        if (!progress.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        progress.get().waitForFinish();
        return Response.noContent().build();
    }

}
