package com.atlassian.jira.plugins.dvcs.event;

import javax.annotation.Nonnull;
import java.util.function.Consumer;

/**
 * DAO for SyncEventMapping instances.
 */
public interface SyncEventDao {

    /**
     * Saves a new event to the database
     *
     * @param event the event to be saved
     */
    void save(@Nonnull final ContextAwareSyncEvent event);

    /**
     * Deletes a single sync event from the database
     * The getId of the event must not be empty
     *
     * @param event the event we want to delete
     */
    void delete(@Nonnull final ContextAwareSyncEvent event);

    /**
     * Applies the consumer function to each matching record in the database. The function will be applied in id order.
     * The consumer function can destructively
     * modify the records in the database, but will not terminate if you pass in function that adds records.
     *
     * @param repoId   the repoId we want to match on when getting element to apply on
     * @param consumer the function we want to apply to each record.
     */
    void foreachByRepoId(int repoId, @Nonnull final Consumer<ContextAwareSyncEvent> consumer);

    /**
     * Deletes all events that have the given repoId
     *
     * @param repoId the repoId of the records we want to delete
     * @return then number of deleted records
     */
    long deleteAll(int repoId);
}
