package com.atlassian.jira.plugins.dvcs.rest.filter;

import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

import javax.ws.rs.ext.Provider;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.singletonList;

/**
 * <p>A {@link ResourceFilterFactory} that checks wether the client is authenticated or not.<p>
 *
 * @see AdminOnlyResourceFilter
 */
@Provider
public class AdminOnlyResourceFilterFactory implements ResourceFilterFactory {

    private final GlobalPermissionManager globalPermissionManager;
    private final JiraAuthenticationContext authenticationContext;

    public AdminOnlyResourceFilterFactory(
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final GlobalPermissionManager globalPermissionManager) {
        this.authenticationContext = checkNotNull(authenticationContext);
        this.globalPermissionManager = checkNotNull(globalPermissionManager);
    }

    public List<ResourceFilter> create(AbstractMethod abstractMethod) {
        return singletonList(
                new AdminOnlyResourceFilter(abstractMethod, globalPermissionManager, authenticationContext));
    }
}
