package com.atlassian.jira.plugins.dvcs.listener;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fusion.aci.api.event.InstallationCompletedEvent;
import com.atlassian.fusion.aci.api.event.UninstallationCompletedEvent;
import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialType;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens for ACI LifeCycleCompleted events and responds appropriately.
 */
@Named
public class AciLifeCycleListener {
    private static final Logger log = LoggerFactory.getLogger(AciLifeCycleListener.class);

    private final AnalyticsService analyticsService;
    private final EventPublisher eventPublisher;
    private final OrganizationService organizationService;

    @Inject
    public AciLifeCycleListener(
            @ComponentImport @Nonnull final EventPublisher eventPublisher,
            @Nonnull final AnalyticsService analyticsService,
            @Nonnull final OrganizationService organizationService) {
        this.analyticsService = checkNotNull(analyticsService);
        this.organizationService = checkNotNull(organizationService);
        this.eventPublisher = checkNotNull(eventPublisher);
    }

    @PostConstruct
    private void init() {
        eventPublisher.register(this);
    }

    @PreDestroy
    private void destroy() {
        eventPublisher.unregister(this);
    }

    /**
     * Listens for {@link com.atlassian.fusion.aci.api.event.InstallationCompletedEvent}s and creates new DVCS
     * organisations with the appropriate credentials.
     *
     * @param e the event signalling there was a new ACI installation
     */
    @EventListener
    public void createOrUpdateDvcsOrganisation(final InstallationCompletedEvent e) {
        if (e == null || !BITBUCKET_CONNECTOR_APPLICATION_ID.equals(e.getApplicationId())) {
            return;
        }

        log.debug("Received installation completed event for {}", e.getPrincipalId());

        final Installation installation = e.getInstallation();
        final Organization org = organizationService.getByHostAndName(
                installation.getBaseUrl(), installation.getPrincipalUsername());

        if (org != null) {
            organizationService.migrateExistingOrgToPrincipalCredentialOrg(org, installation.getPrincipalUuid());
        } else {
            organizationService.createNewOrgBasedOnAciInstallation(installation);
        }
    }

    /**
     * Listens for {@link com.atlassian.fusion.aci.api.event.UninstallationCompletedEvent}s and removes the
     * corresponding organisation.
     *
     * @param e the event signalling there was an ACI uninstallation
     */
    @EventListener
    public void removeDvcsOrganisation(final UninstallationCompletedEvent e) {
        if (e == null || !BITBUCKET_CONNECTOR_APPLICATION_ID.equals(e.getApplicationId())) {
            return;
        }

        log.debug("Received uninstallation completed event for {}", e.getPrincipalId());

        final Installation installation = e.getInstallation();
        final Optional<Organization> organizationOptional = getOrgUsingInstallation(installation)
                .filter(org -> credentialIsPrincipalBased(org, installation));

        if (organizationOptional.isPresent()) {
            final Organization organization = organizationOptional.get();
            organizationService.remove(organization.getId());
        }
    }

    private boolean credentialIsPrincipalBased(final Organization organization, final Installation installation) {
        boolean isPrincipalBased = (organization.getCredential().getType() == CredentialType.PRINCIPAL_ID);
        if (!isPrincipalBased) {
            log.warn("Received a request from ACI to remove organisation with host '{}' and username '{}' " +
                            "but that organisation is not managed by ACI",
                    installation.getBaseUrl(), installation.getPrincipalUsername());
        }
        return isPrincipalBased;
    }

    private Optional<Organization> getOrgUsingInstallation(final Installation installation) {
        Organization org = organizationService.getByHostAndName(
                installation.getBaseUrl(), installation.getPrincipalUsername());
        if (org == null) {
            log.warn("Received a request from ACI to remove organisation with host '{}' and username '{}' " +
                    "but no such organisation exists", installation.getBaseUrl(), installation.getPrincipalUsername());
        }

        return Optional.ofNullable(org);
    }
}
