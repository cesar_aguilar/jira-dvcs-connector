package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettings;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.cache.CachedReference;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.dataimport.ImportCompletedEvent;
import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.plugins.dvcs.dao.OrganizationAOFacade;
import com.atlassian.jira.plugins.dvcs.dao.OrganizationDao;
import com.atlassian.jira.plugins.dvcs.dao.impl.querydsl.OrganizationQueryDslFacade;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.concurrent.TimeUnit.MINUTES;

/**
 * Cache Organization and return defensive copies rather than the cached object. Cache expires according to
 * CACHE_SETTINGS configuration.
 */
@Named("organizationDao")
public class CachingOrganizationDaoImpl implements OrganizationDao {
    private static final Logger log = LoggerFactory.getLogger(CachingOrganizationDaoImpl.class);

    private static final CacheSettings CACHE_SETTINGS = new CacheSettingsBuilder().expireAfterWrite(30, MINUTES).build();

    @ClusterSafe
    private final CachedReference<List<Organization>> organizationsCache;

    private final OrganizationAOFacade organizationAOFacade;

    private final OrganizationQueryDslFacade organizationQueryDslFacade;

    private final EventPublisher eventPublisher;

    @Inject
    public CachingOrganizationDaoImpl(
            @ComponentImport @Nonnull final CacheManager cacheManager,
            @ComponentImport @Nonnull final EventPublisher eventPublisher,
            @Nonnull final OrganizationAOFacade organizationAOFacade,
            @Nonnull final OrganizationQueryDslFacade organizationQueryDslFacade) {
        checkNotNull(cacheManager);
        this.organizationAOFacade = checkNotNull(organizationAOFacade);
        this.eventPublisher = checkNotNull(eventPublisher);
        this.organizationQueryDslFacade = checkNotNull(organizationQueryDslFacade);

        organizationsCache = cacheManager.getCachedReference(
                getClass().getName() + ".organizationsCache", organizationAOFacade::fetch, CACHE_SETTINGS);
    }

    @Override
    public List<Organization> getAll() {
        return cloneOrgs(getAllCachedOrgs());
    }

    @Override
    public int getAllCount() {
        return getAllCachedOrgs().size();
    }

    @Override
    public List<Organization> getAllByType(final String dvcsType) {
        final List<Organization> orgs = getAllCachedOrgs();
        final Iterable<Organization> orgsByType = Iterables.filter(orgs, org -> dvcsType.equals(org.getDvcsType()));
        return cloneOrgs(orgsByType);
    }

    @Override
    public Organization get(final int organizationId) {
        return findOrganization(org -> org.getId() == organizationId);
    }

    @Override
    public Organization getByPrincipalId(final String principalId) {
        return findOrganization(org -> {
            final Optional<PrincipalIDCredential> credential = org.getCredential().accept(PrincipalIDCredential.visitor());
            return credential.isPresent() && credential.get().getPrincipalId().equals(principalId);
        });
    }

    @Override
    public Organization getByHostAndName(final String hostUrl, final String name) {
        checkNotNull(hostUrl);
        checkNotNull(name);

        return findOrganization(org -> hostUrl.equals(org.getHostUrl()) && name.equalsIgnoreCase(org.getName()));
    }

    @Override
    public List<Organization> getAllByIds(final Collection<Integer> ids) {
        checkNotNull(ids);

        final List<Organization> orgs = getAllCachedOrgs();

        final Iterable<Organization> orgsByIds = Iterables.filter(orgs, org -> ids.contains(org.getId()));

        return cloneOrgs(orgsByIds);
    }

    @Override
    public boolean existsOrganizationWithType(final String... types) {
        if (types == null || ArrayUtils.isEmpty(types)) {
            return false;
        }

        final List<String> typeList = Arrays.asList(types);
        Organization org = findOrganization(o -> typeList.contains(o.getDvcsType()));

        return org != null;
    }

    @Override
    public Collection<String> getAllProjectKeysFromChangesetsInOrganization(int orgId) {
        return organizationQueryDslFacade.getAllProjectKeysFromChangesetsInOrganization(orgId);
    }

    @Override
    public Iterable<String> getCurrentlyLinkedProjects(int orgId) {
        return organizationQueryDslFacade.getCurrentlyLinkedProjects(orgId);
    }

    @Override
    public long linkProject(int orgId, String projectKey) {
        return organizationQueryDslFacade.linkProject(orgId, projectKey);
    }

    @Override
    public void updateLinkedProjects(int orgId, Iterable<String> newProjectKeys) {
        organizationQueryDslFacade.updateLinkedProjects(orgId, newProjectKeys);
    }

    @Override
    public Organization findIntegratedAccount() {
        return findOrganization(Organization::isIntegratedAccount);
    }

    @Override
    public void remove(int organizationId) {
        organizationQueryDslFacade.remove(organizationId);
        clearCache();
    }

    @Override
    public Organization save(final Organization organization) {
        Organization org = organizationAOFacade.save(organization);

        // if operation succeeds then clear the cache otherwise do not clear the cache to keep some stale data for the users
        if (org != null) {
            clearCache();
        }
        return org;
    }

    @Override
    public void setDefaultGroupsSlugs(int orgId, Collection<String> groupsSlugs) {
        organizationAOFacade.updateDefaultGroupsSlugs(orgId, groupsSlugs);

        // if operation succeeds then clear the cache otherwise do not clear the cache to keep some stale data for the users
        clearCache();
    }

    @PostConstruct
    public void registerListener() {
        eventPublisher.register(this);
    }

    @PreDestroy
    public void unregisterListener() {
        eventPublisher.unregister(this);
    }

    /**
     * Listen to ImportCompletedEvent which is raised after a XML is imported in the Integration tests or by the system
     * so that the cache can be cleared.
     */
    @EventListener
    public void onImportCompleted(final ImportCompletedEvent event) {
        log.warn("onImportCompleted - clearing cache !!");
        clearCache();
    }

    private void clearCache() {
        organizationsCache.reset();
    }

    private List<Organization> getAllCachedOrgs() {
        return organizationsCache.get();
    }

    private Organization findOrganization(Predicate<Organization> predicate) {
        final List<Organization> orgs = getAllCachedOrgs();
        Organization matchedOrg = Iterables.find(orgs, predicate, null);
        return matchedOrg != null ? new Organization(matchedOrg) : null;
    }

    private List<Organization> cloneOrgs(@Nonnull Iterable<Organization> orgs) {
        return Lists.newArrayList(Iterables.transform(orgs, Organization::new));
    }
}
