package com.atlassian.jira.plugins.dvcs.util.impl;

import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.plugins.dvcs.util.HelpLinkRenderer;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import static java.util.Objects.requireNonNull;

@Component
@ParametersAreNonnullByDefault
public class HelpLinkRendererImpl implements HelpLinkRenderer {

    private static final String HELP_LINK_FORMAT = "<a href=\"%s\" target=\"_blank\">%s</a>";

    private final HelpUrls helpUrls;

    @Autowired
    public HelpLinkRendererImpl(@ComponentImport final HelpUrls helpUrls) {
        this.helpUrls = requireNonNull(helpUrls);
    }

    @Nonnull
    @Override
    public String render(final String helpUrlKey) {
        final HelpUrl helpUrl = helpUrls.getUrl(helpUrlKey);
        return String.format(HELP_LINK_FORMAT, helpUrl.getUrl(), helpUrl.getTitle());
    }
}
