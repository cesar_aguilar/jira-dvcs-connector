package com.atlassian.jira.plugins.dvcs.streams;

import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFile;
import com.atlassian.jira.plugins.dvcs.model.DvcsUser;
import com.atlassian.jira.plugins.dvcs.model.GlobalFilter;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.ChangesetService;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.jira.plugins.dvcs.webwork.IssueAndProjectKeyManager;
import com.atlassian.jira.plugins.dvcs.webwork.IssueLinker;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerbs;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsException;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.CancelledException;
import com.atlassian.streams.spi.StreamsActivityProvider;
import com.atlassian.streams.spi.UserProfileAccessor;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static com.atlassian.streams.api.ActivityObjectTypes.STANDARD_IRI_BASE;
import static com.atlassian.streams.api.ActivityObjectTypes.newTypeFactory;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.Filters.getIsValues;
import static com.atlassian.streams.spi.Filters.getNotValues;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

@Scanned
public class DvcsStreamsActivityProvider implements StreamsActivityProvider {
    public static final String STREAMS_EXTERNAL_FEED_TITLE_KEY = "streams.external.feed.title";
    public static final String SUMMARY_TEMPLATE_NAME = "/templates/activityentry-summary.vm";
    public static final String TITLE_TEMPLATE_NAME = "/templates/activityentry-title.vm";
    private static final Logger log = LoggerFactory.getLogger(DvcsStreamsActivityProvider.class);
    private static final String STATUS = "status";
    private final ApplicationProperties applicationProperties;
    private final ChangesetService changesetService;
    private final I18nResolver i18nResolver;
    private final IssueAndProjectKeyManager issueAndProjectKeyManager;
    private final IssueLinker issueLinker;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final PermissionManager permissionManager;
    private final ProjectManager projectManager;
    private final RepositoryService repositoryService;
    private final TemplateRenderer templateRenderer;
    private final UserProfileAccessor userProfileAccessor;

    public DvcsStreamsActivityProvider(
            @ComponentImport final ApplicationProperties applicationProperties,
            @ComponentImport final I18nResolver i18nResolver,
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final ProjectManager projectManager,
            @ComponentImport final TemplateRenderer templateRenderer,
            @ComponentImport final UserProfileAccessor userProfileAccessor,
            final ChangesetService changesetService,
            final IssueAndProjectKeyManager issueAndProjectKeyManager,
            final IssueLinker issueLinker,
            final RepositoryService repositoryService) {
        this.applicationProperties = checkNotNull(applicationProperties);
        this.changesetService = checkNotNull(changesetService);
        this.i18nResolver = checkNotNull(i18nResolver);
        this.issueAndProjectKeyManager = checkNotNull(issueAndProjectKeyManager);
        this.issueLinker = checkNotNull(issueLinker);
        this.jiraAuthenticationContext = checkNotNull(jiraAuthenticationContext);
        this.permissionManager = checkNotNull(permissionManager);
        this.projectManager = checkNotNull(projectManager);
        this.repositoryService = checkNotNull(repositoryService);
        this.templateRenderer = checkNotNull(templateRenderer);
        this.userProfileAccessor = checkNotNull(userProfileAccessor);
    }

    private Iterable<StreamsEntry> transformEntries(final ActivityRequest activityRequest,
                                                    final Iterable<Changeset> changesetEntries, final AtomicBoolean cancelled)
            throws StreamsException {
        final List<StreamsEntry> entries = new ArrayList<>();
        final Set<String> alreadyAddedChangesetRawNodes = new HashSet<>(entries.size(), 1.0F);

        for (final Changeset changeset : changesetEntries) {
            if (cancelled.get()) {
                throw new CancelledException();
            } else {
                // https://sdog.jira.com/browse/BBC-308; without this check we would be adding visually same items
                // to activity stream
                if (!alreadyAddedChangesetRawNodes.contains(getNode(changeset))) {
                    StreamsEntry streamsEntry = toStreamsEntry(activityRequest, changeset);
                    if (streamsEntry != null) {
                        entries.add(streamsEntry);
                        alreadyAddedChangesetRawNodes.add(getNode(changeset));
                    }
                }
            }
        }
        return entries;
    }

    private String getNode(final Changeset changeset) {
        String node = changeset.getRawNode();
        // if we don't have raw node e.g. for Github , we use node
        if (StringUtils.isEmpty(node)) {
            node = changeset.getNode();
        }

        return node;
    }

    /**
     * Renders the given Velocity template.
     *
     * @param template the template to render
     * @param model    the view model from which to populate the template
     * @return an empty string if there was a rendering error
     */
    private String renderTemplate(final String template, final Map<String, Object> model) {
        final StringWriter sw = new StringWriter();

        try {
            templateRenderer.render(template, model, sw);
        } catch (final IOException e) {
            log.warn(e.getMessage(), e);
        }

        return sw.toString();
    }

    /**
     * Transforms a single {@link com.atlassian.jira.plugins.dvcs.activeobjects.v2.IssueMapping}
     * into a {@link com.atlassian.streams.api.StreamsEntry}.
     *
     * @param changeset the changeset entry
     * @return the transformed streams entry
     */
    private StreamsEntry toStreamsEntry(final ActivityRequest activityRequest, final Changeset changeset) {
        final Repository repository = repositoryService.get(changeset.getRepositoryId());

        if (repository == null) {
            return null;
        }

        final StreamsEntry.ActivityObject activityObject = new StreamsEntry.ActivityObject(StreamsEntry.ActivityObject.params()
                .id(changeset.getNode()).alternateLinkUri(URI.create(""))
                .activityObjectType(newTypeFactory(STANDARD_IRI_BASE).newType(STATUS)));

        final String commitUrl = changesetService.getCommitUrl(repository, changeset);
        final DvcsUser user = repositoryService.getUser(repository, changeset.getAuthor(), changeset.getRawAuthor());

        StreamsEntry.Renderer renderer = new StreamsEntry.Renderer() {
            @Override
            public Html renderTitleAsHtml(final StreamsEntry entry) {
                final Map<String, Object> model = new HashMap<>();
                model.put("changeset", changeset);
                model.put("user_name", user.getFullName());
                model.put("login", user.getUsername());
                model.put("user_url", user.getUrl());
                model.put("commit_url", commitUrl);
                return new Html(renderTemplate(TITLE_TEMPLATE_NAME, model));
            }

            public Option<Html> renderSummaryAsHtml(final URI baseUri, final StreamsEntry entry) {
                return renderSummaryAsHtml(entry);
            }

            @Override
            public Option<Html> renderSummaryAsHtml(final StreamsEntry entry) {
                return Option.none();
            }

            public Option<Html> renderContentAsHtml(final URI baseUri, final StreamsEntry entry) {
                return renderContentAsHtml(entry);
            }

            @Override
            public Option<Html> renderContentAsHtml(final StreamsEntry entry) {
                final Map<String, Object> model = new HashMap<>();

                final Map<ChangesetFile, String> fileCommitUrls = changesetService.getFileCommitUrls(repository, changeset);
                model.put("file_commit_urls", fileCommitUrls);
                model.put("issue_linker", issueLinker);
                model.put("changeset", changeset);
                model.put("commit_url", commitUrl);
                model.put("max_visible_files", Changeset.MAX_VISIBLE_FILES);
                return some(new Html(renderTemplate(SUMMARY_TEMPLATE_NAME, model)));
            }
        };

        UserProfile userProfile = getUserProfile(activityRequest, user);

        return new StreamsEntry(StreamsEntry.params()
                .id(URI.create(""))
                .postedDate(new DateTime(changeset.getDate()))
                .authors(ImmutableNonEmptyList.of(userProfile))
                .addActivityObject(activityObject)
                .verb(ActivityVerbs.update())
                .alternateLinkUri(URI.create(""))
                .renderer(renderer)
                .applicationType(applicationProperties.getDisplayName()), i18nResolver);
    }

    private UserProfile getUserProfile(final ActivityRequest activityRequest, final DvcsUser user) {
        if (user != null && user.getAvatar() != null && user.getAvatar().startsWith("https")) {
            try {
                URI uri = new URI(user.getAvatar());
                return new UserProfile.Builder("").profilePictureUri(Option.option(uri)).build();
            } catch (URISyntaxException e) {
                // we use anonymous profile
                return userProfileAccessor.getAnonymousUserProfile(activityRequest.getContextUri());
            }
        } else {
            return userProfileAccessor.getAnonymousUserProfile(activityRequest.getContextUri());
        }
    }

    @Override
    public CancellableTask<StreamsFeed> getActivityFeed(final ActivityRequest activityRequest) throws StreamsException {
        final GlobalFilter gf = new GlobalFilter();
        //get all changeset entries that match the specified activity filters
        gf.setInProjects(includeHistoricalProjectKeys(getInProjectsByPermission(getIsValues(activityRequest.getStandardFilters().get(PROJECT_KEY)))));
        gf.setNotInProjects(includeHistoricalProjectKeys(getNotValues(activityRequest.getStandardFilters().get(PROJECT_KEY))));
        gf.setInUsers(getIsValues(activityRequest.getStandardFilters().get(USER.getKey())));
        gf.setNotInUsers(getNotValues(activityRequest.getStandardFilters().get(USER.getKey())));
        gf.setInIssues(includeHistoricalIssueKeys(getIsValues(activityRequest.getStandardFilters().get(ISSUE_KEY.getKey()))));
        gf.setNotInIssues(includeHistoricalIssueKeys(getNotValues(activityRequest.getStandardFilters().get(ISSUE_KEY.getKey()))));
        log.debug("GlobalFilter: " + gf);

        return new CancellableTask<StreamsFeed>() {
            private final AtomicBoolean cancelled = new AtomicBoolean(false);

            @Override
            public StreamsFeed call() throws Exception {
                Iterable<StreamsEntry> streamEntries = new ArrayList<>();
                if (gf.getInProjects() != null && gf.getInProjects().iterator().hasNext()) {
                    final Iterable<Changeset> latestChangesets =
                            changesetService.getLatestChangesets(activityRequest.getMaxResults(), gf);
                    if (cancelled.get()) {
                        throw new CancelledException();
                    }
                    log.debug("Found changeset entries: {}", latestChangesets);

                    final List<Changeset> changesetDetails =
                            changesetService.getChangesetsWithFileDetails(newArrayList(latestChangesets));
                    log.debug("Loaded details for changeset entries: {}", changesetDetails);

                    streamEntries = transformEntries(activityRequest, changesetDetails, cancelled);
                }
                return new StreamsFeed(i18nResolver.getText(STREAMS_EXTERNAL_FEED_TITLE_KEY), streamEntries, Option.<String>none());
            }

            @Override
            public Result cancel() {
                cancelled.set(true);
                return Result.CANCELLED;
            }
        };
    }

    private Set<String> includeHistoricalProjectKeys(Iterable<String> projectKeys) {
        final Set<String> result = new HashSet<>();
        for (String projectKey : projectKeys) {
            result.addAll(issueAndProjectKeyManager.getAllProjectKeys(projectKey));
        }
        return result;
    }

    private Set<String> includeHistoricalIssueKeys(Iterable<String> issueKeys) {
        final Set<String> result = new HashSet<>();
        for (String issueKey : issueKeys) {
            result.addAll(issueAndProjectKeyManager.getAllIssueKeys(issueKey));
        }
        return result;
    }

    private List<String> getInProjectsByPermission(final Collection<String> inProjectsList) {
        final Collection<Project> projectsToCheckPermission;

        if (isEmpty(inProjectsList)) {
            projectsToCheckPermission = projectManager.getProjectObjects();
        } else {
            projectsToCheckPermission = inProjectsList.stream().map(projectManager::getProjectObjByKey).collect(toList());
        }

        final ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        return projectsToCheckPermission.stream()
                .filter(Objects::nonNull)
                .filter(project -> permissionManager.hasPermission(VIEW_DEV_TOOLS, project, user))
                .map(Project::getKey)
                .collect(toList());
    }
}
