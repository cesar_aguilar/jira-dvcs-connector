package com.atlassian.jira.plugins.dvcs.rest.internal.v1;

import com.atlassian.annotations.Internal;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@AnonymousAllowed
@Internal
@Path("/healthcheck")
public class HealthcheckResource {
    @GET
    @Path("/")
    public Response overallHealthcheck() {
        return Response.noContent().build();
    }
}
