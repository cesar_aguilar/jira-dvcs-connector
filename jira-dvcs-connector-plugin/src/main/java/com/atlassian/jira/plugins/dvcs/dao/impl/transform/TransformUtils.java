package com.atlassian.jira.plugins.dvcs.dao.impl.transform;

public final class TransformUtils {

    private TransformUtils() {
    }

    public static Class transformPayloadStringToClass(String payloadTypeString) {
        try {
            return Class.forName(payloadTypeString);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    ;
}
