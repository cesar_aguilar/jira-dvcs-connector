package com.atlassian.jira.plugins.dvcs.rest.internal.v1;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.plugins.dvcs.featurediscovery.FeatureDiscoveryService;
import com.atlassian.jira.plugins.dvcs.model.Flag;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static java.util.Objects.requireNonNull;

/**
 * REST resource for operations related to the user.
 */
@Internal
@ParametersAreNonnullByDefault
@Path("/user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    private final FeatureDiscoveryService featureDiscoveryService;

    @Inject
    public UserResource(final FeatureDiscoveryService featureDiscoveryService) {
        this.featureDiscoveryService = requireNonNull(featureDiscoveryService);
    }

    /**
     * Get the "has seen feature discovery" flag for the logged in user.
     * has seen the feature discovery tour for the DVCS Connector.
     *
     * @return HTTP.200 with the flag value
     *
     * @response.representation.200.doc Returned with current the value of the flag for the request user
     */
    @GET
    @Path("/flag/featureDiscoverySeen")
    public Response getHasUserSeenFeatureDiscovery() {
        return Response.ok(Flag.of(featureDiscoveryService.hasUserSeenFeatureDiscovery())).build();
    }

    /**
     * Set the "has seen feature discovery" flag for the logged in user
     *
     * @return HTTP.204 if operation was successful.
     *
     * @response.representation.204.doc Returned if the update was successful
     * @response.representation.400.doc Returned if no flag was provided
     */
    @PUT
    @Path("/flag/featureDiscoverySeen")
    public Response setHasUserSeenFeatureDiscovery(@Nonnull final Flag flag) {
        requireNonNull(flag, "A flag value is required");

        featureDiscoveryService.markUserAsHavingSeenFeatureDiscovery(flag.getValue());

        return Response.noContent().build();
    }
}
