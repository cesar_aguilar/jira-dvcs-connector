package com.atlassian.jira.plugins.dvcs.webwork;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fusion.aci.api.feature.AciDarkFeatures;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsConfigPageShownAnalyticsEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.Source;
import com.atlassian.jira.plugins.dvcs.bbrebrand.BitbucketRebrandDarkFeature;
import com.atlassian.jira.plugins.dvcs.featurediscovery.FeatureDiscoveryService;
import com.atlassian.jira.plugins.dvcs.listener.PluginFeatureDetector;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.service.InvalidOrganizationManager;
import com.atlassian.jira.plugins.dvcs.service.InvalidOrganizationsManagerImpl;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.service.remote.SyncDisabledHelper;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.BitbucketCommunicator;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;
import com.atlassian.jira.plugins.dvcs.spi.github.GithubCommunicator;
import com.atlassian.jira.plugins.dvcs.spi.githubenterprise.GithubEnterpriseCommunicator;
import com.atlassian.jira.projects.unlicensed.UnlicensedProjectPageRenderer;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.software.api.conditions.SoftwareGlobalAdminCondition;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.ActionViewDataMappings;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.config.properties.APKeys.JIRA_TITLE;
import static com.atlassian.jira.plugins.dvcs.ProjectTypeKeys.SOFTWARE;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyMap;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Webwork action for the DVCS Connector admin page
 */
@Scanned
public class ConfigureDvcsOrganizations extends JiraWebActionSupport {
    
    @VisibleForTesting
    public static final String GIT_HUB_ENTERPRISE_NAME = "GitHub Enterprise";
    @VisibleForTesting
    static final String BITBUCKET_CLOUD_NAME = "Bitbucket Cloud";
    @VisibleForTesting
    static final String BITBUCKET_CLOUD_NAME_NO_REBRAND = "Bitbucket";
    @VisibleForTesting
    static final String GIT_HUB_NAME = "GitHub";
    @VisibleForTesting
    static final String SYNCHRONIZATION_DISABLED_TITLE_KEY =
            "com.atlassian.jira.plugins.dvcs.configure-dvcs-organizations.synchronization-disabled-title";
    @VisibleForTesting
    static final String SYNCHRONIZATION_ALL_DISABLED_TITLE_KEY =
            "com.atlassian.jira.plugins.dvcs.configure-dvcs-organizations.synchronization-all-disabled-title";
    @VisibleForTesting
    static final String SYNCHRONIZATION_DISABLED_MESSAGE_KEY =
            "com.atlassian.jira.plugins.dvcs.configure-dvcs-organizations.synchronization-disabled-message";
    @VisibleForTesting
    static final String SYNCHRONIZATION_ALL_DISABLED_MESSAGE_KEY =
            "com.atlassian.jira.plugins.dvcs.configure-dvcs-organizations.synchronization-all-disabled-message";
    @VisibleForTesting
    static final String UNLICENSED = "unlicensed";

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigureDvcsOrganizations.class);
    private static final String CONTENT_DATA_MAP_KEY = "content";
    private static final String DEFAULT_SOURCE = CommonDvcsConfigurationAction.DEFAULT_SOURCE;
    private static final String UNLICENSED_PAGE_WEB_RESOURCE =
            "com.atlassian.jira.plugins.jira-bitbucket-connector-plugin:unlicensed-page-resources";
    private final BitbucketRebrandDarkFeature bitbucketRebrandDarkFeature;
    private final EventPublisher eventPublisher;
    private final FeatureManager featureManager;
    private final I18nHelper i18nHelper;
    private final InvalidOrganizationManager invalidOrganizationsManager;
    private final OrganizationService organizationService;
    private final PageBuilderService pageBuilderService;
    private final PluginFeatureDetector pluginFeatureDetector;
    private final SoftwareGlobalAdminCondition softwareGlobalAdminCondition;
    private final SyncDisabledHelper syncDisabledHelper;
    private final UnlicensedProjectPageRenderer unlicensedProjectPageRenderer;
    private final ApplicationProperties applicationProperties;
    private final FeatureDiscoveryService featureDiscoveryService;

    private String postCommitRepositoryType;
    private String source = DEFAULT_SOURCE;

    @Autowired
    public ConfigureDvcsOrganizations(
            @ComponentImport final EventPublisher eventPublisher,
            @ComponentImport final FeatureManager featureManager,
            @ComponentImport final I18nHelper i18nhelper,
            @ComponentImport final PageBuilderService pageBuilderService,
            @ComponentImport final PluginSettingsFactory pluginSettingsFactory,
            @ComponentImport final UnlicensedProjectPageRenderer unlicensedProjectPageRenderer,
            @ComponentImport final ApplicationProperties applicationProperties,
            final BitbucketRebrandDarkFeature bitbucketRebrandDarkFeature,
            final FeatureDiscoveryService featureDiscoveryService,
            final OrganizationService organizationService,
            final PluginFeatureDetector featuresDetector,
            final SoftwareGlobalAdminCondition softwareGlobalAdminCondition,
            final SyncDisabledHelper syncDisabledHelper) {
        this.bitbucketRebrandDarkFeature = checkNotNull(bitbucketRebrandDarkFeature);
        this.eventPublisher = checkNotNull(eventPublisher);
        this.featureManager = checkNotNull(featureManager);
        this.i18nHelper = checkNotNull(i18nhelper);
        this.invalidOrganizationsManager = new InvalidOrganizationsManagerImpl(pluginSettingsFactory);
        this.organizationService = checkNotNull(organizationService);
        this.pageBuilderService = checkNotNull(pageBuilderService);
        this.pluginFeatureDetector = checkNotNull(featuresDetector);
        this.softwareGlobalAdminCondition = checkNotNull(softwareGlobalAdminCondition);
        this.syncDisabledHelper = checkNotNull(syncDisabledHelper);
        this.unlicensedProjectPageRenderer = checkNotNull(unlicensedProjectPageRenderer);
        this.applicationProperties = checkNotNull(applicationProperties);
        this.featureDiscoveryService = checkNotNull(featureDiscoveryService);
    }

    @Override
    protected void doValidation() {
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        LOGGER.debug("Configure organization default action.");
        eventPublisher.publish(new DvcsConfigPageShownAnalyticsEvent(getSourceOrDefault()));
        return INPUT;
    }

    public String doDefault() throws Exception {
        if (softwareGlobalAdminCondition.shouldDisplay(emptyMap())) {
            return doExecute();
        }

        return UNLICENSED;
    }

    @ActionViewDataMappings(UNLICENSED)
    public Map<String, Object> getUnlicensedViewData() {
        final RequiredResources requiredResources = pageBuilderService.assembler().resources()
                .requireWebResource(UNLICENSED_PAGE_WEB_RESOURCE);
        return ImmutableMap.of(CONTENT_DATA_MAP_KEY,
                unlicensedProjectPageRenderer.render(requiredResources, SOFTWARE));
    }

    public List<Organization> loadOrganizations() {
        final List<Organization> allOrganizations = organizationService.getAll(true);
        sort(allOrganizations);
        return allOrganizations;
    }

    public boolean isInvalidOrganization(Organization organization) {
        return !invalidOrganizationsManager.isOrganizationValid(organization.getId());
    }

    /**
     * Custom sorting of organizations - integrated accounts are displayed on top.
     */
    private void sort(List<Organization> allOrganizations) {
        Collections.sort(allOrganizations, (org1, org2) -> {
            // integrated accounts has precedence
            if (org1.isIntegratedAccount() && !org2.isIntegratedAccount()) {
                return -1;

            } else if (!org1.isIntegratedAccount() && org2.isIntegratedAccount()) {
                return +1;

            } else {
                // by default compares via name
                return org1.getName().toLowerCase().compareTo(org2.getName().toLowerCase());
            }

        });
    }

    public String getPostCommitRepositoryType() {
        return postCommitRepositoryType;
    }

    public void setPostCommitRepositoryType(String postCommitRepositoryType) {
        this.postCommitRepositoryType = postCommitRepositoryType;
    }

    public boolean isOnDemandLicense() {
        return featureManager.isEnabled(CoreFeatures.ON_DEMAND);
    }

    public boolean isUserInvitationsEnabled() {
        return pluginFeatureDetector.isUserInvitationsEnabled();
    }

    public boolean isIntegratedAccount(Organization org) {
        return org.isIntegratedAccount();
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Source getSourceOrDefault() {
        return isNotBlank(source) ? Source.valueOf(source.toUpperCase()) : Source.UNKNOWN;
    }

    public boolean isGitHubSyncDisabled() {
        return syncDisabledHelper.isGithubSyncDisabled();
    }

    public boolean isBitbucketSyncDisabled() {
        return syncDisabledHelper.isBitbucketSyncDisabled();
    }

    public boolean isGitHubEnterpriseSyncDisabled() {
        return syncDisabledHelper.isGithubEnterpriseSyncDisabled();
    }

    public boolean isAnySyncDisabled() {
        return syncDisabledHelper.isBitbucketSyncDisabled()
                || syncDisabledHelper.isGithubSyncDisabled()
                || syncDisabledHelper.isGithubEnterpriseSyncDisabled();
    }

    public boolean isAllSyncDisabled() {
        return syncDisabledHelper.isBitbucketSyncDisabled()
                && syncDisabledHelper.isGithubSyncDisabled()
                && syncDisabledHelper.isGithubEnterpriseSyncDisabled();
    }

    public String getSyncDisabledWarningTitle() {
        if (!isAnySyncDisabled()) {
            return null;
        }

        if (syncDisabledHelper.isSyncDisabled()) {
            // All synchronizations are disabled
            return i18nHelper.getText(SYNCHRONIZATION_ALL_DISABLED_TITLE_KEY);
        }

        return i18nHelper.getText(SYNCHRONIZATION_DISABLED_TITLE_KEY, getDisabledSystemsList());
    }

    public String getSyncDisabledWarningMessage() {
        if (!isAnySyncDisabled()) {
            return null;
        }

        if (syncDisabledHelper.isSyncDisabled()) {
            // All synchronizations are disabled
            return i18nHelper.getText(SYNCHRONIZATION_ALL_DISABLED_MESSAGE_KEY);
        }

        return i18nHelper.getText(SYNCHRONIZATION_DISABLED_MESSAGE_KEY, getDisabledSystemsList());
    }

    private String getDisabledSystemsList() {
        String bitbucketCloudProductName = BITBUCKET_CLOUD_NAME;

        if (!bitbucketRebrandDarkFeature.isBitbucketRebrandEnabled()) {
            bitbucketCloudProductName = BITBUCKET_CLOUD_NAME_NO_REBRAND;
        }

        return Joiner.on("/").skipNulls().join(
                syncDisabledHelper.isBitbucketSyncDisabled() ? bitbucketCloudProductName : null,
                syncDisabledHelper.isGithubSyncDisabled() ? GIT_HUB_NAME : null,
                syncDisabledHelper.isGithubEnterpriseSyncDisabled() ? GIT_HUB_ENTERPRISE_NAME : null
        );
    }

    public boolean isSyncDisabled(String dvcsType) {
        if (BitbucketCommunicator.BITBUCKET.equals(dvcsType)) {
            return isBitbucketSyncDisabled();
        }

        if (GithubCommunicator.GITHUB.equals(dvcsType)) {
            return isGitHubSyncDisabled();
        }

        if (GithubEnterpriseCommunicator.GITHUB_ENTERPRISE.equals(dvcsType)) {
            return isGitHubEnterpriseSyncDisabled();
        }

        return false;
    }

    public boolean isBitbucketRebrandEnabled() {
        return bitbucketRebrandDarkFeature.isBitbucketRebrandEnabled();
    }

    public boolean isAciEnabled() {
        return featureManager.isEnabled(AciDarkFeatures.ACI_ENABLED_FEATURE_KEY);
    }

    public String getBitbucketOverrideUrl() {
        return BitbucketDetails.getHostUrl();
    }

    public String getJiraInstanceTitle() {
        return applicationProperties.getString(JIRA_TITLE);
    }

    public boolean shouldShowFeatureDiscoveryForUser() {
        return !featureDiscoveryService.hasUserSeenFeatureDiscovery();
    }
}
