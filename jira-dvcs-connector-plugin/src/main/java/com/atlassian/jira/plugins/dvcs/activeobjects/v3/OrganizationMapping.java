package com.atlassian.jira.plugins.dvcs.activeobjects.v3;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.Table;

@Preload
@Table("OrganizationMapping")
public interface OrganizationMapping extends Entity {

    public static final String HOST_URL = "HOST_URL";
    public static final String NAME = "NAME";
    public static final String DVCS_TYPE = "DVCS_TYPE";
    public static final String AUTOLINK_NEW_REPOS = "AUTOLINK_NEW_REPOS";
    @Deprecated
    public static final String ADMIN_USERNAME = "ADMIN_USERNAME";
    @Deprecated
    public static final String ADMIN_PASSWORD = "ADMIN_PASSWORD";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String SMARTCOMMITS_FOR_NEW_REPOS = "SMARTCOMMITS_FOR_NEW_REPOS";
    public static final String DEFAULT_GROUPS_SLUGS = "DEFAULT_GROUPS_SLUGS"; // serialized, separated by ";"
    public static final String APPROVAL_STATE = "APPROVAL_STATE";

    public static final String OAUTH_KEY = "OAUTH_KEY";
    public static final String OAUTH_SECRET = "OAUTH_SECRET";

    public static final String PRINCIPAL_ID = "PRINCIPAL_ID";

    String getHostUrl();

    void setHostUrl(String hostUrl);

    String getName();

    void setName(String name);

    @Indexed
    String getDvcsType();

    void setDvcsType(String dvcsType);

    boolean isAutolinkNewRepos();

    void setAutolinkNewRepos(boolean autolinkNewRepos);

    @Deprecated
    String getAdminUsername();

    @Deprecated
    void setAdminUsername(String adminUsername);

    @Deprecated
    String getAdminPassword();

    @Deprecated
    void setAdminPassword(String adminPassword);

    String getAccessToken();

    void setAccessToken(String accessToken);

    boolean isSmartcommitsForNewRepos();

    void setSmartcommitsForNewRepos(boolean enabled);

    String getDefaultGroupsSlugs();

    void setDefaultGroupsSlugs(String slugs);

    String getApprovalState();

    void setApprovalState(String approvalState);

    String getOauthKey();

    void setOauthKey(String key);

    String getOauthSecret();

    void setOauthSecret(String secret);

    String getPrincipalId();

    void setPrincipalId(String id);
}
