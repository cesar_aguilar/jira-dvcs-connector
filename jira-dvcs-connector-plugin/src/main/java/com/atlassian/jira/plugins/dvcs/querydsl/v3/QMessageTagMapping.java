package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.PrimaryKey;


public class QMessageTagMapping extends EnhancedRelationalPathBase<QMessageTagMapping> {

    private static final String AO_TABLE_NAME = "AO_E8B6CC_MESSAGE_TAG";
    private static final long serialVersionUID = -1329811996708193021L;

    public final NumberPath<Integer> ID = createInteger("ID");
    public final NumberPath<Integer> MESSAGE_ID = createIntegerCol("MESSAGE_ID").notNull().build();

    public final PrimaryKey<QMessageTagMapping> MESSAGE_TAG_PK = createPrimaryKey(ID);
    public final StringPath TAG = createString("TAG");

    public QMessageTagMapping() {
        super(QMessageTagMapping.class, AO_TABLE_NAME);
    }

}
