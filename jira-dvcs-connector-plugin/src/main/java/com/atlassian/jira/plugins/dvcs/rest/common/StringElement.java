package com.atlassian.jira.plugins.dvcs.rest.common;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A simple container of a String
 */
@XmlRootElement
public class StringElement {

    public static StringElement of(String value) {
        return new StringElement(value);
    }

    @XmlElement
    private String value;

    public StringElement(String value) {
        this.value = value;
    }

    public StringElement() {
    }

    public String getValue() {
        return value;
    }
}
