package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;


public class QBranchHeadMapping extends EnhancedRelationalPathBase<QBranchHeadMapping> {

    private static final String AO_TABLE_NAME = "AO_E8B6CC_BRANCH_HEAD_MAPPING";
    private static final long serialVersionUID = -6912223984558590959L;

    public final StringPath BRANCH_NAME = createString("BRANCH_NAME");
    public final StringPath HEAD = createString("HEAD");
    public final NumberPath<Integer> ID = createInteger("ID");
    public final NumberPath<Integer> REPOSITORY_ID = createInteger("REPOSITORY_ID");

    public final com.querydsl.sql.PrimaryKey<QBranchHeadMapping> BRANCHHEADMAPPING_PK = createPrimaryKey(ID);

    public QBranchHeadMapping() {
        super(QBranchHeadMapping.class, AO_TABLE_NAME);
    }

}