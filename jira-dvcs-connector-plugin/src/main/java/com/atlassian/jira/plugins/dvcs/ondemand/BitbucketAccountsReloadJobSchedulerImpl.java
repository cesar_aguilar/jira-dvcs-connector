package com.atlassian.jira.plugins.dvcs.ondemand;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.status.JobDetails;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Date;

import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.System.currentTimeMillis;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

@Component
public class BitbucketAccountsReloadJobSchedulerImpl implements BitbucketAccountsReloadJobScheduler {
    @VisibleForTesting
    static final JobId JOB_ID = JobId.of("bitbucket-accounts-reload");
    @VisibleForTesting
    static final JobRunnerKey JOB_RUNNER_KEY = JobRunnerKey.of(BitbucketAccountsReloadJobScheduler.class.getName());
    @VisibleForTesting
    // Having this delay minimises the impact of a race in the schedule method
    static final long DELAY = MILLISECONDS.convert(15, SECONDS);
    private static final Logger log = LoggerFactory.getLogger(BitbucketAccountsReloadJobSchedulerImpl.class);
    private final JobRunner jobRunner;
    private final SchedulerService schedulerService;

    @Autowired
    public BitbucketAccountsReloadJobSchedulerImpl(
            @ComponentImport final SchedulerService schedulerService,
            final BitbucketAccountsReloadJobRunner jobRunner) {
        this.jobRunner = checkNotNull(jobRunner);
        this.schedulerService = checkNotNull(schedulerService);
    }

    @PostConstruct
    public void registerJobHandler() {
        schedulerService.registerJobRunner(JOB_RUNNER_KEY, jobRunner);
    }

    @PreDestroy
    public void unregisterJobHandler() {
        schedulerService.unregisterJobRunner(JOB_RUNNER_KEY);
    }

    @Override
    public void schedule() {
        final JobDetails jobDetails = schedulerService.getJobDetails(JOB_ID);
        if (jobDetails == null) {
            try {
                schedulerService.scheduleJob(JOB_ID, getJobConfig());
                log.debug("job is now scheduled: {}", schedulerService.getJobDetails(JOB_ID));
            } catch (SchedulerServiceException e) {
                throw new IllegalStateException(e);
            }
        } else {
            log.debug("job has been scheduled previously: {}", jobDetails);
        }
    }

    private JobConfig getJobConfig() {
        final Date runTime = new Date(currentTimeMillis() + DELAY);
        return JobConfig.forJobRunnerKey(JOB_RUNNER_KEY)
                .withRunMode(RUN_ONCE_PER_CLUSTER)
                .withSchedule(Schedule.runOnce(runTime));
    }
}
