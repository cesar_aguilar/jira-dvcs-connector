package com.atlassian.jira.plugins.dvcs.activeobjects.v1;

import net.java.ao.Entity;

/**
 * Active objects storage for the mapping between a bitbucket repository and a jira project.
 */
@Deprecated
public interface ProjectMapping extends Entity {
    String getRepositoryUri();

    void setRepositoryUri(String owner);

    String getProjectKey();

    void setProjectKey(String projectKey);

    String getUsername();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);
}
