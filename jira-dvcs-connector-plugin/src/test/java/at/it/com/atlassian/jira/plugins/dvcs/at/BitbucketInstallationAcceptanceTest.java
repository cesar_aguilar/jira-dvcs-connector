package at.it.com.atlassian.jira.plugins.dvcs.at;

import com.atlassian.fusion.aci.api.feature.AciDarkFeatures;
import com.atlassian.fusion.aci.test.ACITestClient;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.BitBucketConfigureOrganizationsPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.JiraApproveOrgPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.ConnectionSuccessfulDialog;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.FeatureDiscoveryDialog;
import com.atlassian.jira.plugins.dvcs.rest.OrganizationClient;
import com.atlassian.jira.plugins.dvcs.rest.UserClient;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants;
import com.atlassian.pageobjects.TestedProductFactory;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.google.common.collect.ImmutableMap;
import it.util.LocalDevOnlyPropertiesLoader;
import org.apache.commons.io.IOUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
/**
 * Basic smoke test that we can install DVCS connector (easy connect) in the unicorn environment AT run
 */
@Test(groups = {"com.atlassian.test.categories.OnDemandAcceptanceTest"})
public class BitbucketInstallationAcceptanceTest {

    private static final String TESTING_USER = "test_user";
    private static final JiraTestedProduct JIRA = TestedProductFactory.create(JiraTestedProduct.class);
    private static final String PRINCIPAL_UUID = "{dbc99d5a-d14a-41bd-a273-d42220228dds}";
    private static final String APPROVAL_URL_FOR_USER = JiraApproveOrgPage.JIRA_APPROVAL_PAGE_URL
            + "?principalUuid=" + PRINCIPAL_UUID + "&accountName=" + TESTING_USER;

    private WireMockServer wireMockServer;
    private UserClient userClient;
    private OrganizationClient organizationClient;
    private ACITestClient aciTestClient;

    @BeforeClass
    public void setup() throws Exception {
        LocalDevOnlyPropertiesLoader.loadLocalDevOnlyProperties();

        wireMockServer = new WireMockServer(wireMockConfig().port(12345));
        wireMockServer.start();
        JIRA.quickLoginAsAdmin();

        JIRA.backdoor().darkFeatures().enableForSite(AciDarkFeatures.ACI_ENABLED_FEATURE_KEY);
        JIRA.backdoor().darkFeatures().enableForSite(AciDarkFeatures.ACI_DISABLE_HOST_VALIDATION_FEATURE_KEY);

        userClient = new UserClient(JIRA.environmentData());
        userClient.setUserHasSeenFeatureDiscovery(false);

        organizationClient = new OrganizationClient(JIRA.environmentData());
        organizationClient.deleteAll();

        aciTestClient = new ACITestClient(getJiraBaseUrl(), ImmutableMap.of());
    }

    @AfterClass
    public void tearDown() {
        wireMockServer.stop();
        JIRA.backdoor().darkFeatures().disableForSite(AciDarkFeatures.ACI_ENABLED_FEATURE_KEY);
        JIRA.backdoor().darkFeatures().disableForSite(AciDarkFeatures.ACI_DISABLE_HOST_VALIDATION_FEATURE_KEY);
    }

    @Test
    public void bitbucketInitiatedInstallation_installsOrganization_duringHappyPath() throws Exception {
        // setup
        setupWireMock();

        // execute
        // Send the install payload to JIRA
        final String installJson = getJsonFromFile("testUserInstallPayload.json");
        aciTestClient.postInstall(BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID, installJson);

        // Navigate the browser to the approval page
        JIRA.getTester().getDriver().navigate().to(getJiraBaseUrl() + APPROVAL_URL_FOR_USER);
        final JiraApproveOrgPage approvalPage = JIRA.getPageBinder().bind(JiraApproveOrgPage.class);
        approvalPage.approve();

        // Bind the configure Organizations page where the approval page directed us
        final BitBucketConfigureOrganizationsPage configureOrganizationsPage
                = JIRA.getPageBinder().bind(BitBucketConfigureOrganizationsPage.class);

        // Configure the organization
        final ConnectionSuccessfulDialog connectionSuccessfulDialog
                = configureOrganizationsPage.getConnectionSuccessfulDialog();
        waitUntilTrue(connectionSuccessfulDialog.getAutoLinkRepositoryCheckbox().timed().isVisible());

        connectionSuccessfulDialog.getAutoLinkRepositoryCheckbox().click();
        connectionSuccessfulDialog.getSubmitButton().click();

        // Skip feature discovery
        final FeatureDiscoveryDialog featureDiscoveryDialog = configureOrganizationsPage.getFeatureDiscoveryDialog();
        waitUntilTrue(featureDiscoveryDialog.getCloseDialogLink().timed().isVisible());

        featureDiscoveryDialog.getCloseDialogLink().click();

        // verify
        final Optional<Organization> maybeOrganization
                = organizationClient.findOrganization(DvcsType.BITBUCKET.toString(), "test_user");

        assertThat("organization should have been created", maybeOrganization.isPresent(), equalTo(true));

        Organization organization = maybeOrganization.get();

        assertThat("smart commits should be on", organization.isSmartcommitsOnNewRepos(), equalTo(true));
        assertThat("auto link should be off", organization.isAutolinkNewRepos(), equalTo(false));

        boolean userHasSeenFeatureDiscovery = userClient.hasUserSeenFeatureDiscovery();
        assertThat("user should now have seen feature discovery", userHasSeenFeatureDiscovery, equalTo(true));
    }

    private String getJiraBaseUrl() {
        final String baseUrlExternalForm = JIRA.environmentData().getBaseUrl().toExternalForm();
        return baseUrlExternalForm.endsWith("/") ? baseUrlExternalForm : baseUrlExternalForm + "/";
    }

    private void setupWireMock() {
        final String userProfileJson = getJsonFromFile("testUserProfileResponse.json");

        final ResponseDefinitionBuilder responseDefinitionBuilder = new ResponseDefinitionBuilder();
        responseDefinitionBuilder.withBody(userProfileJson)
                .withHeader("content-type", APPLICATION_JSON);

        wireMockServer.stubFor(get(urlEqualTo("/api/1.0/users/" + TESTING_USER))
                .willReturn(responseDefinitionBuilder));

        wireMockServer.stubFor(get(urlMatching("/api/internal/account/.*/addons/jira-bitbucket-connector-plugin.*"))
                .willReturn(aResponse().withStatus(200)));
    }

    private String getJsonFromFile(String path) {
        try (InputStream jsonStream = BitbucketInstallationAcceptanceTest.class.getResourceAsStream(path)) {
            if (jsonStream == null) {
                throw new IllegalArgumentException("Cannot find resource at " + path);
            }
            return IOUtils.toString(jsonStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
