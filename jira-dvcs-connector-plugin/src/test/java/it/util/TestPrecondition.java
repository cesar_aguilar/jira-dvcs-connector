package it.util;

import org.testng.SkipException;

import static java.lang.String.format;

/**
 * Similar to a {@link TestRule} but will throw a {@link org.testng.SkipException} on fail
 * to skip a test rather than fail it.
 * <p>
 * Used to express a test precondition.
 */
public abstract class TestPrecondition {
    /**
     * Apply the precondition expressed by this instance.
     *
     * @throws SkipException If the precondition fails
     */
    public final void apply() throws SkipException {
        try {
            doApply();
        } catch (Exception e) {
            throw new SkipException(format("Precondition '%s' failed - %s", getClass().getSimpleName(), e.getMessage()));
        }
    }

    /**
     * Implementing classes perform precondition checking logic here
     */
    protected abstract void doApply() throws Exception;
}
