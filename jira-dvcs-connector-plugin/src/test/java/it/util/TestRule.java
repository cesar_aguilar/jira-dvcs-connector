package it.util;

import org.testng.Assert;

/**
 * An approximation to a JUnit Rule that can be used to capture and apply common pieces of test setup/teardown logic.
 * <p>
 * Converts any exception thrown into an {@link AssertionError} which will fail the test.
 */
public abstract class TestRule {

    /**
     * Apply the rule expressed by this instance.
     *
     * @throws AssertionError If the rule fails
     */
    public final void apply() throws AssertionError {
        try {
            doApply();
        } catch (Exception e) {
            Assert.fail(String.format("Unable to apply rule '%s'", getClass().getSimpleName()), e);
        }
    }

    /**
     * Implementing classes perform logic here
     */
    protected abstract void doApply() throws Exception;
}
