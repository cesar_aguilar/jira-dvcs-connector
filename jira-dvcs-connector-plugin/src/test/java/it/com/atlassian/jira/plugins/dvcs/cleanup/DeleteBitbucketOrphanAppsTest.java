package it.com.atlassian.jira.plugins.dvcs.cleanup;

import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints.ConsumerRemoteRestpoint;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.RemoteRequestorFactory.createDefaultRequestor;

/**
 * Delete the orphan OAuth Applications created by Webdriver tests for Bitbucket.
 */
public class DeleteBitbucketOrphanAppsTest extends DeleteOrphanAppsBaseTest {
    @Override
    protected void deleteOrphanOAuthApplications(final String repoOwner, final String repoPassword) {
        final ConsumerRemoteRestpoint consumerRemoteRestpoint = new ConsumerRemoteRestpoint(
                createDefaultRequestor(BitbucketDetails.getHostUrl(), repoOwner, repoPassword));

        consumerRemoteRestpoint.getConsumers(repoOwner)
                .stream()
                .filter(consumer -> isConsumerExpired(consumer.getName()))
                .forEach(consumer -> {
                    consumerRemoteRestpoint.deleteConsumer(repoOwner, consumer.getId().toString());
                });
    }
}
