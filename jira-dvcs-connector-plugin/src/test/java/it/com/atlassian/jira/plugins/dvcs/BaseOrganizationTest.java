package it.com.atlassian.jira.plugins.dvcs;

import com.atlassian.jira.plugins.dvcs.pageobjects.page.BaseConfigureOrganizationsPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

/**
 * Base class for BitBucket integration tests. Initializes the JiraTestedProduct and logs admin in.
 */
public abstract class BaseOrganizationTest<T extends BaseConfigureOrganizationsPage> extends DvcsWebDriverTestCase {
    protected T configureOrganizations;

    @BeforeMethod
    public void loginToJira() {
        // log in to JIRA
        configureOrganizations = JIRA.quickLoginAsAdmin(getConfigureOrganizationsPageClass());
        configureOrganizations.setJiraTestedProduct(JIRA);
        configureOrganizations.deleteAllOrganizations();
    }

    protected abstract Class<T> getConfigureOrganizationsPageClass();

    @AfterMethod
    public void logout() {
        JIRA.getTester().getDriver().manage().deleteAllCookies();
    }
}
