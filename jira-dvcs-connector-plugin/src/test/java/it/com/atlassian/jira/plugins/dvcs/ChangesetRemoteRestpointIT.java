package it.com.atlassian.jira.plugins.dvcs;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.pageobjects.config.EnvironmentBasedProductInstance;
import com.atlassian.jira.pageobjects.config.ProductInstanceBasedEnvironmentData;
import com.atlassian.jira.plugins.dvcs.base.resource.TimestampNameTestResource;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.rest.json.CredentialJson;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.BitbucketRemoteClient;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketChangesetPage;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConsumer;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketNewChangeset;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketRepository;
import com.atlassian.jira.plugins.dvcs.util.PasswordUtil;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import it.util.LocalDevOnlyPropertiesLoader;
import org.apache.commons.lang3.RandomStringUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static it.util.LocalGitRepo.configureRemoteOriginToBitbucket;
import static it.util.LocalGitRepo.createLocalTempGitRepo;
import static it.util.LocalGitRepo.removeLocalGitRepo;
import static it.util.TestAccounts.DVCS_CONNECTOR_TEST_ACCOUNT;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class ChangesetRemoteRestpointIT {

    private static final String BB_REPO_NAME_PREFIX = "test404commitsrepo_";
    private static final String DVCS_REPO_OWNER = DVCS_CONNECTOR_TEST_ACCOUNT;
    private static final String PROJECT_KEY = "TPC404";

    private static final JIRAEnvironmentData ENVIRONMENT_DATA =
            new ProductInstanceBasedEnvironmentData(new EnvironmentBasedProductInstance());
    private static final Backdoor BACKDOOR = new Backdoor(ENVIRONMENT_DATA);

    private static final DvcsConnectorRestClient DVCS_CONNECTOR_REST_CLIENT =
            new DvcsConnectorRestClient(ENVIRONMENT_DATA);
    private static final TimestampNameTestResource TIMESTAMP_NAME_TEST_RESOURCE = new TimestampNameTestResource();

    private BitbucketConsumer bitbucketConsumer;
    private BitbucketRemoteClient bitbucketRemoteClient;
    private BitbucketRepository testBitbucketRepo;
    private Git localGitRepo;
    private Organization testDvcsOrganization;
    private String dvcsRepoPassword;

    private String commitHash;
    private String staleCommitHash;

    @BeforeClass
    public void setup() throws IOException, GitAPIException {
        LocalDevOnlyPropertiesLoader.loadLocalDevOnlyProperties();
        dvcsRepoPassword = PasswordUtil.getPassword(DVCS_REPO_OWNER);

        removeJiraProjectsCreatedByThisTestClass();
        DVCS_CONNECTOR_REST_CLIENT.deleteAllOrganizations();

        bitbucketRemoteClient = new BitbucketRemoteClient(DVCS_REPO_OWNER, dvcsRepoPassword);

        bitbucketConsumer = bitbucketRemoteClient.getConsumerRemoteRestpoint().createConsumer(DVCS_REPO_OWNER);

        testDvcsOrganization =
                createDvcsConnectorOrganization(bitbucketConsumer.getKey(), bitbucketConsumer.getSecret());

        testBitbucketRepo = createTestBitbucketRepo(BB_REPO_NAME_PREFIX);

        localGitRepo = createLocalTempGitRepo();
        configureRemoteOriginToBitbucket(localGitRepo, DVCS_REPO_OWNER, testBitbucketRepo.getSlug());

        String[] commits = makeTwoCommitsResetLastAndReturnShas();
        commitHash = commits[0];
        staleCommitHash = commits[1];

        // staleCommitHash should not exist in the BB repo because the staleCommit is removed
        // but it seems that there is some delay in BB (GC / sync process etc)
        // that's why some noise is added to the staleCommitHash
        staleCommitHash += "noise";
    }

    @Test
    public void shouldReturnChangesetsForGivenCommitHash() {
        List<String> includes = Collections.singletonList(commitHash);
        List<String> excludes = Collections.emptyList();
        checkGettingCorrectChangesetsFromBitbucket(includes, excludes, commitHash);
    }

    @Test
    public void shouldIgnoreBadCommitsInExcludes() {
        List<String> includes = Collections.singletonList(commitHash);
        List<String> excludes = Collections.singletonList(staleCommitHash);
        checkGettingCorrectChangesetsFromBitbucket(includes, excludes, commitHash);
    }

    @Test
    public void shouldIgnoreBadCommitsInIncludesAndExcludes() {
        List<String> includes = Arrays.asList(commitHash, staleCommitHash);
        List<String> excludes = Arrays.asList("another_stale2", "another_stale3");
        checkGettingCorrectChangesetsFromBitbucket(includes, excludes, commitHash);
    }

    @AfterClass
    public void tearDown() {
        // Delete temp local GIT repo
        removeLocalGitRepo(localGitRepo);

        // Remove DVCS Organization in JIRA
        Response deleteOrganizationResponse =
                DVCS_CONNECTOR_REST_CLIENT.deleteOrganization(testDvcsOrganization.getId(), true);
        assertEquals(deleteOrganizationResponse.statusCode, 204);

        // Delete Bitbucket repo
        bitbucketRemoteClient.getRepositoriesRest().removeRepository(DVCS_REPO_OWNER, testBitbucketRepo.getName());

        // Remove OAuth consumer in BB
        bitbucketRemoteClient.getConsumerRemoteRestpoint()
                .deleteConsumer(DVCS_REPO_OWNER, bitbucketConsumer.getId().toString());

        removeJiraProjectsCreatedByThisTestClass();
    }

    private void removeJiraProjectsCreatedByThisTestClass() {
        BACKDOOR.project().getProjects()
                .stream().map(project -> project.key)
                .filter(Objects::nonNull)
                .filter(key -> key.startsWith(PROJECT_KEY))
                .forEach(key -> BACKDOOR.project().deleteProject(key));
    }

    private String[] makeTwoCommitsResetLastAndReturnShas() throws GitAPIException, IOException {
        String[] commits = new String[2];

        // create a project
        final long projectId = BACKDOOR.project().addProject("Test 404 Commits Project", PROJECT_KEY, "admin");
        assertTrue(projectId > 0);

        // create an issue
        final IssueCreateResponse issueCreateResponse = BACKDOOR.issues()
                .createIssue(PROJECT_KEY, PROJECT_KEY + " Issue summary");
        final String issueKey = issueCreateResponse.key();
        assertNotNull(issueKey);


        // create 2 commits
        commits[0] = commitAndGetHash(localGitRepo, "Message1 " + issueKey);
        commits[1] = commitAndGetHash(localGitRepo, "Message2 " + issueKey);

        // push
        localGitRepo.push().setCredentialsProvider(
                new UsernamePasswordCredentialsProvider(DVCS_REPO_OWNER, dvcsRepoPassword))
                .call();

        // git reset HEAD~1 --hard
        localGitRepo.reset().setRef("HEAD~1").setMode(ResetCommand.ResetType.HARD).call();

        // git push
        localGitRepo.push().setCredentialsProvider(
                new UsernamePasswordCredentialsProvider(DVCS_REPO_OWNER, dvcsRepoPassword))
                .setForce(true)
                .call();

        return commits;
    }

    private void checkGettingCorrectChangesetsFromBitbucket(final List<String> includes, final List<String> excludes, String commitHash) {
        BitbucketChangesetPage page = bitbucketRemoteClient.getChangesetsRest()
                .getNextChangesetsPage(testDvcsOrganization.getName(), testBitbucketRepo.getSlug(), includes, excludes, 5, null);

        BitbucketNewChangeset bitbucketNewChangeset = page.getValues().get(0);

        assertEquals(bitbucketNewChangeset.getHash(), commitHash);
    }

    private String commitAndGetHash(final Git localGitRepo, final String commitMessage)
            throws GitAPIException, IOException {
        final String fileName = "test.txt";
        final File testFile = new File(localGitRepo.getRepository().getDirectory(), fileName);
        //noinspection ResultOfMethodCallIgnored
        testFile.createNewFile();


        localGitRepo.add().addFilepattern(fileName).call();

        RevCommit commit = localGitRepo.commit().setMessage(commitMessage).call();

        return commit.getName();
    }

    private BitbucketRepository createTestBitbucketRepo(final String repoNamePrefix) {
        final String randomRepoName = RandomStringUtils.random(6, true, true);

        final String repoNamePrefixWithRandom = (repoNamePrefix + randomRepoName).toLowerCase();
        final String repoName = TIMESTAMP_NAME_TEST_RESOURCE.randomName(repoNamePrefixWithRandom, 30 * 60 * 1000);
        bitbucketRemoteClient.getRepositoriesRest().createGitRepository(repoName);
        return bitbucketRemoteClient.getRepositoriesRest().getRepository(DVCS_REPO_OWNER, repoName);
    }

    private Organization createDvcsConnectorOrganization(final String oauthKey, final String oauthSecret) {
        final Organization newOrg = new Organization();
        newOrg.setAutolinkNewRepos(false);
        newOrg.setHostUrl(BitbucketDetails.getHostUrl());
        newOrg.setName(DVCS_REPO_OWNER);
        newOrg.setDvcsType("bitbucket");
        newOrg.setSmartcommitsOnNewRepos(true);
        newOrg.setOrganizationUrl(BitbucketDetails.getHostUrl() + "/" + DVCS_REPO_OWNER);

        final Response<Organization> postResponse = DVCS_CONNECTOR_REST_CLIENT.postOrganization(newOrg);
        assertEquals(postResponse.statusCode, 200);
        final Organization savedOrg = postResponse.body;

        assertNotNull(savedOrg);
        assertNotEquals(savedOrg.getId(), 0);

        // Update OAuth details for new Org in DVCS Connector
        final CredentialJson credentialRaw = new CredentialJson().setKey(oauthKey).setSecret(oauthSecret);
        final Response updateOAuthResponse = DVCS_CONNECTOR_REST_CLIENT.updateOrganizationCredential(
                savedOrg.getId(), credentialRaw);

        assertEquals(updateOAuthResponse.statusCode, 200);

        return savedOrg;
    }
}
