package com.atlassian.jira.plugins.dvcs.event;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

/**
 * A poorly behaved event for use in testing
 */
public class BadEvent implements ContextAwareSyncEvent {
    private final int repoId;
    private final Date date;

    public BadEvent() {
        this.repoId = 0;
        this.date = new Date();
    }

    public BadEvent(int repoId, Date date) {
        this.repoId = repoId;
        this.date = date;
    }

    @Nonnull
    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public Optional<Integer> getId() {
        return Optional.empty();
    }

    @Override
    public int getRepoId() {
        return repoId;
    }

    @Override
    public String asJson() throws IOException {
        throw new RuntimeException("ya can't json me");
    }

    @Override
    public String getEventName() {
        return "some.class.that.does.not.exist";
    }

    @Override
    public boolean scheduledSync() {
        return false;
    }

    @Override
    public SyncEvent getSyncEvent() {
        return null;
    }
}
