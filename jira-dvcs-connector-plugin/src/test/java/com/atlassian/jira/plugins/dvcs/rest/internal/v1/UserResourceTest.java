package com.atlassian.jira.plugins.dvcs.rest.internal.v1;

import com.atlassian.jira.plugins.dvcs.featurediscovery.FeatureDiscoveryService;
import com.atlassian.jira.plugins.dvcs.util.TestRestResourceInstance;
import com.atlassian.jira.plugins.dvcs.util.UnitTestRestServerRule;
import com.jayway.restassured.http.ContentType;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserResourceTest {

    @Rule
    public UnitTestRestServerRule server = new UnitTestRestServerRule(this);

    @Mock
    private FeatureDiscoveryService featureDiscoveryService;

    @InjectMocks
    @TestRestResourceInstance
    private UserResource resource;

    @Test
    public void getFeatureDiscovery_returnsValueOfFlag() {
        when(featureDiscoveryService.hasUserSeenFeatureDiscovery()).thenReturn(true);

        given()
                .contentType(ContentType.JSON)
        .when()
                .get("/user/flag/featureDiscoverySeen")
        .then()
                .statusCode(200)
                .body("flag.value", is(true));
    }

    @Test
    public void upateFeatureDiscovery_setsValueOfFlag() {
        given()
                .contentType(ContentType.JSON)
                .body("{\"flag\":{\"value\":true}}")
        .when()
                .put("/user/flag/featureDiscoverySeen")
        .then()
                .statusCode(204);

        verify(featureDiscoveryService, times(1)).markUserAsHavingSeenFeatureDiscovery(true);
    }

}
