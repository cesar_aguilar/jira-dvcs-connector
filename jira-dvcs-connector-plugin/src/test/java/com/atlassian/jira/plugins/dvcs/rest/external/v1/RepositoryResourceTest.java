package com.atlassian.jira.plugins.dvcs.rest.external.v1;

import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.jira.plugins.dvcs.service.optional.aci.ACIJwtServiceAccessor;
import com.atlassian.jira.plugins.dvcs.util.TestRestResourceInstance;
import com.atlassian.jira.plugins.dvcs.util.UnitTestRestServerRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static com.jayway.restassured.RestAssured.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RepositoryResourceTest {
    @Rule
    public UnitTestRestServerRule server = new UnitTestRestServerRule(this);

    @Mock
    OrganizationService organizationService;
    @Mock
    RepositoryService repositoryService;
    @Mock
    ACIJwtServiceAccessor jwtServiceAccessor;

    @InjectMocks
    @TestRestResourceInstance
    RepositoryResource resource;

    @Test
    public void enableRepository_returnsNoContent_whenEnableSuccessful() {
        final int id = 9;
        given()
                .contentType("application/json")
                .pathParam("id", id)
                .post("/repository/{id}/enable")
                .then()
                // check
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        verify(repositoryService).enableRepository(id, true);
    }
}