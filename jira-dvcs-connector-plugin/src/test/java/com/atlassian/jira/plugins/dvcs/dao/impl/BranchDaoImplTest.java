package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.util.MockitoTestNgListener;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.Listeners;

@Listeners(MockitoTestNgListener.class)
public class BranchDaoImplTest {

    @Mock
    ActiveObjects activeObjects;

    @InjectMocks
    BranchDaoImpl branchDao;
}
