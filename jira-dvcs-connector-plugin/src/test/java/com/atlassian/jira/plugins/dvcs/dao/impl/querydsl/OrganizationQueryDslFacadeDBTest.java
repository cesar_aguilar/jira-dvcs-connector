package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;


import com.atlassian.jira.plugins.dvcs.activeobjects.v3.ChangesetMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.IssueToChangesetMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationToProjectMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryMapping;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.BitbucketCommunicator.BITBUCKET;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.nullValue;

public class OrganizationQueryDslFacadeDBTest extends QueryDSLDatabaseTest {

    @Test
    public void getAllProjectKeys_returnsDistinct_whenMultipleChangesets() {
        // prepare
        final ChangesetMapping firstChangeset = changesetAOPopulator.createCSM(changesetAOPopulator.getDefaultCSParams(),
                ISSUE_KEY + "1", enabledRepository);

        // associate manually to make sure IDs don't accidentally equal
        final String differentProjectKey = "FOO";
        changesetAOPopulator.associateToIssue(firstChangeset, differentProjectKey + "-123");

        // execute
        final Iterable<String> result
                = organizationQueryDslFacade.getAllProjectKeysFromChangesetsInOrganization(bitbucketOrganization.getID());

        // verify
        assertThat(result, containsInAnyOrder(differentProjectKey, "QDSL"));
    }

    @Test
    public void getAllProjectKeys_returnsDistinct_whenMultipleChangesetsMultipleOrg() {
        // prepare
        changesetAOPopulator.createCSM(changesetAOPopulator.getDefaultCSParams(), ISSUE_KEY + "1", enabledRepository);
        final String differentProjectKey = "FOO";
        changesetAOPopulator.createCSM(changesetAOPopulator.getDefaultCSParams(), differentProjectKey + "-123", enabledRepository);

        createSecondOrgWithIssueAndProject();

        // execute
        final Iterable<String> result
                = organizationQueryDslFacade.getAllProjectKeysFromChangesetsInOrganization(bitbucketOrganization.getID());

        // verify
        assertThat(result, containsInAnyOrder(differentProjectKey, "QDSL"));
    }

    @Test
    public void getAllProjectKeys_returnsOnlyEnabled_whenDisabledRepository() {
        // prepare
        final ChangesetMapping firstChangeset = changesetAOPopulator.createCSM(changesetAOPopulator.getDefaultCSParams(),
                ISSUE_KEY + "1", enabledRepository);

        // associate manually to make sure IDs don't accidentally equal
        final String differentProjectKey = "FOO";
        changesetAOPopulator.associateToIssue(firstChangeset, differentProjectKey + "-123");

        final String disabledProjectKey = "XYZ";
        final RepositoryMapping disabledRepository = repositoryAOPopulator.createRepository(bitbucketOrganization, false, false);
        changesetAOPopulator.createCSM(changesetAOPopulator.getDefaultCSParams(),
                disabledProjectKey + "-1", disabledRepository);

        // execute
        final Iterable<String> result
                = organizationQueryDslFacade.getAllProjectKeysFromChangesetsInOrganization(bitbucketOrganization.getID());

        // verify
        assertThat(result, containsInAnyOrder(differentProjectKey, "QDSL"));
    }

    @Test
    public void getAllProjectKeys_returnsEmpty_whenNoChangesets() throws SQLException {
        // prepare
        entityManager.deleteWithSQL(IssueToChangesetMapping.class, "1 = 1", new Object[]{});

        // execute
        final Iterable<String> result
                = organizationQueryDslFacade.getAllProjectKeysFromChangesetsInOrganization(bitbucketOrganization.getID());

        // verify
        assertThat(result.iterator().hasNext(), is(false));
    }

    @Test
    public void getCurrentlyLinkedProjects_returnsValues_whenEntriesExist() throws SQLException {
        // prepare
        final String projectKey1 = "ABC";
        createOrganizationToProjectMapping(projectKey1);

        final String projectKey2 = "XZY";
        createOrganizationToProjectMapping(projectKey2);

        // execute
        final Iterable<String> linkedProjects
                = organizationQueryDslFacade.getCurrentlyLinkedProjects(bitbucketOrganization.getID());

        // verify
        assertThat(linkedProjects, containsInAnyOrder(projectKey1, projectKey2));
    }

    @Test
    public void getCurrentlyLinkedProjects_returnsCorrectValues_whenEntriesExistAndSecondOrg() throws SQLException {
        // prepare
        final String projectKey1 = "ABC";
        createOrganizationToProjectMapping(projectKey1);

        final String projectKey2 = "XZY";
        createOrganizationToProjectMapping(projectKey2);

        createSecondOrgWithIssueAndProject();

        // execute
        final Iterable<String> linkedProjects
                = organizationQueryDslFacade.getCurrentlyLinkedProjects(bitbucketOrganization.getID());

        // verify
        assertThat(linkedProjects, containsInAnyOrder(projectKey1, projectKey2));
    }

    @Test
    public void getCurrentlyLinkedProjects_returnsEmpty_whenNothingExists() {
        // execute
        final Iterable<String> linkedProjects
                = organizationQueryDslFacade.getCurrentlyLinkedProjects(bitbucketOrganization.getID());

        // verify
        assertThat(linkedProjects.iterator().hasNext(), is(false));
    }

    @Test
    public void linkProject_addsLinkedProject_whenValid() throws SQLException {
        // prepare
        final String foo = "foo";
        entityManager.deleteWithSQL(OrganizationToProjectMapping.class, "1 = 1", new Object[]{});

        // execute
        final Long recordsCreated = organizationQueryDslFacade.linkProject(bitbucketOrganization.getID(), foo);

        // verify
        assertThat(recordsCreated, equalTo(1l));
        final Iterable<String> linkedProjectsIterable
                = organizationQueryDslFacade.getCurrentlyLinkedProjects(bitbucketOrganization.getID());
        final List<String> linkedProjects = Lists.newArrayList(linkedProjectsIterable);

        assertThat(linkedProjects.size(), equalTo(1));
        final String returnedProjectKey = linkedProjects.get(0);
        assertThat(returnedProjectKey, equalTo(foo));
    }

    @Test
    public void updateLinkedProjects_replacesAllValues_whenCalled() throws SQLException {
        createSecondOrgWithIssueAndProject();

        final String projectKey1 = "ABC";
        createOrganizationToProjectMapping(projectKey1);

        final String projectKey2 = "XZY";
        createOrganizationToProjectMapping(projectKey2);

        final String projectKey3 = "JUJ";

        organizationQueryDslFacade.updateLinkedProjects(bitbucketOrganization.getID(),
                ImmutableList.of(projectKey1, projectKey3));

        final Iterable<String> linkedProjectsIterable
                = organizationQueryDslFacade.getCurrentlyLinkedProjects(bitbucketOrganization.getID());

        assertThat(linkedProjectsIterable, containsInAnyOrder(projectKey1, projectKey3));
    }

    @Test
    public void remove_organizationOnly_deletesOrganization() throws SQLException {
        // setup
        final OrganizationMapping organizationMapping
                = organizationAOPopulator.create(BITBUCKET, "https://bogus2.bitbucket.org", "222");
        final int organizationId = organizationMapping.getID();

        // execute
        organizationQueryDslFacade.remove(organizationId);

        // verify
        assertThat(entityManager.get(OrganizationMapping.class, organizationId), nullValue());
    }

    @Test
    public void remove_organizationDoesNotExist_works() throws SQLException {
        // setup
        final int organizationId = 12312;

        // execute
        organizationQueryDslFacade.remove(organizationId);

        // verify
        assertThat(entityManager.get(OrganizationMapping.class, organizationId), nullValue());
    }

    @Test
    public void remove_organizationAndProjectKey_deletesEverything() throws SQLException {
        // setup
        final OrganizationMapping organizationMapping
                = organizationAOPopulator.create(BITBUCKET, "https://bogus3.bitbucket.org", "222");
        final int organizationId = organizationMapping.getID();

        final OrganizationToProjectMapping organizationToProjectMapping
                = createOrganizationToProjectMapping(organizationId, "OFO");
        final int organizationToProjectMappingId = organizationToProjectMapping.getID();

        // execute
        organizationQueryDslFacade.remove(organizationId);

        // verify
        assertThat(entityManager.get(OrganizationMapping.class, organizationId), nullValue());
        assertThat(entityManager.get(OrganizationToProjectMapping.class, organizationToProjectMappingId), nullValue());
    }

    private OrganizationToProjectMapping createOrganizationToProjectMapping(final String projectKey)
            throws SQLException {

        return createOrganizationToProjectMapping(bitbucketOrganization.getID(), projectKey);
    }

    private OrganizationToProjectMapping createOrganizationToProjectMapping(final int organizationId,
                                                                            final String projectKey)
            throws SQLException {

        final Map<String, Object> projectMapping = ImmutableMap.of(
                OrganizationToProjectMapping.ORGANIZATION_ID, organizationId,
                OrganizationToProjectMapping.PROJECT_KEY, projectKey);
        return entityManager.create(OrganizationToProjectMapping.class, projectMapping);
    }

    private void createSecondOrgWithIssueAndProject() {
        final OrganizationMapping org2 = organizationAOPopulator.create(BITBUCKET, "https://bogus.4.bitbucket.org", "iorg2");
        final RepositoryMapping repo2 = repositoryAOPopulator.createRepository(org2, false, true, "foo/fah");
        changesetAOPopulator.createCSM("sdf", "XXX-11", repo2);
    }
}
