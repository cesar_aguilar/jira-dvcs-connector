package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.jira.plugins.dvcs.activeobjects.DvcsConnectorTableNameConverter;
import com.atlassian.jira.plugins.dvcs.activeobjects.SchemaAOPopulator;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryToProjectMapping;
import com.atlassian.jira.plugins.dvcs.dao.RepositoryDao;
import com.atlassian.jira.plugins.dvcs.dao.impl.transform.RepositoryTransformer;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.sync.Synchronizer;
import com.google.common.collect.ImmutableMap;
import net.java.ao.Entity;
import net.java.ao.EntityManager;
import net.java.ao.Query;
import net.java.ao.test.converters.NameConverters;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.jdbc.DynamicJdbcConfiguration;
import net.java.ao.test.jdbc.Jdbc;
import net.java.ao.test.jdbc.NonTransactional;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(ActiveObjectsJUnitRunner.class)
@Jdbc(DynamicJdbcConfiguration.class)
@Data(RepositoryDaoImplTest.DatabaseSeeder.class)
@NameConverters(table = DvcsConnectorTableNameConverter.class)
public class RepositoryDaoImplTest {

    private static final String PROJECT_KEY = "ACME";
    private static final String[] NON_DELETED_REPOS = {"REPO1", "REPO2"};
    private static final String[] DELETED_REPOS = {"REPO3"};
    private static final String ORG_NAME = "MY_ORG";

    @Mock
    private Synchronizer synchronizer;

    @Inject // by test runner
    private EntityManager entityManager;

    private RepositoryDao repositoryDao;

    @Before
    public void setUpDao() throws Exception {
        initMocks(this);

        repositoryDao = new RepositoryDaoImpl(new TestActiveObjects(entityManager), new RepositoryTransformer(), synchronizer);
    }

    @Test
    @NonTransactional
    public void remove_withValidRepoName_shouldDeleteAssociatedProjectMapping() throws Exception {
        // Set up
        final int repositoryId = getRepositoryMapping(NON_DELETED_REPOS[0]).getID();

        // Invoke
        repositoryDao.remove(repositoryId);

        // Check
        assertNoRowWithId(RepositoryMapping.class, repositoryId);
    }

    @Test
    @NonTransactional
    public void getByNameForOrganization_withMatchingOrgAndName_returnsResult() throws Exception {
        final int orgId = getOrganizationMapping().getID();

        final Repository result = repositoryDao.getByNameForOrganization(orgId, NON_DELETED_REPOS[0]);

        assertThat(result, notNullValue());
        assertThat(result.getName(), is(NON_DELETED_REPOS[0]));
    }

    @Test
    @NonTransactional
    public void getByNameForOrganization_withNonMatchingOrg_returnsNull() throws Exception {
        final int orgId = getOrganizationMapping().getID();

        final Repository result = repositoryDao.getByNameForOrganization(orgId + 1, NON_DELETED_REPOS[0]);

        assertThat(result, nullValue());
    }

    @Test
    @NonTransactional
    public void getByNameForOrganization_withNonMatchingName_returnsNull() throws Exception {
        final int orgId = getOrganizationMapping().getID();

        final Repository result = repositoryDao.getByNameForOrganization(orgId, "Not a repo!");

        assertThat(result, nullValue());
    }

    @Test
    public void getAll_retrievesAllNonDeletedRepositories_whenFlagFalse() throws Exception {
        final List<Repository> repositories = repositoryDao.getAll(false);
        assertThat(repositories.size(), is(NON_DELETED_REPOS.length));
    }

    @Test
    public void getAll_retrievesAllRepositories_whenFlagTrue() throws Exception {
        final List<Repository> repositories = repositoryDao.getAll(true);
        assertThat(repositories.size(), is(NON_DELETED_REPOS.length + DELETED_REPOS.length));
    }

    @Test
    @NonTransactional
    public void get_returnsResult_whenValidRepoId() throws Exception {
        final int repoId = getRepositoryMapping(NON_DELETED_REPOS[0]).getID();

        final Repository result = repositoryDao.get(repoId);

        assertThat(result, notNullValue());
        assertThat(result.getName(), is(NON_DELETED_REPOS[0]));
    }

    @Test
    @NonTransactional
    public void get_returnsNull_whenInvalidRepoId() throws Exception {
        final int repoId = 123456;

        final Repository result = repositoryDao.get(repoId);

        assertThat(result, nullValue());
    }

    private RepositoryMapping getRepositoryMapping(final String repoName) throws Exception {
        final Query findByName = Query.select().where(RepositoryMapping.NAME + " = ?", repoName);
        final RepositoryMapping[] mappings = entityManager.find(RepositoryMapping.class, findByName);
        assertThat(mappings.length, is(1));
        return mappings[0];
    }

    private OrganizationMapping getOrganizationMapping() throws Exception {
        final Query findByName = Query.select().where(OrganizationMapping.NAME + " = ?", ORG_NAME);
        final OrganizationMapping[] mappings = entityManager.find(OrganizationMapping.class, findByName);
        assertThat(mappings.length, is(1));
        return mappings[0];
    }

    private void assertNoRowWithId(final Class<? extends Entity> table, final int id) throws Exception {
        final Entity entity = entityManager.get(table, id);
        assertThat(entity, is(nullValue(Entity.class)));
    }

    /**
     * Populates the db with the tables and rows required by this test.
     */
    public static class DatabaseSeeder implements DatabaseUpdater {

        @Override
        public void update(final EntityManager entityManager) throws Exception {
            // Create the tables
            new SchemaAOPopulator(entityManager).dropAndCreateTables(
                    RepositoryMapping.class, RepositoryToProjectMapping.class, OrganizationMapping.class);

            // Insert the test data
            final int orgId = addOrganizationMapping(entityManager, ORG_NAME);
            for (String repoName : NON_DELETED_REPOS) {
                final int repositoryId = addRepositoryMapping(entityManager, orgId, repoName, false);
                addProjectMapping(repositoryId, entityManager);
            }
            for (String repoName : DELETED_REPOS) {
                final int repositoryId = addRepositoryMapping(entityManager, orgId, repoName, true);
                addProjectMapping(repositoryId, entityManager);
            }
        }

        private int addOrganizationMapping(final EntityManager em, final String orgName) throws Exception {
            final ImmutableMap<String, Object> fields = ImmutableMap.of(
                    OrganizationMapping.NAME, orgName
            );
            return em.create(OrganizationMapping.class, fields).getID();
        }

        private int addRepositoryMapping(final EntityManager em, final int orgId,
                                         final String repoName, final boolean deleted) throws Exception {
            final Map<String, Object> fields = ImmutableMap.of(
                    RepositoryMapping.ORGANIZATION_ID, orgId,
                    RepositoryMapping.NAME, repoName,
                    RepositoryMapping.DELETED, deleted
            );
            return em.create(RepositoryMapping.class, fields).getID();
        }

        private void addProjectMapping(final int repositoryId, final EntityManager entityManager) throws Exception {
            final Map<String, Object> fields = ImmutableMap.of(
                    RepositoryToProjectMapping.PROJECT_KEY, PROJECT_KEY,
                    RepositoryToProjectMapping.REPOSITORY_ID, repositoryId
            );
            entityManager.create(RepositoryToProjectMapping.class, fields);
        }
    }
}