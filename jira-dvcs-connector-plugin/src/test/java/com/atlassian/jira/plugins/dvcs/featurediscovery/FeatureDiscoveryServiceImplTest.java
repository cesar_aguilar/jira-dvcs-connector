package com.atlassian.jira.plugins.dvcs.featurediscovery;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.usersettings.UserSettings;
import com.atlassian.sal.api.usersettings.UserSettingsBuilder;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.google.common.base.Function;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.annotation.Nullable;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.jira.plugins.dvcs.featurediscovery.FeatureDiscoveryServiceImpl.FEATURE_DISCOVERY_USER_SETTINGS_KEY;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class FeatureDiscoveryServiceImplTest {

    private static final UserKey USER = new UserKey("user");

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private UserSettingsService userSettingsService;

    @Mock
    private UserSettings userSettings;

    @Mock
    private UserManager userManager;

    @Captor
    private ArgumentCaptor<Function<UserSettingsBuilder, UserSettings>> settingsUpdateCaptor;

    @InjectMocks
    private FeatureDiscoveryServiceImpl classUnderTest;

    @Before
    public void setUp() throws Exception {
        setLoggedInUser(USER);
        when(userSettingsService.getUserSettings(any(UserKey.class))).thenReturn(userSettings);
    }

    @Test
    public void hasUserSeenFeatureDiscovery_returnsTrue_whenNoUser() {
        setNoLoggedInUser();
        assertThat(classUnderTest.hasUserSeenFeatureDiscovery(), is(true));
    }

    @Test
    public void hasUserSeenFeatureDiscovery_returnsFalse_whenNoFlagSet() {
        setFlagInUserSettings(null);
        assertThat(classUnderTest.hasUserSeenFeatureDiscovery(), is(false));
    }

    @Test
    public void hasUserSeenFeatureDiscovery_returnsFalse_whenFlagIsFalse() {
        setFlagInUserSettings(false);
        assertThat(classUnderTest.hasUserSeenFeatureDiscovery(), is(false));
    }

    @Test
    public void hasUserSeenFeatureDiscovery_returnsTrue_whenFlagIsTrue() {
        setFlagInUserSettings(true);
        assertThat(classUnderTest.hasUserSeenFeatureDiscovery(), is(true));
    }

    @Test
    public void markUserAsHavingSeenFeatureDiscovery_noop_whenNoUser() {
        setNoLoggedInUser();
        classUnderTest.markUserAsHavingSeenFeatureDiscovery(true);
        verifySettingsNotUpdated();
    }

    @Test
    public void markUserAsHavingSeenFeatureDiscovery_storesFlagAgainstUser_whenUser() {
        classUnderTest.markUserAsHavingSeenFeatureDiscovery(true);
        verifyFlagSetOnUserSettings(true);
    }

    private void setNoLoggedInUser() {
        setLoggedInUser(null);
    }

    private void setLoggedInUser(final UserKey user) {
        when(userManager.getRemoteUserKey()).thenReturn(user);
    }

    private void setFlagInUserSettings(@Nullable final Boolean flag) {
        when(userSettings.getBoolean(eq(FEATURE_DISCOVERY_USER_SETTINGS_KEY))).thenReturn(option(flag));
    }

    private void verifySettingsNotUpdated() {
        verify(userSettingsService, never()).updateUserSettings(any(UserKey.class), any());
    }

    private void verifyFlagSetOnUserSettings(boolean flagValue) {
        verify(userSettingsService, times(1)).updateUserSettings(eq(USER), settingsUpdateCaptor.capture());
        final UserSettingsBuilder builder = mock(UserSettingsBuilder.class);
        when(builder.put(any(), any(Boolean.class))).thenReturn(builder);
        settingsUpdateCaptor.getValue().apply(builder);
        verify(builder, times(1)).put(FEATURE_DISCOVERY_USER_SETTINGS_KEY, flagValue);
    }
}