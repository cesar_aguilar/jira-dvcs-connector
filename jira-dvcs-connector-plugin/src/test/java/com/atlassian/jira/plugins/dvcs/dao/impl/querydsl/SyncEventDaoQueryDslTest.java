package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.event.BadEvent;
import com.atlassian.jira.plugins.dvcs.event.ContextAwareSyncEvent;
import com.atlassian.jira.plugins.dvcs.event.DefaultContextAwareSyncEvent;
import com.atlassian.jira.plugins.dvcs.event.SyncEventMapping;
import com.atlassian.jira.plugins.dvcs.event.TestEvent;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QSyncEventMapping;
import com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessor;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.io.InputStream;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessorUtils.dropTable;
import static com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessorUtils.setUpDatabaseUsingSQLFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class SyncEventDaoQueryDslTest {

    private static final String AO_TABLE_NAME_PREFIX = "AO_E8B6CC";
    private static final int REPO_ONE = 1;
    private static final Date NOW = new Date();
    private static final String DATA_PREFIX = "This is event number:";
    private static final String SYNC_EVENT_TABLE = "AO_E8B6CC_SYNC_EVENT";
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    private SyncEventDaoQueryDsl classUnderTest;
    @Mock
    private ActiveObjects ao;
    private StandaloneDatabaseAccessor databaseAccessor;

    @Before
    public void setUp() throws Exception {
        final InputStream inputStream = MessageQueueItemDaoQueryDslTest.class.getResourceAsStream("/com.atlassian.jira.plugins.dvcs.dao.impl.querydsl/SyncEventDaoQueryDslTest.sql");
        databaseAccessor = setUpDatabaseUsingSQLFile(AO_TABLE_NAME_PREFIX, inputStream, SyncEventMapping.class);

        classUnderTest = new SyncEventDaoQueryDsl(ao, databaseAccessor);
    }

    @After
    public void finalCleanUp() throws Exception {
        dropTable(SYNC_EVENT_TABLE, databaseAccessor);
    }

    @Test
    public void testSaveCalledOnMalformedEventThrowsException() throws Exception {
        final ContextAwareSyncEvent newEvent = new BadEvent();
        try {
            classUnderTest.save(newEvent);
        } catch (RuntimeException e) {
            assertThat(e.getMessage(), is(Matchers.equalTo("ya can't json me")));
        }
    }

    @Test
    public void testDelete() throws Exception {
        QSyncEventMapping syncEventMapping = new QSyncEventMapping();

        final long before = databaseAccessor.runInTransaction(connection -> connection
                .select(syncEventMapping.all())
                .from(syncEventMapping)
                .where(syncEventMapping.ID.eq(1)).fetchCount());
        assertThat(before, is(1L));

        DefaultContextAwareSyncEvent event = new DefaultContextAwareSyncEvent(1, REPO_ONE, false, new TestEvent(NOW, DATA_PREFIX + 1));

        classUnderTest.delete(event);

        final long count = databaseAccessor.runInTransaction(connection -> connection
                .select(syncEventMapping.all())
                .from(syncEventMapping)
                .where(syncEventMapping.ID.eq(1)).fetchCount());
        assertThat(count, is(0L));
    }

    @Test
    public void testDeleteAll() throws Exception {
        QSyncEventMapping syncEventMapping = new QSyncEventMapping();

        final long beforeCount = databaseAccessor.runInTransaction(connection -> connection
                .select(syncEventMapping.all())
                .from(syncEventMapping).where(syncEventMapping.REPO_ID.eq(REPO_ONE))
                .fetchCount());
        assertThat(beforeCount, is(3L));

        classUnderTest.deleteAll(REPO_ONE);

        final long afterCount = databaseAccessor.runInTransaction(connection -> connection
                .select(syncEventMapping.all())
                .from(syncEventMapping).where(syncEventMapping.REPO_ID.eq(REPO_ONE))
                .fetchCount());
        assertThat(afterCount, is(0L));
    }

    @Test
    public void testForeachOnlyGrabsMatchingResults() throws Exception {
        classUnderTest.foreachByRepoId(REPO_ONE, contextAwareEvent ->
                assertThat(contextAwareEvent.getRepoId(), is(Matchers.equalTo(REPO_ONE))));
    }

    @Test
    public void testForeachOnlyCallConsumerCorrectNumberOfTimes() throws Exception {
        QSyncEventMapping syncEventMapping = new QSyncEventMapping();

        long beforeCount = databaseAccessor.runInTransaction(connection -> connection
                .select(syncEventMapping.all())
                .from(syncEventMapping).where(syncEventMapping.REPO_ID.eq(REPO_ONE))
                .fetchCount());

        final AtomicInteger counter = new AtomicInteger(0);

        classUnderTest.foreachByRepoId(REPO_ONE, contextAwareEvent ->
                counter.incrementAndGet());
        assertThat(counter.get(), is((int) beforeCount));
    }

    @Test
    public void testForeachCanHandleDeletingConsumer() throws Exception {
        final QSyncEventMapping syncEventMapping = new QSyncEventMapping();
        final Long before = databaseAccessor.runInTransaction(connection ->
                connection.select(syncEventMapping.all())
                        .from(syncEventMapping)
                        .where(syncEventMapping.REPO_ID.eq(REPO_ONE))
                        .fetchCount());
        assertThat(before, is(3L));

        classUnderTest.foreachByRepoId(REPO_ONE, classUnderTest::delete);

        final Long after = databaseAccessor.runInTransaction(connection ->
                connection.select(syncEventMapping.all())
                        .from(syncEventMapping)
                        .where(syncEventMapping.REPO_ID.eq(REPO_ONE))
                        .fetchCount());

        assertThat(after, is(0L));
    }

    @Test
    public void testForeachCanHandleEmptyTableWithNoExceptions() {
        final QSyncEventMapping syncEventMapping = new QSyncEventMapping();

        databaseAccessor.runInTransaction(connection ->
                connection.delete(syncEventMapping)
                        .where(syncEventMapping.REPO_ID.eq(REPO_ONE))
                        .execute());

        classUnderTest.foreachByRepoId(REPO_ONE, System.out::println);
    }
}