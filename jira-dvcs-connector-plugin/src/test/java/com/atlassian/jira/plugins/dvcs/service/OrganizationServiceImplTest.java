package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.fusion.aci.api.service.ACIInstallationService;
import com.atlassian.fusion.aci.api.service.exception.ConnectApplicationNotFoundException;
import com.atlassian.fusion.aci.api.service.exception.InstallationNotFoundException;
import com.atlassian.fusion.aci.api.service.exception.RemoteApplicationClientException;
import com.atlassian.jira.exception.NotFoundException;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType;
import com.atlassian.jira.plugins.dvcs.analytics.event.Source;
import com.atlassian.jira.plugins.dvcs.analytics.smartcommits.SmartCommitsAnalyticsService;
import com.atlassian.jira.plugins.dvcs.dao.OrganizationDao;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.credential.BasicAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.Credential;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialType;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.ThreeLeggedOAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.TwoLeggedOAuthCredential;
import com.atlassian.jira.plugins.dvcs.service.optional.aci.AciInstallationServiceAccessor;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicator;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicatorProvider;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.InOrderImpl;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static com.atlassian.jira.plugins.dvcs.model.Organization.ApprovalState.APPROVED;
import static com.atlassian.jira.plugins.dvcs.model.Organization.ApprovalState.PENDING_APPROVAL;
import static com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory.create2LOCredential;
import static com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory.create3LOCredential;
import static com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory.createPrincipalCredential;
import static com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory.getOAuthAccessToken;
import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.BitbucketCommunicator.BITBUCKET;
import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class OrganizationServiceImplTest {
    private static final int ORG_ID = 1;
    private static final String NEW_OAUTH_KEY = "key";
    private static final String NEW_OAUTH_SECRET = "secret";

    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private AnalyticsService analyticsService;

    @Mock
    private DvcsCommunicator dvcsCommunicator;

    @Mock
    private DvcsCommunicatorProvider dvcsCommunicatorProvider;

    @Mock
    private Organization organization;

    @Mock
    private OrganizationDao organizationDao;

    @Mock
    private SmartCommitsAnalyticsService smartCommitsAnalyticsService;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private RepositoryService repositoryService;

    @Captor
    private ArgumentCaptor<Credential> credentialArgumentCaptor;

    @Captor
    private ArgumentCaptor<Organization> organizationArgumentCaptor;

    @Mock
    private AciInstallationServiceAccessor aciInstallationServiceAccessor;

    @Mock
    private ACIInstallationService aciInstallationService;

    @Mock
    private Repository repository;

    @Mock
    private Installation installation;

    @Mock
    private LinkerService linkerService;

    @InjectMocks
    private OrganizationServiceImpl organizationService;

    @Before
    public void setUp() throws Exception {
        when(aciInstallationServiceAccessor.get()).thenReturn(Optional.of(aciInstallationService));

        when(i18nHelper.getText(any(String.class), any(Object.class))).then(i -> i.getArguments()[0]);

        when(organizationDao.save(any(Organization.class))).then(i -> i.getArguments()[0]);

        when(organizationDao.get(ORG_ID)).thenReturn(organization);

        when(dvcsCommunicatorProvider.getCommunicator("bitbucket")).thenReturn(dvcsCommunicator);
    }

    @Test(expected = NotFoundException.class)
    public void testApproveOrganization_throwsNotFoundException_whenOrgNotExists() {
        // PREPARE
        when(organizationDao.get(ORG_ID)).thenReturn(null);

        // TEST
        organizationService.changeOrganizationApprovalState(ORG_ID, APPROVED);
    }

    @Test
    public void testApproveOrganization_forOrgInPendingApprovalState() {
        // PREPARE
        when(organization.getApprovalState()).thenReturn(PENDING_APPROVAL);

        // TEST
        organizationService.changeOrganizationApprovalState(ORG_ID, APPROVED);

        // VERIFY
        verify(organization).setApprovalState(APPROVED);
        verify(organizationDao).save(organization);
    }

    @Test
    public void testApproveOrganization_forOrgInApprovedState() {
        // PREPARE
        when(organization.getApprovalState()).thenReturn(APPROVED);

        // TEST
        organizationService.changeOrganizationApprovalState(ORG_ID, APPROVED);

        // VERIFY
        verify(organization).setApprovalState(APPROVED);
        verify(organizationDao).save(organization);
    }

    @Test
    public void testPendOrganization_forOrgInPendingApprovalState() {
        // setup
        when(organization.getApprovalState()).thenReturn(PENDING_APPROVAL);

        // execute
        organizationService.changeOrganizationApprovalState(ORG_ID, PENDING_APPROVAL);

        // check
        verify(organization).setApprovalState(PENDING_APPROVAL);
        verify(organizationDao).save(organization);
    }

    @Test
    public void testPendOrganization_forOrgInApprovedState() {
        // setup
        when(organization.getApprovalState()).thenReturn(APPROVED);

        // execute
        organizationService.changeOrganizationApprovalState(ORG_ID, PENDING_APPROVAL);

        // check
        verify(organization).setApprovalState(PENDING_APPROVAL);
        verify(organizationDao).save(organization);
    }

    @Test
    public void testEnableSmartcommitsOnNewRepos() throws Exception {
        organizationService.enableSmartcommitsOnNewRepos(1, true);
        verify(smartCommitsAnalyticsService).fireSmartCommitAutoEnabledConfigChange(ORG_ID, true);
    }

    @Test(expected = IllegalStateException.class)
    public void testUpdateOAuthWhenNonOAuth() {
        when(organization.getCredential()).thenReturn(createPrincipalCredential("principal"));

        organizationService.updateOAuthCredentials(ORG_ID, NEW_OAUTH_KEY, NEW_OAUTH_SECRET);
    }

    @Test
    public void testUpdateOAuthWithNonexistentOrg() {
        assertThat(organizationService.updateOAuthCredentials(123, NEW_OAUTH_KEY, NEW_OAUTH_SECRET)).isNull();
    }

    @Test
    public void testUpdateOAuthWhen2LO() {
        when(organization.getCredential()).thenReturn(create2LOCredential("k", "s"));

        organizationService.updateOAuthCredentials(ORG_ID, NEW_OAUTH_KEY, NEW_OAUTH_SECRET);

        verify(organization).setCredential(credentialArgumentCaptor.capture());
        verify(organizationDao).save(organization);

        assertThat(credentialArgumentCaptor.getValue()).isInstanceOf(TwoLeggedOAuthCredential.class);

        TwoLeggedOAuthCredential updatedCredential = (TwoLeggedOAuthCredential) credentialArgumentCaptor.getValue();
        assertThat(updatedCredential.getKey()).isEqualTo(NEW_OAUTH_KEY);
        assertThat(updatedCredential.getSecret()).isEqualTo(NEW_OAUTH_SECRET);
    }

    @Test
    public void testUpdateOAuthWhen3LO() {
        when(organization.getCredential()).thenReturn(create3LOCredential("k", "s", "t"));

        organizationService.updateOAuthCredentials(ORG_ID, NEW_OAUTH_KEY, NEW_OAUTH_SECRET);

        verify(organization).setCredential(credentialArgumentCaptor.capture());
        verify(organizationDao).save(organization);

        assertThat(credentialArgumentCaptor.getValue()).isInstanceOf(ThreeLeggedOAuthCredential.class);

        ThreeLeggedOAuthCredential updatedCredential = (ThreeLeggedOAuthCredential) credentialArgumentCaptor.getValue();
        assertThat(updatedCredential.getKey()).isEqualTo(NEW_OAUTH_KEY);
        assertThat(updatedCredential.getSecret()).isEqualTo(NEW_OAUTH_SECRET);
        assertThat(updatedCredential.getAccessToken()).isEqualTo("t");
    }

    @Test(expected = NotFoundException.class)
    public void approveOrganization_throwsNotFoundException_ifOrganiozationNotExists() {
        when(organizationDao.get(anyInt())).thenReturn(null);
        organizationService.changeOrganizationApprovalState(1, APPROVED);
    }

    @Test
    public void approveOrganization_setApprovedFlag_forValidOrganization() {
        // setup
        final Organization org = createSampleOrganization();
        org.setId(5);
        org.setApprovalState(PENDING_APPROVAL);
        when(organizationDao.get(org.getId())).thenReturn(org);

        // execute
        final Organization result = organizationService.changeOrganizationApprovalState(org.getId(), APPROVED);

        // check
        MatcherAssert.assertThat(result, notNullValue());
        MatcherAssert.assertThat(result.getApprovalState(), equalTo(APPROVED));
        verify(organizationDao).save(org);
    }

    @Test
    public void saveOrganization_usesProvidedCredentials_whenCredentialsProvided() {
        final Organization sampleOrganization = createSampleOrganization();
        setNewOrganization(sampleOrganization);

        final Organization saved = organizationService.save(sampleOrganization);

        assertThat(saved).isSameAs(sampleOrganization);
        verify(repositoryService, times(1)).syncRepositoryList(sampleOrganization, false);
    }

    @Test
    public void saveOrganization_usesAnonymousCredentials_whenCredentialsNotProvided() {
        final Organization sampleOrganization = createSampleOrganization();
        sampleOrganization.setCredential(null);
        setNewOrganization(sampleOrganization);

        final Organization saved = organizationService.save(sampleOrganization);

        assertThat(saved.getCredential().getType() == CredentialType.UNAUTHENTICATED);
        verify(repositoryService, times(1)).syncRepositoryList(sampleOrganization, false);
    }

    @Test
    public void saveOrganization_doesNotCreateNewOrg_whenExistingOrg() {
        final Organization sampleOrganization = createSampleOrganization();
        setExistingOrganization(sampleOrganization);

        final Organization saved = organizationService.save(sampleOrganization);

        assertThat(saved).isSameAs(sampleOrganization);
        verify(organizationDao, never()).save(any(Organization.class));
        verify(repositoryService, never()).syncRepositoryList(any(Organization.class), any(Boolean.class));
    }

    @Test(expected = NullPointerException.class)
    public void saveOrganization_fails_whenNullOrg() {
        organizationService.save(null);

        verify(organizationDao, never()).getByHostAndName(any(String.class), any(String.class));
        verify(organizationDao, never()).save(any(Organization.class));
        verify(repositoryService, never()).syncRepositoryList(any(Organization.class), any(Boolean.class));
    }

    @Test
    public void testGetAllByType() {
        Organization sampleOrganization = createSampleOrganization();
        List<Organization> list = new ArrayList<>();
        list.add(sampleOrganization);

        when(organizationDao.getAllByType("bitbucket")).thenReturn(list);

        List<Organization> all = organizationService.getAll(false, "bitbucket");

        assertThat(all.get(0)).isSameAs(sampleOrganization);

        verify(organizationDao).getAllByType("bitbucket");
    }

    @Test
    public void testGetAllByTypeLoadRepositories() {
        Organization sampleOrganization = createSampleOrganization();
        List<Organization> list = new ArrayList<>();
        list.add(sampleOrganization);

        when(organizationDao.getAllByType("bitbucket")).thenReturn(list);
        when(repositoryService.getAllByOrganization(0)).thenReturn(new ArrayList<>());

        organizationService.getAll(true, "bitbucket");

        verify(organizationDao).getAllByType("bitbucket");
        verify(repositoryService).getAllByOrganization(0);
    }

    @Test
    public void testEnableAutolinkNewRepos() {
        Organization sampleOrganization = createSampleOrganization();
        when(organizationDao.get(0)).thenReturn(sampleOrganization);

        organizationService.enableAutolinkNewRepos(0, true);

        verify(organizationDao).save(argThat(new ArgumentMatcher<Organization>() {
            @Override
            public boolean matches(Object argument) {
                Organization savingOrganization = (Organization) argument;
                return savingOrganization.isAutolinkNewRepos();
            }

        }));
    }

    @Test
    public void testDisableAutolinkNewRepos() {
        Organization sampleOrganization = createSampleOrganization();
        when(organizationDao.get(0)).thenReturn(sampleOrganization);

        organizationService.enableAutolinkNewRepos(0, false);

        verify(organizationDao).save(argThat(new ArgumentMatcher<Organization>() {
            @Override
            public boolean matches(Object argument) {
                Organization savingOrganization = (Organization) argument;
                return !savingOrganization.isAutolinkNewRepos();
            }
        }));
    }

    @Test
    public void testUpdateCredentialsAccessToken() {
        Organization sampleOrganization = createSampleOrganization();
        when(organizationDao.get(0)).thenReturn(sampleOrganization);
        when(dvcsCommunicatorProvider.getCommunicator("bitbucket")).thenReturn(dvcsCommunicator);

        organizationService.updateCredentialsAccessToken(0, "doesnotmatter_AT");

        verify(organizationDao).save(sampleOrganization);
        assertThat(getOAuthAccessToken(sampleOrganization.getCredential())).isEqualTo("doesnotmatter_AT");
    }

    @Test
    public void removeShouldNoopWhenOrgIdDoesntMatchAnOrg() {
        // setup
        final int organisationId = 1234;

        // execute
        organizationService.remove(organisationId);

        // check
        verify(repositoryService, never()).removeRepositories(any());
        verify(organizationDao, never()).remove(organisationId);
        verify(repositoryService, never()).removeOrphanRepositories(any());
    }

    @Test
    public void removeOrgShouldRemoveAssociatedRepositories() {
        // setup
        final int organisationId = 1234;
        final List<Repository> repositories = ImmutableList.of(repository);
        when(repositoryService.getAllByOrganization(organisationId, true)).thenReturn(repositories);

        final Organization organisation = mock(Organization.class);
        when(organizationDao.get(organisationId)).thenReturn(organisation);
        when(organisation.getCredential()).thenReturn(new BasicAuthCredential("user", "password"));

        InOrder inOrder = new InOrderImpl(ImmutableList.of(repositoryService, organizationDao));

        // execute
        organizationService.remove(organisationId);

        // check
        inOrder.verify(repositoryService).removeRepositories(repositories);
        inOrder.verify(organizationDao).remove(organisationId);
        inOrder.verify(repositoryService).removeOrphanRepositories(repositories);
    }

    @Test
    public void removalOfAciManagedOrgShouldRemoveOrgAndThenInformAci() {
        testOrgRemovalAndInformingOfAci(Optional.empty());
    }

    @Test
    public void removalOfAciManagedOrgShouldSwallowExceptionsRelatedToInstallationDisappearing() {
        testOrgRemovalAndInformingOfAci(Optional.of(new InstallationNotFoundException("installation not found")));
    }

    @Test
    public void removalOfAciManagedOrgShouldSwallowExceptionsRelatedToApplicationDisappearing() {
        testOrgRemovalAndInformingOfAci(Optional.of(new ConnectApplicationNotFoundException("application not found")));
    }

    @Test
    public void removeOrg_swallowsException_whenRemoteApplicationClientExceptionThrown() {
        testOrgRemovalAndInformingOfAci(Optional.of(
                new RemoteApplicationClientException("could not create connection to remote",
                        new SocketTimeoutException())));
    }

    private void testOrgRemovalAndInformingOfAci(final Optional<Exception> exceptionThrownByAciUninstallation) {
        // setup
        final int organisationId = 1234;
        final List<Repository> repositories = ImmutableList.of(repository);
        when(repositoryService.getAllByOrganization(organisationId, true)).thenReturn(repositories);

        final String principalId = "principal id";
        final Organization organisation = mock(Organization.class);
        when(organizationDao.get(organisationId)).thenReturn(organisation);
        when(organisation.getCredential()).thenReturn(new PrincipalIDCredential(principalId));

        ACIInstallationService aciInstallationService = mock(ACIInstallationService.class);

        if (exceptionThrownByAciUninstallation.isPresent()) {
            when(aciInstallationService.uninstall(BITBUCKET_CONNECTOR_APPLICATION_ID, principalId, Optional.empty()))
                    .thenThrow(exceptionThrownByAciUninstallation.get());
        }

        when(aciInstallationServiceAccessor.get()).thenReturn(Optional.of(aciInstallationService));

        InOrder inOrder = new InOrderImpl(ImmutableList.of(repositoryService, organizationDao, aciInstallationService));

        // execute
        organizationService.remove(organisationId);

        // check
        verify(analyticsService).publishConnectOrganisationRemoved(organisation);

        inOrder.verify(repositoryService).removeRepositories(repositories);
        inOrder.verify(organizationDao).remove(organisationId);
        inOrder.verify(repositoryService).removeOrphanRepositories(repositories);

        // we want to make sure we're removing the linkers and commit hooks before we tell ACI to complete the
        // rest of the uninstall
        inOrder.verify(aciInstallationService).uninstall(BITBUCKET_CONNECTOR_APPLICATION_ID, principalId, Optional.empty());
    }

    @Test
    public void createNewOrgBasedOnAciInstallation() {
        // setup
        final String principalId = "{1234}";
        final String baseUrl = "baseUrlValue";
        final String principalUserName = "principalUserNameValue";
        final String dvcsType = BITBUCKET;
        final boolean isAutolinkNewRepos = false;
        final boolean isSmartCommitsOnNewRepos = false;

        when(installation.getPrincipalUuid()).thenReturn(principalId);
        when(installation.getBaseUrl()).thenReturn(baseUrl);
        when(installation.getPrincipalUsername()).thenReturn(principalUserName);

        // execute
        organizationService.createNewOrgBasedOnAciInstallation(installation);

        // check
        verify(analyticsService).publishOrganisationAddStarted(Source.DEVTOOLS, DvcsType.BITBUCKET);
        verify(organizationDao).save(organizationArgumentCaptor.capture());
        final Organization org = organizationArgumentCaptor.getValue();
        assertThat(org).isNotNull();
        assertThat(org.getHostUrl()).isEqualTo(baseUrl);
        assertThat(org.getName()).isEqualTo(principalUserName);
        assertThat(org.getDvcsType()).isEqualTo(dvcsType);
        assertThat(org.isAutolinkNewRepos()).isEqualTo(isAutolinkNewRepos);
        assertThat(org.isSmartcommitsOnNewRepos()).isEqualTo(isSmartCommitsOnNewRepos);
        assertThat(org.getApprovalState()).isEqualTo(PENDING_APPROVAL);

        final Optional<PrincipalIDCredential> maybeCredential = org.getCredential().accept(PrincipalIDCredential.visitor());
        assertThat(maybeCredential.isPresent()).isTrue();
        assertThat(maybeCredential.get().getPrincipalId()).isEqualTo(principalId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNewOrgBasedOnAciInstallation_fails_whenPrincipalIdIsNull() {
        // setup
        when(installation.getPrincipalUuid()).thenReturn(null);

        // execute
        organizationService.createNewOrgBasedOnAciInstallation(installation);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNewOrgBasedOnAciInstallation_fails_whenPrincipalIdIsEmpty() {
        // setup
        when(installation.getPrincipalUuid()).thenReturn("");

        // execute
        organizationService.createNewOrgBasedOnAciInstallation(installation);
    }


    @Test
    public void migrateExistingOrgToPrincipalCredentialOrg_updatesCredentialAndFiresOrgUpdatedEventAndNoLinkerCleanup_whenOldCredentialIsPrincipalBasedCredential() {
        // setup
        final int orgId = 1;
        final Organization org = createOrgWithTwoReposAndRegisterWithMocks(orgId);
        final String oldPrincipalId = "oldPrincipalId";
        final String principalId = "{1234}";
        final Credential oldCredential = CredentialFactory.createPrincipalCredential(oldPrincipalId);
        org.setCredential(oldCredential);

        // execute
        organizationService.migrateExistingOrgToPrincipalCredentialOrg(org, principalId);

        // check
        verify(analyticsService).publishConnectOrganisationUpdated(org);
        verify(organizationDao).save(organizationArgumentCaptor.capture());
        organizationArgumentCaptor.getValue();
        assertThat(org).isNotNull();
        final Optional<PrincipalIDCredential> maybeCredential = org.getCredential()
                .accept(PrincipalIDCredential.visitor());
        assertThat(maybeCredential.isPresent()).isTrue();
        assertThat(maybeCredential.get().getPrincipalId()).isEqualTo(principalId);
        verify(dvcsCommunicator, never()).removeLinkersForRepo(any());
    }

    @Test
    public void migrateExistingOrgToPrincipalCredentialOrg_updatesCredentialAndFiresOrgMigratedEventAndRemovesOldLinkers_whenOldCredentialIsNotPrincipalBasedCredential() {
        // setup
        final String principalId = "{1234}";
        final Credential oldCredential = CredentialFactory.createBasicAuthCredential("a", "b");
        final int orgId = 1;
        final Organization org = createOrgWithTwoReposAndRegisterWithMocks(orgId);
        org.setCredential(oldCredential);
        when(dvcsCommunicatorProvider.getCommunicator(anyString())).thenReturn(dvcsCommunicator);
        when(linkerService.removeLinkersAsync(org)).thenReturn(new CompletableFuture<>());

        // execute
        organizationService.migrateExistingOrgToPrincipalCredentialOrg(org, principalId);

        // check
        verify(analyticsService).publishConnectOrganisationMigrated(org);
        verify(organizationDao).save(organizationArgumentCaptor.capture());
        organizationArgumentCaptor.getValue();
        assertThat(org).isNotNull();
        verify(linkerService).removeLinkersAsync(org);
        verify(repositoryService).removePostcommitHooksAsync(org, true);
    }

    private Organization createOrgWithTwoReposAndRegisterWithMocks(final int orgId) {
        final Organization org = new Organization();
        org.setId(orgId);
        final Repository repo1 = new Repository();
        repo1.setName("repo1");
        final Repository repo2 = new Repository();
        repo2.setName("repo2");
        List<Repository> repos = Lists.newArrayList();
        repos.add(repo1);
        repos.add(repo2);
        org.setRepositories(repos);
        when(organizationDao.get(orgId)).thenReturn(org);
        when(repositoryService.getAllByOrganization(orgId)).thenReturn(repos);
        return org;
    }

    @Test(expected = IllegalArgumentException.class)
    public void migrateExistingOrgToPrincipalCredentialOrg_fails_whenPrincipalIdIsNull() {
        organizationService.migrateExistingOrgToPrincipalCredentialOrg(organization, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void migrateExistingOrgToPrincipalCredentialOrg_fails_whenPrincipalIdIsBlank() {
        organizationService.migrateExistingOrgToPrincipalCredentialOrg(organization, " ");
    }

    @Test
    public void setAutoLinkAndSmartCommitsSetsSmartCommitStateOnNonLinkedRepos() {
        final int orgId = 1;
        final Organization org = createOrgWithTwoReposAndRegisterWithMocks(orgId);
        final boolean autoLink = false;
        final boolean smartCommits = true;

        organizationService.setAutolinkAndSmartcommits(orgId, autoLink, smartCommits);

        assertThat(org.isAutolinkNewRepos()).isEqualTo(autoLink);
        assertThat(org.isSmartcommitsOnNewRepos()).isEqualTo(smartCommits);
        repositoryService.getAllByOrganization(orgId).stream()
                .forEach(repo -> assertThat(repo.isSmartcommitsEnabled()).isEqualTo(smartCommits));
    }

    private void setExistingOrganization(final Organization org) {
        when(organizationDao.getByHostAndName(org.getHostUrl(), org.getName())).thenReturn(org);
    }

    private void setNewOrganization(final Organization org) {
        when(organizationDao.getByHostAndName(org.getHostUrl(), org.getName())).thenReturn(null);
    }

    private Organization createSampleOrganization() {
        final Organization organization = new Organization();
        organization.setDvcsType("bitbucket");
        organization.setHostUrl("https://bitbucket.org");
        organization.setName("doesnotmatter");
        organization.setCredential(create2LOCredential("doesnotmatter_u", "doesnotmatter_p"));
        return organization;
    }
}