package com.atlassian.jira.plugins.dvcs.activeobjects;

import net.java.ao.EntityManager;
import net.java.ao.RawEntity;

import javax.annotation.Nonnull;
import java.sql.SQLException;

/**
 * Populates the database schema with database objects.
 */
public class SchemaAOPopulator extends AOPopulator {

    public SchemaAOPopulator(@Nonnull final EntityManager entityManager) {
        super(entityManager);
    }

    /**
     * Creates the database tables for the given entities, first dropping them if they exist.
     * <p>
     * Equivalent to {@link EntityManager#migrateDestructively(Class[])}, but does not throw
     * checked exceptions or cause IDEA to complain about unchecked generic array creation.
     *
     * @param entityClasses the entity classes for which to drop and create the tables
     * @throws RuntimeException if the underlying method throws an SQLException
     */
    @SafeVarargs
    public final void dropAndCreateTables(final Class<? extends RawEntity<?>>... entityClasses) {
        try {
            entityManager.migrateDestructively(entityClasses);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
