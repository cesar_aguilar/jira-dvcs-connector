package com.atlassian.jira.plugins.dvcs.webwork;

import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.fusion.aci.api.service.ACIInstallationService;
import com.atlassian.fusion.aci.api.service.exception.ConnectApplicationNotFoundException;
import com.atlassian.fusion.aci.api.service.exception.InstallationNotFoundException;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.service.optional.aci.AciInstallationServiceAccessor;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants;
import com.atlassian.jira.plugins.dvcs.util.DvcsConstants;
import com.atlassian.jira.plugins.dvcs.util.TestNGMockComponentContainer;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Optional;

import static com.atlassian.jira.config.properties.APKeys.JIRA_TITLE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class BitbucketPostInstallApprovalActionTest {

    private static final int ORG_ID = 101;

    // Action view name constants. These should match view names in atlassian-plugin.xml. The constants
    // are duplicated here to provide safeguard in case an arbitrary change is made to corresponding
    // constants in the action class.
    private static final String APPROVAL_VIEW = "approval";
    private static final String APPROVAL_ERROR_VIEW = "approvalError";
    private static final String UNKNOWN_ERROR_VIEW = "unknownError";
    private static final String NON_ADMIN_APPROVAL_VIEW = "nonAdmin";
    private static final String REDIRECT_TO_ADMIN_VIEW = "redirectToAdminView";
    private static final String BITBUCKET_ERROR_ACCESS_DENIED = "access_denied";

    private static final String REQUIRE_RESOURCE_KEY =
            DvcsConstants.PLUGIN_KEY + ":bitbucket-post-install-approval-resources";

    private static final String JIRA_TEST_BASEURL = "http://someurl";
    private static final String JIRA_TEST_TITLE = "My JIRA";
    private static final String HOST_URL = "http://baseUrl";
    private static final String PRINCIPAL_USER_NAME = "principalUserName";
    private static final String CANCEL_BUTTON_URL_FOR_APPROVAL_VIEW = "https://cancelButtonUrlForAdminViewUrl";
    private static final String CANCEL_BUTTON_URL_FOR_ERROR_VIEW = "https://cancelButtonUrlForErrorViewUrl";
    private static final String GRANT_ACCESS_BUTTON_URL = "https://grantAccessButtonRedirectUrl";
    private static final String ORG_ALREADY_APPROVED_REDIRECT_URL = "https://alreadyApprovedUrl";
    private static final String START_AGAIN_BUTTON_URL_FOR_ERROR_VIEW = "https://startAgainButtonUrlForErrorView";
    private static final String XSRF_TOKEN = "xsrfToken";
    private static final String LOGOUT_BUTTON_URL = "https://jira/logout";
    private static final String OK_BUTTON_NON_ADMIN_APPROVAL_URL = "https://jira/dashboard";

    private final TestNGMockComponentContainer mockComponentContainer = new TestNGMockComponentContainer(this);

    @Mock
    private AciInstallationServiceAccessor aciInstallationServiceAccessor;
    @Mock
    private ACIInstallationService aciInstallationService;
    @Mock
    private PageBuilderService pageBuilderService;
    @Mock
    private WebResourceAssembler webResourceAssembler;
    @Mock
    private RequiredResources requiredResources;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private ApplicationUser currentUser;
    @Mock
    private OrganizationService organizationService;
    @Mock
    private BitbucketPostInstallApprovalActionUrlProvider urlProvider;
    @Mock
    private Installation installation;
    @Mock
    private Organization organization;
    @InjectMocks
    private BitbucketPostInstallApprovalAction action;
    @Mock
    @AvailableInContainer
    private ApplicationProperties salApplicationProperties;
    @Mock
    @AvailableInContainer
    private com.atlassian.jira.config.properties.ApplicationProperties jiraApplicationProperties;
    @Mock
    @AvailableInContainer
    private XsrfTokenGenerator xsrfTokenGenerator;

    @BeforeMethod
    public void setUp() throws Exception {
        action = null; // To force mockito to re-instantiate the action object
        organization = null;
        urlProvider = null;
        initMocks(this);
        mockComponentContainer.beforeMethod();

        when(pageBuilderService.assembler()).thenReturn(webResourceAssembler);
        when(webResourceAssembler.resources()).thenReturn(requiredResources);

        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(currentUser);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, currentUser)).thenReturn(true);

        when(installation.getBaseUrl()).thenReturn(HOST_URL);
        when(installation.getPrincipalUsername()).thenReturn(PRINCIPAL_USER_NAME);

        when(organization.getId()).thenReturn(ORG_ID);

        when(aciInstallationServiceAccessor.get()).thenReturn(Optional.of(aciInstallationService));

        when(organizationService.getByHostAndName(HOST_URL, PRINCIPAL_USER_NAME))
                .thenReturn(organization);

        when(salApplicationProperties.getBaseUrl(UrlMode.AUTO)).thenReturn(JIRA_TEST_BASEURL);
        when(jiraApplicationProperties.getString(JIRA_TITLE)).thenReturn(JIRA_TEST_TITLE);
        when(jiraApplicationProperties.getEncoding()).thenReturn("UTF-8");

        when(xsrfTokenGenerator.generateToken(any(HttpServletRequest.class))).thenReturn(XSRF_TOKEN);

        when(urlProvider.grantAccessButtonRedirectUrl(any(Organization.class))).thenReturn(GRANT_ACCESS_BUTTON_URL);

        when(urlProvider.cancelButtonUrlForApprovalView(anyBoolean(), any(), any()))
                .thenReturn(CANCEL_BUTTON_URL_FOR_APPROVAL_VIEW);
        when(urlProvider.cancelButtonUrlForErrorView(anyBoolean(), any(), any()))
                .thenReturn(CANCEL_BUTTON_URL_FOR_ERROR_VIEW);
        when(urlProvider.startAgainButtonUrlForErrorView(anyBoolean(), any(), any()))
                .thenReturn(START_AGAIN_BUTTON_URL_FOR_ERROR_VIEW);
        when(urlProvider.logoutButtonUrl()).thenReturn(LOGOUT_BUTTON_URL);
        when(urlProvider.okButtonForNonAdminView(anyBoolean(), anyString(), any()))
                .thenReturn(OK_BUTTON_NON_ADMIN_APPROVAL_URL);
    }

    @Test
    public void doExecute_returnsApprovalViewAndCorrectData_whenAciInstallationAndDvcsOrganizationExist() {
        // PREPARE
        final String principalUuid = "{1234}";
        when(aciInstallationService.get(BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID, principalUuid))
                .thenReturn(installation);

        // TEST
        action.setPrincipalUuid(principalUuid);
        final String result = action.doExecute();
        final Map<String, Object> viewData = action.getApprovalViewData();

        // VERIFY
        assertThat(result, equalTo(APPROVAL_VIEW));
        assertThat(viewData.size(), equalTo(7));
        assertThat(viewData.get("baseUrl"), equalTo(JIRA_TEST_BASEURL));
        assertThat(viewData.get("orgId"), equalTo(ORG_ID));
        assertThat(viewData.get("accountName"), equalTo(PRINCIPAL_USER_NAME));
        assertThat(viewData.get("grantAccessButtonRedirectUrl"), equalTo(GRANT_ACCESS_BUTTON_URL));
        assertThat(viewData.get("cancelButtonUrl"), equalTo(CANCEL_BUTTON_URL_FOR_APPROVAL_VIEW));
        assertThat(viewData.get("jiraInstanceTitle"), equalTo(JIRA_TEST_TITLE));
        assertThat(viewData.get("atlToken"), equalTo(XSRF_TOKEN));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
    }

    @Test
    public void doExecute_returnsApprovalViewAndCorrectData_whenAciInstallationAndDvcsOrganizationExist_and_isJiraInitiatedIsTrue_usingClientKey() {
        // PREPARE
        final String clientKey = "key1";
        when(aciInstallationService.getByClientKey(clientKey)).thenReturn(installation);

        // TEST
        action.setClientKey(clientKey);
        action.setJiraInitiated("true");
        final String result = action.doExecute();
        final Map<String, Object> viewData = action.getApprovalViewData();

        // VERIFY
        assertThat(result, equalTo(APPROVAL_VIEW));
        assertThat(viewData.size(), equalTo(7));
        assertThat(viewData.get("baseUrl"), equalTo(JIRA_TEST_BASEURL));
        assertThat(viewData.get("orgId"), equalTo(ORG_ID));
        assertThat(viewData.get("accountName"), equalTo(PRINCIPAL_USER_NAME));
        assertThat(viewData.get("grantAccessButtonRedirectUrl"), equalTo(GRANT_ACCESS_BUTTON_URL));
        assertThat(viewData.get("cancelButtonUrl"), equalTo(CANCEL_BUTTON_URL_FOR_APPROVAL_VIEW));
        assertThat(viewData.get("jiraInstanceTitle"), equalTo(JIRA_TEST_TITLE));
        assertThat(viewData.get("atlToken"), equalTo(XSRF_TOKEN));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
    }

    @Test
    public void doExecute_returnsUnknownErrorView_whenPrincipalUuidAndClientKeyMissing() {
        // TEST
        action.setAccountName(PRINCIPAL_USER_NAME);
        action.setPrincipalUuid("");
        action.setClientKey("");
        final String result = action.doExecute();

        // VERIFY
        final Map<String, Object> viewData = action.getUnknownErrorViewData();
        assertThat(result, equalTo(UNKNOWN_ERROR_VIEW));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
        assertViewDataForErrorViews(viewData);
    }

    @Test
    public void doExecute_returnsApprovalErrorView_whenApprovalErrorQueryParamSet() {
        // TEST
        action.setAccountName(PRINCIPAL_USER_NAME);
        action.setApprovalError("true");
        final String result = action.doExecute();

        // VERIFY
        final Map<String, Object> viewData = action.getApprovalErrorViewData();
        assertThat(result, equalTo(APPROVAL_ERROR_VIEW));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
        assertViewDataForErrorViews(viewData);
    }

    @Test
    public void doExecute_returnsApprovalErrorView_whenOrganizationNotExists() {
        // PREPARE
        final String principalUuid = "{1234}";
        when(aciInstallationService.get(BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID, principalUuid))
                .thenReturn(installation);
        when(organizationService.getByHostAndName(any(), any())).thenReturn(null);

        // TEST
        action.setPrincipalUuid(principalUuid);
        action.setAccountName(PRINCIPAL_USER_NAME);
        final String result = action.doExecute();

        // VERIFY
        final Map<String, Object> viewData = action.getApprovalErrorViewData();
        assertThat(result, equalTo(APPROVAL_ERROR_VIEW));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
        assertViewDataForErrorViews(viewData);
    }

    @Test
    public void doExecute_returnsApprovalViewAndTriggersOrgCreation_whenInstallationExistsButOrganizationDoesNotExist() {
        // PREPARE
        final String principalUuid = "{1234}";
        when(aciInstallationService.get(BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID, principalUuid))
                .thenReturn(installation);
        when(organizationService.getByHostAndName(any(), any())).thenReturn(null);

        doAnswer(invocation -> {
            when(organizationService.getByHostAndName(installation.getBaseUrl(), installation.getPrincipalUsername()))
                    .thenReturn(organization);
            return null;
        }).when(organizationService).createNewOrgBasedOnAciInstallation(installation);

        // TEST
        action.setPrincipalUuid(principalUuid);
        action.setAccountName(PRINCIPAL_USER_NAME);
        final String result = action.doExecute();
        final Map<String, Object> viewData = action.getApprovalViewData();

        // VERIFY
        verify(organizationService).createNewOrgBasedOnAciInstallation(installation);
        assertThat(result, equalTo(APPROVAL_VIEW));
        assertThat(viewData.size(), equalTo(7));
        assertThat(viewData.get("baseUrl"), equalTo(JIRA_TEST_BASEURL));
        assertThat(viewData.get("orgId"), equalTo(ORG_ID));
        assertThat(viewData.get("accountName"), equalTo(PRINCIPAL_USER_NAME));
        assertThat(viewData.get("grantAccessButtonRedirectUrl"), equalTo(GRANT_ACCESS_BUTTON_URL));
        assertThat(viewData.get("cancelButtonUrl"), equalTo(CANCEL_BUTTON_URL_FOR_APPROVAL_VIEW));
        assertThat(viewData.get("jiraInstanceTitle"), equalTo(JIRA_TEST_TITLE));
        assertThat(viewData.get("atlToken"), equalTo(XSRF_TOKEN));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
    }

    @Test
    public void doExecute_returnsNonAdminApprovalView_whenInstallationNotExistsByPrincipalUuid() {
        // PREPARE
        final String principalUuid = "{1234}";
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, currentUser)).thenReturn(false);

        // TEST
        action.setPrincipalUuid(principalUuid);
        action.setAccountName(PRINCIPAL_USER_NAME);
        action.setJiraInitiated("true");
        final String result = action.doExecute();

        // VERIFY
        final Map<String, Object> viewData = action.getNonAdminViewData();
        assertThat(result, equalTo(NON_ADMIN_APPROVAL_VIEW));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
        assertThat(viewData.size(), equalTo(6));
        assertThat(viewData.get("baseUrl"), equalTo(JIRA_TEST_BASEURL));
        assertThat(viewData.get("okButtonUrl"), equalTo(OK_BUTTON_NON_ADMIN_APPROVAL_URL));
        assertThat(viewData.get("logoutButtonUrl"), equalTo(LOGOUT_BUTTON_URL));
        assertThat(viewData.get("accountName"), equalTo(PRINCIPAL_USER_NAME));
        assertThat(viewData.get("jiraInstanceTitle"), equalTo(JIRA_TEST_TITLE));
        assertThat(viewData.get("atlToken"), equalTo(XSRF_TOKEN));
    }

    @Test
    public void doExecute_returnsApprovalErrorView_whenInstallationNotExistsByPrincipalUuid() {
        // PREPARE
        final String principalUuid = "{1234}";
        when(aciInstallationService.get(BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID, principalUuid))
                .thenThrow(InstallationNotFoundException.forPrincipalId(
                        BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID, principalUuid));

        // TEST
        action.setPrincipalUuid(principalUuid);
        action.setAccountName(PRINCIPAL_USER_NAME);
        final String result = action.doExecute();

        // VERIFY
        final Map<String, Object> viewData = action.getApprovalErrorViewData();
        assertThat(result, equalTo(APPROVAL_ERROR_VIEW));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
        assertViewDataForErrorViews(viewData);
    }

    @Test
    public void doExecute_returnsApprovalErrorView_whenInstallationNotExistsByClientKey() {
        // PREPARE
        final String clientKey = "{1234}";
        when(aciInstallationService.getByClientKey(clientKey))
                .thenThrow(InstallationNotFoundException.forClientKey(clientKey));

        // TEST
        action.setClientKey(clientKey);
        action.setAccountName(PRINCIPAL_USER_NAME);
        action.setJiraInitiated("true");
        final String result = action.doExecute();

        // VERIFY
        final Map<String, Object> viewData = action.getApprovalErrorViewData();
        assertThat(result, equalTo(APPROVAL_ERROR_VIEW));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
        assertViewDataForErrorViews(viewData);
    }

    @Test
    public void doExecute_returnsApprovalErrorView_whenConnectApplicationNotExistInAci() {
        // PREPARE
        final String principalUuid = "{1234}";
        when(aciInstallationService.get(BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID, principalUuid))
                .thenThrow(
                        new ConnectApplicationNotFoundException(BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID));

        // TEST
        action.setPrincipalUuid(principalUuid);
        action.setAccountName(PRINCIPAL_USER_NAME);
        final String result = action.doExecute();

        // VERIFY
        final Map<String, Object> viewData = action.getApprovalErrorViewData();
        assertThat(result, equalTo(APPROVAL_ERROR_VIEW));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
        assertViewDataForErrorViews(viewData);
    }

    @Test
    public void doExecute_returnsUnknownErrorView_whenUnknownErrorQueryParamSet() {
        // TEST
        action.setAccountName(PRINCIPAL_USER_NAME);
        action.setPrincipalUuid("someid");
        action.setUnknownError("");
        final String result = action.doExecute();

        // VERIFY
        final Map<String, Object> viewData = action.getUnknownErrorViewData();
        assertThat(result, equalTo(UNKNOWN_ERROR_VIEW));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
        assertViewDataForErrorViews(viewData);
    }

    @Test
    public void doExecute_returnsUnknownErrorView_whenErrorQueryParamPresent() {
        // TEST
        action.setAccountName(PRINCIPAL_USER_NAME);
        action.setPrincipalUuid("someid");
        action.setError("BitbucketError");
        final String result = action.doExecute();

        // VERIFY
        final Map<String, Object> viewData = action.getUnknownErrorViewData();
        assertThat(result, equalTo(UNKNOWN_ERROR_VIEW));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
        assertViewDataForErrorViews(viewData);
    }

    @Test
    public void doExecute_returnsUnknownErrorView_whenErrorDescriptionQueryParamPresent() {
        // TEST
        action.setAccountName(PRINCIPAL_USER_NAME);
        action.setPrincipalUuid("someid");
        action.setError_description("Some error occured");
        final String result = action.doExecute();

        // VERIFY
        final Map<String, Object> viewData = action.getUnknownErrorViewData();
        assertThat(result, equalTo(UNKNOWN_ERROR_VIEW));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
        assertViewDataForErrorViews(viewData);
    }

    @Test
    public void doExecute_returnsRedirectToAdminView_whenOrganizationAlreadyApproved() {
        // PREPARE
        final String principalUuid = "{1234}";
        when(aciInstallationService.get(BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID, principalUuid))
                .thenReturn(installation);
        when(organization.getApprovalState()).thenReturn(Organization.ApprovalState.APPROVED);
        when(urlProvider.dvcsConnectorAdminPageUrl()).thenReturn(ORG_ALREADY_APPROVED_REDIRECT_URL);

        // TEST
        action.setPrincipalUuid(principalUuid);
        final String result = action.doExecute();
        final Map<String, Object> viewData = action.getRedirectToAdminViewData();

        // VERIFY
        assertThat(result, equalTo(REDIRECT_TO_ADMIN_VIEW));
        assertThat(viewData.size(), equalTo(1));
        assertThat(viewData.get("redirectUrl"), equalTo(ORG_ALREADY_APPROVED_REDIRECT_URL));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
    }

    @Test
    public void doExecute_returnsRedirectToAdminView_whenUserDeclinesScopeApprovalInBitbucket() {
        // PREPARE
        final String principalUuid = "{1234}";
        when(aciInstallationService.get(BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID, principalUuid))
                .thenReturn(installation);
        when(organization.getApprovalState()).thenReturn(Organization.ApprovalState.APPROVED);
        when(urlProvider.dvcsConnectorAdminPageUrl()).thenReturn(ORG_ALREADY_APPROVED_REDIRECT_URL);

        // TEST
        action.setPrincipalUuid(principalUuid);
        action.setError(BITBUCKET_ERROR_ACCESS_DENIED);
        final String result = action.doExecute();
        final Map<String, Object> viewData = action.getRedirectToAdminViewData();

        // VERIFY
        assertThat(result, equalTo(REDIRECT_TO_ADMIN_VIEW));
        assertThat(viewData.size(), equalTo(1));
        assertThat(viewData.get("redirectUrl"), equalTo(ORG_ALREADY_APPROVED_REDIRECT_URL));
        verify(requiredResources).requireWebResource(REQUIRE_RESOURCE_KEY);
    }

    private void assertViewDataForErrorViews(final Map<String, Object> viewData) {
        assertThat(viewData.size(), equalTo(4));
        assertThat(viewData.get("accountName"), equalTo(PRINCIPAL_USER_NAME));
        assertThat(viewData.get("startAgainButtonUrl"), equalTo(START_AGAIN_BUTTON_URL_FOR_ERROR_VIEW));
        assertThat(viewData.get("cancelButtonUrl"), equalTo(CANCEL_BUTTON_URL_FOR_ERROR_VIEW));
        assertThat(viewData.get("jiraInstanceTitle"), equalTo(JIRA_TEST_TITLE));
    }
}