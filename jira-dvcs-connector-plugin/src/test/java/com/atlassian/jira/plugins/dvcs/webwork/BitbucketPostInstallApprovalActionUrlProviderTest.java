package com.atlassian.jira.plugins.dvcs.webwork;

import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.util.TestNGMockComponentContainer;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class BitbucketPostInstallApprovalActionUrlProviderTest {
    public static final String BB_ADDON_DIRECTORY_RELATIVE_URL = "/account/user/account+Name%25/addon-directory";
    public static final String BB_ADDON_MANAGEMENT_RELATIVE_URL = "/account/user/account+Name%25/addon-management";
    public static final String LOGOUT_RELATIVE_URL = "/secure/Logout.jspa";
    private static final String JIRA_TEST_BASEURL = "http://someurl";
    private static final String INSTALLATION_BASE_URL = "http://some.bitbucket.org";
    private static final String EXPECTED_DVCS_CONNECTOR_ADMIN_URL =
            JIRA_TEST_BASEURL + "/secure/admin/ConfigureDvcsOrganizations!default.jspa";
    private static final String EXPECTED_REFRESH_ORG_ADMIN_URL = EXPECTED_DVCS_CONNECTOR_ADMIN_URL + "#connectionSuccessful?orgId=1";
    private static final String EXPECTED_JIRA_DASHBOARD_URL = JIRA_TEST_BASEURL;
    private final TestNGMockComponentContainer mockComponentContainer = new TestNGMockComponentContainer(this);
    @Mock
    Installation installation;
    @Mock
    @AvailableInContainer
    private ApplicationProperties salApplicationProperties;
    @Mock
    @AvailableInContainer
    private com.atlassian.jira.config.properties.ApplicationProperties jiraApplicationProperties;
    @InjectMocks
    private BitbucketPostInstallApprovalActionUrlProvider urlProvider;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        mockComponentContainer.beforeMethod();

        when(jiraApplicationProperties.getEncoding()).thenReturn("UTF-8");
        when(salApplicationProperties.getBaseUrl(UrlMode.AUTO)).thenReturn(JIRA_TEST_BASEURL);
        when(installation.getBaseUrl()).thenReturn(INSTALLATION_BASE_URL);
    }

    @Test
    public void bitbucketBaseUrl_returnsInstallationBaseUrl_whenInstallationNotNull() {
        // TEST
        final String result = urlProvider.bitbucketBaseUrl(Optional.ofNullable(installation));
        // VERIFY
        assertThat(result, equalTo(INSTALLATION_BASE_URL));
    }

    @Test
    public void bitbucketBaseUrl_returnsDefaultBitbucketUrl_whenInstallationNull() {
        // TEST
        final String result = urlProvider.bitbucketBaseUrl(Optional.empty());
        // VERIFY
        assertThat(result, equalTo(BitbucketPostInstallApprovalActionUrlProvider.BITBUCKET_BASE_URL));
    }

    @Test
    public void grantAccessButtonRedirectUrl() {
        final Organization org = new Organization();
        org.setId(1);

        // TEST
        final String result = urlProvider.grantAccessButtonRedirectUrl(org);
        // VERIFY
        assertThat(result, equalTo(EXPECTED_REFRESH_ORG_ADMIN_URL));
    }

    @Test
    public void dvcsConnectorAdminPageUrl() {
        // TEST
        final String result = urlProvider.dvcsConnectorAdminPageUrl();
        // VERIFY
        assertThat(result, equalTo(EXPECTED_DVCS_CONNECTOR_ADMIN_URL));
    }

    @Test
    public void cancelButtonUrlForApprovalView_returnsDvcsAdminUrl_whenIsJiraInitiated() {
        // TEST
        final String result = urlProvider.cancelButtonUrlForApprovalView(true, null, null);
        // VERIFY
        assertThat(result, equalTo(EXPECTED_DVCS_CONNECTOR_ADMIN_URL));
    }

    @Test
    public void cancelButtonUrlForApprovalView_returnsDefaultBbHomepageUrl_whenNotStartedInJiraAndInformationMissing() {
        // TEST
        final String result = urlProvider.cancelButtonUrlForApprovalView(false, null, null);
        // VERIFY
        assertThat(result, equalTo(BitbucketPostInstallApprovalActionUrlProvider.BITBUCKET_BASE_URL));
    }

    @Test
    public void cancelButtonUrlForApprovalView_returnsDefaultBbHomepageUrl_whenNotStartedInJiraAndInstallationMissing() {
        // TEST
        final String result = urlProvider.cancelButtonUrlForApprovalView(false, "account Name%", null);
        // VERIFY
        assertThat(result, equalTo(BitbucketPostInstallApprovalActionUrlProvider.BITBUCKET_BASE_URL
                + BB_ADDON_MANAGEMENT_RELATIVE_URL));
    }

    @Test
    public void cancelButtonUrlForApprovalView_returnsBbHomepageUrl_whenNotStartedInJiraAndInstallationMissing() {
        // TEST
        final String result = urlProvider.cancelButtonUrlForApprovalView(false, "account Name%", installation);
        // VERIFY
        assertThat(result, equalTo(INSTALLATION_BASE_URL + BB_ADDON_MANAGEMENT_RELATIVE_URL));
    }

    @Test
    public void startAgainButtonUrlForErrorView_returnsDvcsAdminUrl_whenIsJiraInitiated() {
        // TEST
        final String result = urlProvider.startAgainButtonUrlForErrorView(true, null, null);
        // VERIFY
        assertThat(result, equalTo(EXPECTED_DVCS_CONNECTOR_ADMIN_URL));
    }

    @Test
    public void startAgainButtonUrlForErrorView_returnsDefaultBbHomepageUrl_whenNotStartedInJiraAndInformationMissing() {
        // TEST
        final String result = urlProvider.startAgainButtonUrlForErrorView(false, null, null);
        // VERIFY
        assertThat(result, equalTo(BitbucketPostInstallApprovalActionUrlProvider.BITBUCKET_BASE_URL));
    }

    @Test
    public void startAgainButtonUrlForErrorView_returnsDefaultBbHomepageUrl_whenNotStartedInJiraAndInstallationMissing() {
        // TEST
        final String result = urlProvider.startAgainButtonUrlForErrorView(false, "account Name%", null);
        // VERIFY
        assertThat(result, equalTo(BitbucketPostInstallApprovalActionUrlProvider.BITBUCKET_BASE_URL
                + BB_ADDON_DIRECTORY_RELATIVE_URL));
    }

    @Test
    public void startAgainButtonUrlForErrorView_returnsBbHomepageUrl_whenNotStartedInJiraAndInstallationMissing() {
        // TEST
        final String result = urlProvider.startAgainButtonUrlForErrorView(false, "account Name%", installation);
        // VERIFY
        assertThat(result, equalTo(INSTALLATION_BASE_URL + BB_ADDON_DIRECTORY_RELATIVE_URL));
    }


    @Test
    public void cancelButtonUrlForErrorView_returnsDvcsAdminUrl_whenIsJiraInitiated() {
        // TEST
        final String result = urlProvider.cancelButtonUrlForErrorView(true, null, null);
        // VERIFY
        assertThat(result, equalTo(EXPECTED_DVCS_CONNECTOR_ADMIN_URL));
    }

    @Test
    public void cancelButtonUrlForErrorView_returnsDefaultBbHomepageUrl_whenNotStartedInJiraAndInformationMissing() {
        // TEST
        final String result = urlProvider.cancelButtonUrlForErrorView(false, null, null);
        // VERIFY
        assertThat(result, equalTo(BitbucketPostInstallApprovalActionUrlProvider.BITBUCKET_BASE_URL));
    }

    @Test
    public void cancelButtonUrlForErrorView_returnsDefaultBbHomepageUrl_whenNotStartedInJiraAndInstallationMissing() {
        // TEST
        final String result = urlProvider.cancelButtonUrlForErrorView(false, "account Name%", null);
        // VERIFY
        assertThat(result, equalTo(BitbucketPostInstallApprovalActionUrlProvider.BITBUCKET_BASE_URL
                + BB_ADDON_DIRECTORY_RELATIVE_URL));
    }

    @Test
    public void cancelButtonUrlForErrorView_returnsBbHomepageUrl_whenNotStartedInJiraAndInstallationMissing() {
        // TEST
        final String result = urlProvider.cancelButtonUrlForErrorView(false, "account Name%", installation);
        // VERIFY
        assertThat(result, equalTo(INSTALLATION_BASE_URL + BB_ADDON_DIRECTORY_RELATIVE_URL));
    }

    @Test
    public void logoutButtonUrl_returnsCorrectUrl() {
        // TEST
        final String result = urlProvider.logoutButtonUrl();
        // VERIFY
        assertThat(result, equalTo(JIRA_TEST_BASEURL + LOGOUT_RELATIVE_URL));
    }

    @Test
    public void jiraDashboardUrl_returnsCorrectUrl() {
        // TEST
        final String result = urlProvider.jiraDashboardUrl();
        // VERIFY
        assertThat(result, equalTo(JIRA_TEST_BASEURL));
    }

    @Test
    public void okButtonForNonAdminViewUrlForErrorView_returnsJiraDashboardUrl_whenIsJiraInitiated() {
        // TEST
        final String result = urlProvider.okButtonForNonAdminView(true, null, null);
        // VERIFY
        assertThat(result, equalTo(EXPECTED_JIRA_DASHBOARD_URL));
    }

    @Test
    public void okButtonForNonAdminViewUrlForErrorView_returnsDefaultBbHomepageUrl_whenNotStartedInJiraAndInformationMissing() {
        // TEST
        final String result = urlProvider.okButtonForNonAdminView(false, null, null);
        // VERIFY
        assertThat(result, equalTo(BitbucketPostInstallApprovalActionUrlProvider.BITBUCKET_BASE_URL));
    }

    @Test
    public void okButtonForNonAdminViewUrlForErrorView_returnsDefaultBbHomepageUrl_whenNotStartedInJiraAndInstallationMissing() {
        // TEST
        final String result = urlProvider.okButtonForNonAdminView(false, "account Name%", null);
        // VERIFY
        assertThat(result, equalTo(BitbucketPostInstallApprovalActionUrlProvider.BITBUCKET_BASE_URL
                + BB_ADDON_DIRECTORY_RELATIVE_URL));
    }

    @Test
    public void okButtonForNonAdminViewUrlForErrorView_returnsBbHomepageUrl_whenNotStartedInJiraAndInstallationMissing() {
        // TEST
        final String result = urlProvider.okButtonForNonAdminView(false, "account Name%", installation);
        // VERIFY
        assertThat(result, equalTo(INSTALLATION_BASE_URL + BB_ADDON_DIRECTORY_RELATIVE_URL));
    }
}