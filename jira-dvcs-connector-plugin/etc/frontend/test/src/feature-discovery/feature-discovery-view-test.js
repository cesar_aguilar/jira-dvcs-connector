define([
    'fusion/test/qunit',
    'fusion/test/hamjest',
    'jquery',
    'fusion/test/mock-declaration',
    'fusion/test/mock-view',
    'fusion/test/mock-dialog2',
    'fusion/test/backbone'
], function (QUnit,
             __,
             $,
             MockDeclaration,
             MockView,
             mockDialog2,
             Backbone
){

    QUnit.module('jira-dvcs-connector/feature-discovery/feature-discovery-view', {
        require: {
            main: 'jira-dvcs-connector/feature-discovery/feature-discovery-view'
        },
        mocks: {
            backbone: QUnit.moduleMock('jira-dvcs-connector/lib/backbone', function() {
                return Backbone;
            }),
            dialog2: QUnit.moduleMock('jira-dvcs-connector/aui/dialog2',
                function() { return mockDialog2; }
            )
        },
        templates: {
            "JIRA.Templates.DVCSConnector.FeatureDiscovery.dialog": function() {
                return `
                    <div id="dvcs-feature-discovery-dialog">
                        <button class="aui-dialog2-header-close"></button>
                        <aui-tour page="111">
                            <div class="aui-tour-container">
                                <div class="aui-tour-content">
                                    <div class="aui-tour-page" aria-hidden="false">
                                        <div class="dvcs-feature-discovery-link-container"></div>
                                    </div>
                                </div>
                            </div>
                            <button class="aui-tour-next"></button>
                        </aui-tour>
                    </div>`;
            }
        },
        beforeEach: function(assert, FeatureDiscoveryView) {
            this.view = new FeatureDiscoveryView();
        }
    });

    QUnit.test('Calling show shows the dialog',
        function(assert) {
            // setup
            var featureDiscoveryDialog = this.view;

            // execute
            featureDiscoveryDialog.show();

            // verify
            assert.assertThat(featureDiscoveryDialog.dialog.wasShown(), __.is(true));
        }
    );

    QUnit.test('Calling close triggers a closed event after removing the dialog',
        function(assert) {
            // setup
            var featureDiscoveryDialog = this.view;

            // verify
            var confirmDialogClosed = assert.async();
            featureDiscoveryDialog.on('closed', function() {
                assert.assertThat(featureDiscoveryDialog.dialog.wasRemoved(), __.is(true));
                confirmDialogClosed();
            });

            // execute
            featureDiscoveryDialog.close();
        }
    );

    QUnit.test('The feature discovery is aborted when a user hides the dialog (ESC etc.)',
        function(assert) {
            // setup
            var featureDiscoveryDialog = this.view;

            // verify
            var confirmTourAborted = assert.async();
            featureDiscoveryDialog.on('tourAborted', function() {
                assert.assertThat(featureDiscoveryDialog.dialog.wasRemoved(), __.is(false));
                confirmTourAborted();
            });

            var confirmDialogClosed = assert.async();
            featureDiscoveryDialog.on('closed', function() {
                assert.assertThat(featureDiscoveryDialog.dialog.wasRemoved(), __.is(true));
                confirmDialogClosed();
            });

            // execute
            featureDiscoveryDialog.dialog.trigger('hide');
        }
    );

    QUnit.test('The feature discovery is aborted when a user closes the dialog',
        function(assert) {
            // setup
            var featureDiscoveryDialog = this.view;

            // verify
            var confirmTourAborted = assert.async();
            featureDiscoveryDialog.on('tourAborted', function() {
                assert.assertThat(featureDiscoveryDialog.dialog.wasRemoved(), __.is(false));
                confirmTourAborted();
            });

            var confirmDialogClosed = assert.async();
            featureDiscoveryDialog.on('closed', function() {
                assert.assertThat(featureDiscoveryDialog.dialog.wasRemoved(), __.is(true));
                confirmDialogClosed();
            });

            // execute
            featureDiscoveryDialog.$el.find('.aui-dialog2-header-close').click();
        }
    );

    QUnit.test('The feature discovery is completed when a user clicks the final button',
        function(assert) {
            // setup
            var featureDiscoveryDialog = this.view;

            // verify
            var confirmTourFinished = assert.async();
            featureDiscoveryDialog.on('tourFinished', function() {
                assert.assertThat(featureDiscoveryDialog.dialog.wasRemoved(), __.is(false));
                confirmTourFinished();
            });

            var confirmDialogClosed = assert.async();
            featureDiscoveryDialog.on('closed', function() {
                assert.assertThat(featureDiscoveryDialog.dialog.wasRemoved(), __.is(true));
                confirmDialogClosed();
            });

            // execute
            featureDiscoveryDialog.$el.find('aui-tour').trigger('aui-tour-finish');
        }
    );

    QUnit.test('Page change hides links on hidden pages',
        function(assert) {

            // setup
            var featureDiscoveryDialog = this.view;
            featureDiscoveryDialog._userHasTriggeredActualPageChange = true;

            _markTourPageAsHidden(featureDiscoveryDialog, true);

            var confirmPageChanged = assert.async();
            featureDiscoveryDialog.on('pageChanged', function() {
                // trigger the transition end after we receive the page change event to ensure
                // the handler has been registered
                _triggerPageTransitionEnd(featureDiscoveryDialog);
                confirmPageChanged();
            });

            // verify
            var confirmLinkElementsShown = assert.async();
            featureDiscoveryDialog.on('_pageLinkElementsShown', function() {
                _assertLinkContainerIsVisible(assert, featureDiscoveryDialog, true);
                confirmLinkElementsShown();
            });

            var confirmLinkElementsHidden = assert.async();
            featureDiscoveryDialog.on('_pageLinkElementsHidden', function() {
                _assertLinkContainerIsVisible(assert, featureDiscoveryDialog, false);
                confirmLinkElementsHidden();
            });

            // execute
            _triggerAuiTourPageChange(featureDiscoveryDialog);
        }
    );

    QUnit.test('Page change does not hide links on visible pages',
        function(assert) {

            // setup
            var featureDiscoveryDialog = this.view;
            featureDiscoveryDialog._userHasTriggeredActualPageChange = true;

            _markTourPageAsHidden(featureDiscoveryDialog, false);

            var confirmPageChanged = assert.async();
            featureDiscoveryDialog.on('pageChanged', function() {
                // trigger the transition end after we receive the page change event to ensure
                // the handler has been registered
                _triggerPageTransitionEnd(featureDiscoveryDialog);
                confirmPageChanged();
            });

            // verify
            var confirmLinkElementsShown = assert.async();
            featureDiscoveryDialog.on('_pageLinkElementsShown', function() {
                _assertLinkContainerIsVisible(assert, featureDiscoveryDialog, true);
                confirmLinkElementsShown();
            });

            var confirmLinkElementsHidden = assert.async();
            featureDiscoveryDialog.on('_pageLinkElementsHidden', function() {
                _assertLinkContainerIsVisible(assert, featureDiscoveryDialog, true);
                confirmLinkElementsHidden();
            });

            // execute
            _triggerAuiTourPageChange(featureDiscoveryDialog);
        }
    );

    QUnit.test('Nav element click sets the user triggered flag and causes a page change',
        function(assert) {
            // setup
            var featureDiscoveryDialog = this.view;
            featureDiscoveryDialog._userHasTriggeredActualPageChange = false;

            // verify
            var confirmPageChanged = assert.async();
            featureDiscoveryDialog.on('pageChanged', function() {
                assert.assertThat(featureDiscoveryDialog._userHasTriggeredActualPageChange, __.is(true));
                confirmPageChanged();
            });

            // execute
            _clickNext(featureDiscoveryDialog);
        }
    );

    QUnit.test('Get current page index retrieves the current tour page index',
        function(assert) {
            assert.assertThat(this.view.getCurrentPageIndex(), __.is(111));
        }
    );

    function _clickNext(featureDiscoveryDialog) {
        featureDiscoveryDialog.$el.find('.aui-tour-next').click();
    }

    function _triggerAuiTourPageChange(featureDiscoveryDialog) {
        featureDiscoveryDialog.$el.find('aui-tour').trigger('aui-tour-page-change');
    }

    function _triggerPageTransitionEnd(featureDiscoveryDialog) {
        featureDiscoveryDialog.$el.find('.aui-tour-content').trigger('transitionend');
    }

    function _markTourPageAsHidden(featureDiscoveryDialog, hidden) {
        featureDiscoveryDialog.$el.find('.aui-tour-page').attr('aria-hidden', hidden);
    }

    function _assertLinkContainerIsVisible(assert, featureDiscoveryDialog, visible) {
        assert.assertThat('Link container should be ' +  (visible ? 'visible' : 'hidden'),
            featureDiscoveryDialog.$el.find('.dvcs-feature-discovery-link-container').hasClass('hidden'),
            __.is(!visible));
    }

});