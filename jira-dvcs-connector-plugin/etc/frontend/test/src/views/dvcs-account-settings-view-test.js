define([
    'fusion/test/qunit',
    'fusion/test/jquery',
    'fusion/test/hamjest',
    'fusion/test/backbone',
    'underscore'

], function (QUnit,
             $,
             __,
             Backbone,
             _) {

    const NotificationMock = {
        showError: sinon.spy(),
        showWarning: sinon.spy()
    };

    const MockedContextPath = () => sinon.spy();

    const ConfigureAccountDialogMock = Backbone.View.extend({
        initialize: sinon.spy()
    });

    const ModelMock = Backbone.Model.extend({
        getId: () => 1,
        getName: () => "orgName",
        isReposDefaultEnabled: () => true,
        isSmartCommitsDefaultEnabled: () => true,
    });

    QUnit.module('jira-dvcs-connector/bitbucket/views/dvcs-account-settings', {
        require: {
            main: 'jira-dvcs-connector/bitbucket/views/dvcs-account-settings',
            backbone: 'fusion/test/backbone'
        },

        templates: {},

        mocks: {
            jquery: QUnit.moduleMock('jquery', () => $),
            underscore: QUnit.moduleMock('underscore', () => _),
            backbone: QUnit.moduleMock('jira-dvcs-connector/lib/backbone', () => Backbone),
            ConfigureAccountDialog: QUnit.moduleMock('jira-dvcs-connector/bitbucket/views/default-account-setting',
                () => ConfigureAccountDialogMock),
            contextPath: QUnit.moduleMock('wrm/context-path', () => MockedContextPath),
            Notification: QUnit.moduleMock('jira-dvcs-connector/admin/dvcs-notifications', () => NotificationMock)

        },

        setupFixture: function () {
            this.$el = $('<div class="the-view-el"></div>');
            this.fixture.append(this.$el);
            this.$el.append('<ul><li class="default-setting-item"></li></ul>');
        },

        beforeEach: function (assert) {
            this.setupFixture();

        },

        afterEach: function () {
            ConfigureAccountDialogMock.prototype.initialize.reset();
            MockedContextPath().reset();
            NotificationMock.showError.reset();
            NotificationMock.showWarning.reset();
        }
    });

    QUnit.test('should create dialog on click', function (assert, accountSettingsView) {
        const settingsView = new accountSettingsView({
            el: this.$el,
            model: new ModelMock()
        });
        const li = this.fixture.find('.default-setting-item');
        li.click();

        assert.assertThat('should call create instance of account settings dialog', ConfigureAccountDialogMock.prototype.initialize.callCount, __.equalTo(1));
        assert.assertThat('should pass correct parameters to settings dialog', ConfigureAccountDialogMock.prototype.initialize.firstCall.args[0],
            __.hasProperties({
                organizationId: 1,
                organizationName: "orgName",
                autoLinkEnabled: true,
                smartCommitsEnabled: true
            }));
    });

});