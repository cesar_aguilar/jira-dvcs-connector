require([
    'jquery',
    'jira-dvcs-connector/bitbucket/views/dvcs-accounts-page'
], function ($,
             DVCSPage) {
    'use strict';

    $(function () {
        new DVCSPage({
            el: 'section#upper-section'
        });
    });
});
