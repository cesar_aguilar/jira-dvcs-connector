/**
 * Module for the configure organisation list dropdown.
 *
 * @module jira-dvcs-connector/bitbucket/views/dvcs-account-settings
 */
define('jira-dvcs-connector/bitbucket/views/dvcs-account-settings', [
    'jquery',
    'underscore',
    'jira-dvcs-connector/lib/backbone',
    'jira-dvcs-connector/bitbucket/views/default-account-setting',
    'wrm/context-path',
    'jira-dvcs-connector/admin/dvcs-notifications'
], function ($,
             _,
             Backbone,
             ConfigureSettingsDialog,
             contextPath,
             Notification) {
    'use strict';

    return Backbone.View.extend({
        events: {
            "click .default-setting-item": '_openDefaultSettingsDialog'
        },

        render: function () {
        },

        _openDefaultSettingsDialog: function () {
            var self = this;

            var configureSettingsDialog = new ConfigureSettingsDialog({
                organizationId: self.model.getId(),
                organizationName: self.model.getName(),
                baseUrl: contextPath(),
                autoLinkEnabled: self.model.isReposDefaultEnabled(),
                smartCommitsEnabled: self.model.isSmartCommitsDefaultEnabled(),
                onSuccess: function (newState) {
                    self.model.setReposDefaultEnabled(newState.autoLinkReposEnabled);
                    self.model.setSmartCommitsDefaultEnabled(newState.smartCommitsEnabled);
                },
                onError: function () {
                    Notification.showError(
                        AJS.I18n.getText('com.atlassian.jira.plugins.dvcs.admin.error.unexpected'),
                        ".notifications");
                }
            });
            configureSettingsDialog.render();
        }

    });
});