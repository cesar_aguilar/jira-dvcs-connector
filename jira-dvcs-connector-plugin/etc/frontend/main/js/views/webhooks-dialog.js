define('jira-dvcs-connector/bitbucket/views/webhooks-dialog',
    ['jira-dvcs-connector/aui/dialog2'],
    function(dialog2) {

        "use strict";

        return {
            showWebhooksDialog: function(accountId, repoWebhooks) {
                var dialog = dialog2(dvcs.connector.plugin.soy.webhooksWarning({
                    repoWebhooks: repoWebhooks,
                    accountId: accountId
                }));

                dialog.on('show', function() {
                    var buttonClose = $(this).find('.aui-button');
                    buttonClose.focus();
                    buttonClose.on('click', function() {
                        dialog.remove();
                    });
                });

                dialog.show();
            }
        };
});
