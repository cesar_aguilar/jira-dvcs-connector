define('jira-dvcs-connector/bitbucket/views/dvcs-repos-table-view', [
    'jira-dvcs-connector/lib/backbone',
    'jira-dvcs-connector/bitbucket/views/dvcs-repo-row-view'
], function (Backbone,
             RepRowView) {

    "use strict";

    return Backbone.View.extend({
        tbodySelector: 'tbody',

        initialize: function (options) {
            this.repos = options.collection;
            this.syncDisabled = options.syncDisabled;

            this.collection.on('change:linked', this._repoChange, this);
            this.collection.on('add', this._repoChange, this);
            this.collection.on('remove', this._repoDeleted, this);

            this.repoViews = {};
        },

        _repoChange: function(model) {
            if (model.isEnabled()) {
                this._addRepoRow(model);
            } else {
                this._removeRepoRow(model);
            }
        },

        render: function() {
            this.tbody = this.$el.find(this.tbodySelector);
            var self = this;
            this.collection.filterEnabled().each(function(repo) {
                self._addRepoRow(repo);
            });
        },

        _addRepoRow: function(repo) {
            var rowIndex = this.collection.filterEnabled().indexOf(repo);
            var syncDisabled = this.syncDisabled;
            var rowHtml = dvcs.connector.plugin.soy.dvcs.accounts.repoRow({repo: repo.toJSON(), syncDisabled: syncDisabled});

            // append row in correct place
            if (rowIndex === 0) {
                this.tbody.prepend(rowHtml);
            } else if (rowIndex < this.tbody.find('tr').length) {
                this.tbody.find('tr').eq(rowIndex - 1).after(rowHtml);
            } else {
                this.tbody.append(rowHtml);
            }

            this.repoViews[repo.id] = new RepRowView({
                el: '#dvcs-repo-row-' + repo.id,
                model: repo
            });
        },

        _repoDeleted: function(repo) {
            // remove repo if it was enabled
            if (repo.isEnabled()) {
                this._removeRepoRow(repo);
            }
        },

        _removeRepoRow: function(repo) {
            this.tbody.find('#dvcs-repo-row-' + repo.id).remove();
            var repoRowView = this.repoViews[repo.id];
            if(repoRowView) {
                repoRowView.destroy();
            }
            delete this.repoViews[repo.id];
        }
    });
});