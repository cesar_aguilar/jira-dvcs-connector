define('jira-dvcs-connector/bitbucket/models/dvcs-account-model', [
    'jira-dvcs-connector/lib/backbone',
    'jira-dvcs-connector/util/window'
], function (Backbone, window) {
    "use strict";

    return Backbone.Model.extend({
        idAttribute: 'id',

        url: function () {
            return window.BASE_URL + '/rest/bitbucket/1.0/organization/' + this.getId();
        },

        getName: function () {
            return this.get('name');
        },

        getId: function () {
            return this.get('id');
        },

        isApproved: function () {
            return this.get('approvalState') == 'APPROVED';
        },

        hasLinkedRepositories: function () {
            return this.get('hasLinkedRepositories');
        },

        setHasLinkedRepositories: function (hasLinkedRepositories) {
            this.set({
                hasLinkedRepositories: hasLinkedRepositories
            });
        },

        isSmartCommitsDefaultEnabled: function () {
            return this.get('smartCommitsDefault');
        },

        setSmartCommitsDefaultEnabled: function (smartcommitsOnNewRepos) {
            this.set({
                smartCommitsDefault: smartcommitsOnNewRepos
            });
        },

        isReposDefaultEnabled: function () {
            return this.get('reposDefaultEnabled');
        },

        setReposDefaultEnabled: function (reposDefaultEnabled) {
            this.set(
                {
                    reposDefaultEnabled: reposDefaultEnabled
                });
        },
    });
});