/**
 * A simple AMD module for wrapping all dvcs js constants
 */
define('jira-dvcs-connector/util/constants', [], function () {

    return {
        // render 10 repos every 100 milli second
        REPOSITORIES_CHUNK_SIZE : 10,
        REPOSITORIES_RENDERING_INTERVAL: 100
    }
});
