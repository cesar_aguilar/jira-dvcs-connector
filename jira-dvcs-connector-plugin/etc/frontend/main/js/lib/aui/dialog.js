/**
 * An AMD wrapper around the AUI Dialog type
 *
 * @module jira-dvcs-connector/aui/dialog
 * @deprecated
 */
define('jira-dvcs-connector/aui/dialog', [], function () {
    'use strict';
    return AJS.Dialog;
});