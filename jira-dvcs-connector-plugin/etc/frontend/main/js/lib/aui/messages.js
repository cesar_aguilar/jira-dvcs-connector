/**
 * AMD wrapper around the AUI messages mechanism
 *
 * @module jira-dvcs-connector/aui/messages
 */
define('jira-dvcs-connector/aui/messages', [], function () {
    return AJS.messages;
});