define('jira-dvcs-connector/lib/backbone', [
    'atlassian/libs/factories/backbone-1.0.0',
    'jira-dvcs-connector/lib/underscore',
    'jquery'
], function (BackboneFactory, _, $) {
    return BackboneFactory(_, $);
})