/**
 * The parent dialogue for configuring the default behaviour of a repository
 *
 * @module jira-dvcs-connector/bitbucket/views/repo-config-dialog
 */
define('jira-dvcs-connector/bitbucket/views/repo-config-dialog', ['require'], function (require) {
    "use strict";

    var $ = require('jquery');
    var Backbone = require('backbone');
    var dialog2 = require('jira-dvcs-connector/aui/dialog2');

    var DvcsConnectorRestClient = require('jira-dvcs-connector/rest/dvcs-connector-rest-client');
    var contextPath = require('wrm/context-path');
    var restClient = new DvcsConnectorRestClient(contextPath());

    /**
     * A view that will render the Connection Successful dialog into the page and show it.
     */
    return Backbone.View.extend({

        /**
         * Show the dialog
         *
         * @returns {repo-config-dialog} The view instance to support method chaining
         */
        render: function () {
            this.dialog.show();
            return this;
        },

        _initialize: function () {
            this.setElement(this.dialog.$el);

            this.$submitButton = this.$el.find('#dialog-submit-button');
            this.$submitSpinner = this.$el.find('#dialog-submit-spinner');
            this.$autolinkCheckbox = this.$el.find('#autolink-checkbox');
            this.$smartCommitsCheckbox = this.$el.find('#smartcommits-checkbox');

            this.dialog.on('show', this._show.bind(this));
            this.dialog.on('hide', this._cleanup.bind(this));
        },

        submitUpdates: function (analyticsFunction, dontHideDialog) {
            var self = this;
            self._toggleSpinnerAndElements(false);

            var autoLinkReposEnabled = this._autoLinkReposEnabled();
            var smartCommitsEnabled = this._smartCommitsEnabled();
            var updateRequest = restClient.organization.updateAutoSettings(
                this.orgId, autoLinkReposEnabled, smartCommitsEnabled);
            $.when(updateRequest)
                .done(function () {
                    self.dialog.hide();
                    if (self.onSuccess) {
                        self.onSuccess({
                            autoLinkReposEnabled: autoLinkReposEnabled,
                            smartCommitsEnabled: smartCommitsEnabled
                        });
                    }
                })
                .fail(function () {
                    if (!dontHideDialog) {
                        self.dialog.hide();
                    } else {
                        self._toggleSpinnerAndElements(true);
                    }
                    if (self.onError) {
                        self.onError();
                    }
                });

            if (_.isFunction(analyticsFunction)) {
                analyticsFunction(this.orgId, autoLinkReposEnabled, smartCommitsEnabled);
            }
        },


        _toggleSpinnerAndElements: function(showSpinner){
            var self = this;
            self.$submitSpinner.toggleClass('hidden');
            self.$submitButton.prop('disabled', showSpinner ? false : 'disabled');
            self.$autolinkCheckbox.prop('disabled', showSpinner ? false : 'disabled');
            self.$smartCommitsCheckbox.prop('disabled', showSpinner ? false : 'disabled');
        },

        _show: function () {

        },

        _cleanup: function () {
            this.remove();
            this.dialog.remove();
        },

        _autoLinkReposEnabled: function () {
            return this.$autolinkCheckbox.is(':checked');
        },

        _smartCommitsEnabled: function () {
            return this.$smartCommitsCheckbox.is(':checked');
        }
    });

});
