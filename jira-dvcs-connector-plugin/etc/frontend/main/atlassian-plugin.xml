<atlassian-plugin key="com.atlassian.jira.plugins.jira-bitbucket-connector-plugin" name="${project.name}"
                  plugins-version="2">

    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}"/>
        <param name="atlassian-data-center-compatible">true</param>
        <bundle-instructions>
            <!-- These are now in jira-dvcs-connector-plugin/pom.xml, in the maven-jira-plugin config -->
        </bundle-instructions>
    </plugin-info>

    <!-- Active Objects -->
    <ao key="ao-module" name="Active Objects Module"
        namespace="com.atlassian.jira.plugins.jira-bitbucket-connector-plugin">
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationToProjectMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryToChangesetMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryToProjectMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.IssueToChangesetMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.ChangesetMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.BranchHeadMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.BranchMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.IssueToBranchMapping</entity>

        <entity>com.atlassian.jira.plugins.dvcs.activity.RepositoryCommitMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestToCommitMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestIssueKeyMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activity.PullRequestParticipantMapping</entity>

        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.GitHubEventMapping</entity>

        <!-- Messaging -->
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageTagMapping</entity>
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageQueueItemMapping</entity>

        <!-- Sync events-->
        <entity>com.atlassian.jira.plugins.dvcs.event.SyncEventMapping</entity>

        <!-- Audit 
        -->
        <entity>com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping</entity>

        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.PropertyMigrator</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.Uri2UrlMigrator</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.v2.To_04_ActiveObjectsV2Migrator</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.v2.To_05_RepositoryTypeMigrator</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.v2.To_06_GithubRepositories</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.CleanupPluginSettings</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.v3.To_08_ActiveObjectsV3Migrator</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.v3.To_09_SmartCommitsColumnsMigrator</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.v3.To_10_LastChangesetNodeMigrator</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.v3.To_11_AddKeyAndSecretToBBAccounts</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.v3.To_12_SplitUpChangesetsMigrator</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.v3.To_13_RemoveFutureChangesets</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.v3.To_15_LinkUpdateAuthorisedInitialise</upgradeTask>
        <upgradeTask>com.atlassian.jira.plugins.dvcs.activeobjects.v3.To_16_OrganizationAddApprovalStateColumn
        </upgradeTask>
    </ao>

    <analytics-whitelist key="dvcs-whitelist" resource="whitelist/dvcs-whitelist.json"/>

    <!-- Integrated account -->

    <navigation-links key="bitbucket-account-nav-links"
                      class="com.atlassian.jira.plugins.dvcs.navlinks.BitbucketCloudAccountNavLinkRepository"
                      name="Bitbucket Cloud Account Navigation Links"/>

    <!-- The webwork1 plugin element is how you add new actions to JIRA -->
    <webwork1 key="repositoryConfiguration" name="Repository Configuration" class="java.lang.Object">
        <!-- Note that HTML in the description element is ignored -->
        <description>
        </description>
        <actions>
            <action name="com.atlassian.jira.plugins.dvcs.webwork.ConfigureDvcsOrganizations"
                    alias="ConfigureDvcsOrganizations" roles-required="admin">
                <view name="input">/templates/dvcs/add-organization.vm</view>
                <view name="unlicensed">/templates/dvcs/unlicensed.vm</view>
            </action>

            <!-- New organization driven actions -->
            <action name="com.atlassian.jira.plugins.dvcs.spi.github.webwork.AddGithubOrganization"
                    alias="AddGithubOrganization" roles-required="admin">
                <view name="input">/templates/dvcs/add-github-organization.vm</view>
            </action>

            <action name="com.atlassian.jira.plugins.dvcs.spi.githubenterprise.webwork.AddGithubEnterpriseOrganization"
                    alias="AddGithubEnterpriseOrganization" roles-required="admin">
                <view name="input">/templates/dvcs/add-githube-organization.vm</view>
            </action>

            <action name="com.atlassian.jira.plugins.dvcs.spi.bitbucket.webwork.AddBitbucketOrganization"
                    alias="AddBitbucketOrganization" roles-required="admin">
                <view name="input">/templates/dvcs/add-bitbucket-organization.vm</view>
            </action>

            <action name="com.atlassian.jira.plugins.dvcs.webwork.AddOrganizationProgressAction"
                    alias="AddOrganizationProgressAction" roles-required="admin">
                <view name="input">/templates/dvcs/add-in-progress.vm</view>

            </action>

            <action name="com.atlassian.jira.plugins.dvcs.spi.github.webwork.RegenerateGithubOauthToken"
                    alias="RegenerateGithubOauthToken" roles-required="admin">
                <view name="input">/templates/dvcs/regenerate-oauth.vm</view>
            </action>

            <action name="com.atlassian.jira.plugins.dvcs.spi.githubenterprise.webwork.RegenerateGithubEnterpriseOauthToken"
                    alias="RegenerateGithubEnterpriseOauthToken" roles-required="admin">
                <view name="input">/templates/dvcs/regenerate-oauth.vm</view>
            </action>

            <action name="com.atlassian.jira.plugins.dvcs.spi.bitbucket.webwork.RegenerateBitbucketOauthToken"
                    alias="RegenerateBitbucketOauthToken" roles-required="admin">
                <view name="input">/templates/dvcs/regenerate-oauth.vm</view>
            </action>

            <action name="com.atlassian.jira.plugins.dvcs.spi.bitbucket.webwork.ConfigureDefaultBitbucketGroups"
                    alias="ConfigureDefaultBitbucketGroups" roles-required="admin">
                <view name="input">/templates/dvcs/add-organization.vm</view>
            </action>

        </actions>
    </webwork1>

    <webwork1 key="bitbucketPostInstallApproval" name="Bitbucket Post-Install Approval" class="java.lang.Object">
        <actions>
            <action name="com.atlassian.jira.plugins.dvcs.webwork.BitbucketPostInstallApprovalAction"
                    alias="BitbucketPostInstallApprovalAction" roles-required="use">
                <view name="approval" type="soy">
                    :soy-templates-bitbucket/dvcs.connector.plugin.soy.bitbucketApproval
                </view>
                <view name="nonAdmin" type="soy">
                    :soy-templates-bitbucket/dvcs.connector.plugin.soy.bitbucketApprovalNonAdmin
                </view>
                <view name="approvalError" type="soy">
                    :soy-templates-bitbucket/dvcs.connector.plugin.soy.bitbucketApprovalError
                </view>
                <view name="unknownError" type="soy">
                    :soy-templates-bitbucket/dvcs.connector.plugin.soy.bitbucketUnknownError
                </view>
                <view name="redirectToAdminView" type="soy">
                    :soy-templates-bitbucket/dvcs.connector.plugin.soy.bitbucketRedirectToAdminView
                </view>
            </action>
        </actions>
    </webwork1>

    <web-resource key="bitbucket-post-install-approval-resources">
        <resource type="download" name="bitbucket-post-install-approval.css"
                  location="css/bitbucket-post-install-approval.less"/>
        <resource type="download" name="rest_dvcs-connector-rest-client.js"
                  location="js/rest/dvcs-connector-rest-client.js"/>
        <resource type="download" name="js/util/window.js" location="js/util/window.js"/>
        <resource type="download" name="util_navigate.js" location="js/util/navigate.js"/>
        <resource type="download" name="bitbucket_views_bitbucket-post-install-approval.js"
                  location="js/bitbucket/views/bitbucket-post-install-approval.js"/>
        <resource type="download" name="bitbucket_views_bitbucket-post-install-approval-init.js"
                  location="js/bitbucket/views/bitbucket-post-install-approval-init.js"/>

        <transformation extension="less">
            <transformer key="lessTransformer"/>
        </transformation>

        <context>com.atlassian.jira.plugins.jira-bitbucket-connector-plugin</context>
        <dependency>com.atlassian.auiplugin:aui-experimental-iconfont</dependency>
    </web-resource>

    <sitemesh key="decoration" path="/secure/BitbucketPostInstallApprovalAction.jspa"/>

    <web-item key="bitbucket_bulk_repo" name="Bitbucket Bulk Repository"
              section="admin_applications_section/admin_applications_integrations_section"
              weight="20">
        <description>Manage DVCS Accounts</description>
        <label>DVCS accounts</label>
        <name>DVCS Name Item</name>

        <link linkId="bitbucket_bulk_repo">/secure/admin/ConfigureDvcsOrganizations!default.jspa</link>
        <condition class="com.atlassian.jira.software.api.conditions.SoftwareGlobalAdminCondition"/>
    </web-item>

    <web-resource name="feature-discovery" key="dvcs-connector-feature-discovery-resources">
        <context>com.atlassian.jira.plugins.jira-bitbucket-connector-plugin</context>

        <dependency>com.atlassian.auiplugin:dialog2</dependency>
        <dependency>com.atlassian.soy.soy-template-plugin:soy-deps</dependency>

        <resource type="download" name="js/lib/aui/tour/tour.js" location="js/lib/aui/tour/tour.js"/>
        <resource type="download" name="js/lib/aui/tour/tour.css" location="js/lib/aui/tour/tour.less"/>

        <resource type="soy" name="feature-discovery.soy" location="soy/feature-discovery.soy"/>
        <resource type="download" name="feature-discovery.soy.js" location="soy/feature-discovery.soy"/>
        <resource type="download" name="feature-discovery-view.js" location="js/feature-discovery/feature-discovery-view.js"/>

        <resource type="download" name="feature-discovery.css" location="css/feature-discovery.less"/>

        <transformation extension="soy">
            <transformer key="soyTransformer"/>
        </transformation>

        <transformation extension="less">
            <transformer key="lessTransformer"/>
        </transformation>

        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>

    </web-resource>

    <!-- Web resources for the main DVCS accounts admin page -->
    <web-resource name="resources" key="resources">
        <context>com.atlassian.jira.plugins.jira-bitbucket-connector-plugin</context>

        <dependency>com.atlassian.auiplugin:aui-dropdown2</dependency>
        <dependency>com.atlassian.auiplugin:dialog2</dependency>
        <dependency>com.atlassian.auiplugin:aui-experimental-iconfont</dependency>
        <dependency>com.atlassian.auiplugin:aui-experimental-expander</dependency>
        <dependency>com.atlassian.auiplugin:aui-select2</dependency>
        <dependency>com.atlassian.auiplugin:aui-spinner</dependency>
        <dependency>com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path</dependency>
        <dependency>com.atlassian.plugin.jslibs:underscore-1.5.2</dependency>
        <dependency>com.atlassian.plugin.jslibs:backbone-1.0.0-factory</dependency>

        <dependency>com.atlassian.soy.soy-template-plugin:soy-deps</dependency>

        <resource type="soy"
                  name="connection-successful-dialog.soy.js"
                  location="soy/bb-connection-successful-dialog.soy"/>
        <transformation extension="soy">
            <transformer key="soyTransformer"/>
        </transformation>

        <resource type="download" name="js/analytics/analytics-client.js" location="js/analytics/analytics-client.js"/>

        <resource type="download" name="bb-connection-successful-dialog.soy.js" location="soy/bb-connection-successful-dialog.soy"/>
        <resource type="download" name="dvcs-sync-progress.soy.js" location="soy/dvcs-sync-progress.soy"/>
        <resource type="soy" name="dvcs-default-account-settings-dialogue.soy.js"
                  location="soy/dvcs-default-account-settings-dialogue.soy"/>

        <resource type="soy" name="dialog-common.soy.js" location="soy/dialog-common.soy"/>

        <resource type="download" name="js/lib/dvcs-underscore.js" location="js/lib/dvcs-underscore.js"/>
        <resource type="download" name="js/lib/dvcs-backbone.js" location="js/lib/dvcs-backbone.js"/>
        <resource type="download" name="js/lib/aui/dialog.js" location="js/lib/aui/dialog.js"/>
        <resource type="download" name="js/lib/aui/dialog2.js" location="js/lib/aui/dialog2.js"/>
        <resource type="download" name="js/lib/aui/messages.js" location="js/lib/aui/messages.js"/>
        <resource type="download" name="js/lib/aui/trigger.js" location="js/lib/aui/trigger.js"/>

        <resource type="download" name="js/util/console.js" location="js/util/console.js"/>
        <resource type="download" name="js/util/window.js" location="js/util/window.js"/>
        <resource type="download" name="js/util/navigate.js" location="js/util/navigate.js"/>
        <resource type="download" name="js/util/settings.js" location="js/util/settings.js"/>
        <resource type="download" name="js/util/constants.js" location="js/util/constants.js"/>

        <resource type="download" name="js/rest/dvcs-connector-rest-client.js"
                  location="js/rest/dvcs-connector-rest-client.js"/>
        <resource type="download" name="js/bitbucket/rest/bitbucket-client.js"
                  location="js/bitbucket/rest/bitbucket-client.js"/>
        <resource type="download" name="js/admin/view/refresh-account-dialog.js"
                  location="js/admin/view/refresh-account-dialog.js"/>
        <resource type="download" name ="js/admin/dvcs-notifications.js"
                  location="js/admin/dvcs-notifications.js"/>
        <resource type="download" name="js/bitbucket/views/connection-successful-dialog.js"
                  location="js/bitbucket/views/connection-successful-dialog.js"/>
        <resource type="download" name="dvcs-default-account-settings-dialogue.soy.js"
                  location="soy/dvcs-default-account-settings-dialogue.soy"/>
        <resource type="download" name="dialog-common.soy.js" location="soy/dialog-common.soy"/>
        <resource type="download" name="js/bitbucket/views/default-account-settings-dialog.js"
                  location="js/bitbucket/views/default-account-settings-dialog.js"/>
        <resource type="download" name="js/bitbucket/views/repo-config-dialog.js"
                  location="js/bitbucket/views/repo-config-dialog.js"/>
        <resource type="download" name="js/views/dvcs-account-settings.js"
                  location="js/views/dvcs-account-settings.js"/>
        <resource type="download" name="js/admin/configure-organization.js"
                  location="js/admin/configure-organization.js"/>
        <resource type="download" name="js/ui/update-sync-status.js" location="js/ui/update-sync-status.js"/>
        <resource type="download" name="javascripts/bitbucket.js" location="javascripts/bitbucket.js"/>
        <resource type="download" name="javascripts/validation.js" location="javascripts/validation.js"/>
        <resource type="download" name="bitbucket_views_bitbucket-admin-page-pending-org-view.js"
                  location="js/bitbucket/views/bitbucket-admin-page-pending-org-view.js"/>
        <resource type="download" name="bitbucket_views_bitbucket-admin-page-org-container-view.js"
                  location="js/bitbucket/views/bitbucket-admin-page-org-container-view.js"/>
        <!-- dvcs account page backbone models -->
        <resource type="download" name="dvcs-account-model.js" location="js/models/dvcs-account-model.js"/>
        <resource type="download" name="dvcs-repo-model.js" location="js/models/dvcs-repo-model.js"/>
        <resource type="download" name="dvcs-repo-collection.js" location="js/models/dvcs-repo-collection.js"/>
        <!-- dvcs account page backbone views -->
        <resource type="download" name="dvcs-repos-table.js" location="js/views/dvcs-repos-table.js"/>
        <resource type="download" name="dvcs-repo-row-view.js" location="js/views/dvcs-repo-row-view.js"/>
        <resource type="download" name="dvcs-post-commit-hook-dialog.js"
                  location="js/views/dvcs-post-commit-hook-dialog.js"/>
        <resource type="download" name="dvcs-account-view.js" location="js/views/dvcs-account-view.js"/>
        <resource type="download" name="dvcs-repo-multi-selector.js" location="js/views/dvcs-repo-multi-selector.js"/>
        <resource type="download" name="webhooks-dialog.js" location="js/views/webhooks-dialog.js"/>
        <resource type="download" name="dvcs-accounts-page.js" location="js/views/dvcs-accounts-page.js"/>
        <resource type="download" name="confirmation-dialog.js" location="js/views/confirmation-dialog.js"/>
        <resource type="download" name="dvcs-accounts-page-init.js" location="js/ui/dvcs-accounts-page-init.js"/>
        <!-- -->

        <resource type="download" name="css/styles.css" location="css/styles.less"/>
        <resource type="download" name="bitbucket-admin-approval.css"
                  location="css/bitbucket-admin-approval.less"/>
        <resource type="download" name="icon.png" location="images/gh_icon.png"/>

        <resource type="download" name="js/admin/dvcs-connector-admin-init.js"
                  location="js/admin/dvcs-connector-admin-init.js"/>

        <resource type="helpPathsAdmin" location="help-admin" name="help-admin"/>

        <transformation extension="less">
            <transformer key="lessTransformer"/>
        </transformation>

        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>

    </web-resource>

    <web-resource name="cssResources" key="cssResources">
        <resource type="download" name="css/styles.css" location="css/styles.less"/>

        <transformation extension="less">
            <transformer key="lessTransformer"/>
        </transformation>

        <context>com.atlassian.jira.plugins.jira-bitbucket-connector-plugin-css</context>
    </web-resource>

    <web-resource name="adminCssResources" key="adminCssResources">
        <resource type="download" name="css/admin.css" location="css/admin.css"/>
        <context>atl.admin</context>
    </web-resource>

    <web-resource key="soy-templates-admin-">
        <context>atl.admin</context>
        <dependency>com.atlassian.soy.soy-template-plugin:soy-deps</dependency>

        <transformation extension="soy">
            <transformer key="soyTransformer"/>
        </transformation>
    </web-resource>

    <web-resource key="soy-templates-bitbucket">
        <transformation extension="soy">
            <transformer key="soyTransformer"/>
        </transformation>

        <resource type="soy" name="bb-post-install-approval.soy" location="soy/bb-post-install-approval.soy"/>
        <resource type="soy" name="bb-approval.soy" location="soy/bb-approval.soy"/>
    </web-resource>

    <web-resource key="soy-templates-plugin-context">
        <context>com.atlassian.jira.plugins.jira-bitbucket-connector-plugin</context>
        <dependency>com.atlassian.soy.soy-template-plugin:soy-deps</dependency>

        <transformation extension="soy">
            <transformer key="soyTransformer"/>
        </transformation>

        <resource type="download" name="jira-dvcs-connector-plugin.soy.js"
                  location="soy/jira-dvcs-connector-plugin.soy"/>

        <resource type="download" name="dvcs-accounts.soy.js" location="soy/dvcs-accounts.soy"/>

        <resource type="soy" name="bb-admin-approval.soy" location="soy/bb-admin-approval.soy"/>
        <resource type="soy" name="bb-approval.soy" location="soy/bb-approval.soy"/>
        <resource type="download" name="bb-approval.soy.js" location="soy/bb-approval.soy"/>
        <resource type="download" name="bb-admin-approval.soy.js" location="soy/bb-admin-approval.soy"/>
    </web-resource>

    <web-resource key="unlicensed-page-resources">
        <resource type="download" name="dvcs-unlicensed.css" location="css/unlicensed.css"/>
    </web-resource>

    <rest key="bitbucket-rest-resources" path="/bitbucket" version="1.0">
        <package>com.atlassian.jira.plugins.dvcs.rest.external.v1</package>
        <description>Public REST Resources for the v1.0 API</description>
    </rest>

    <rest key="bitbucket-internal-rest-resources" path="/dvcs-connector-internal" version="1.0">
        <package>com.atlassian.jira.plugins.dvcs.rest.internal.v1</package>
        <description>Internal REST Resources</description>
    </rest>

    <resource type="download" name="images/" location="images/"/>
    <resource type="i18n" name="bitbucket-i18n" location="i18n"/>
    <resource type="i18n" name="dvcs-connector-props" location="dvcsconnector"/>

    <activity-streams-provider key="dvcs-streams-provider" name="Dvcs Streams Provider"
                               i18n-name-key="Bitbucket Connector"
                               class="com.atlassian.jira.plugins.dvcs.streams.DvcsStreamsActivityProvider">
    </activity-streams-provider>

    <!-- Web Panels -->
    <web-panel key="add-user-bitbucket-access-extension" location="webpanels.admin.adduser.applicationaccess">
        <conditions type="AND">
            <condition class="com.atlassian.jira.software.api.conditions.SoftwareGlobalAdminCondition"/>
            <condition
                    class="com.atlassian.jira.plugins.dvcs.bitbucket.access.conditions.BitbucketAccessExtensionCondition"/>
        </conditions>
        <context-provider
                class="com.atlassian.jira.plugins.dvcs.bitbucket.access.AddUserBitbucketAccessExtensionContextProvider"/>
        <resource name="view" type="velocity" location="/templates/dvcs/add-user-bitbucket-access-extension.vm"/>
    </web-panel>

    <web-resource key="add-user-bitbucket-access-components">
        <resource type="download" name="dvcs-bitbucket-access.js" location="javascripts/bitbucket-access.js"/>
        <resource type="download" name="dvcs-add-user-form.js" location="javascripts/add-user-form.js"/>
        <resource type="download" name="dvcs-bitbucket-access-controller.js"
                  location="javascripts/bitbucket-access-controller.js"/>
    </web-resource>
    <resource type="qunit" name="javascripts/bitbucket-access-test.js" location="javascripts/bitbucket-access-test.js"/>
    <resource type="qunit" name="javascripts/add-user-form-test.js" location="javascripts/add-user-form-test.js"/>
    <resource type="qunit" name="javascripts/bitbucket-access-controller-test.js"
              location="javascripts/bitbucket-access-controller-test.js"/>

    <web-resource key="add-user-bitbucket-access-extension-resources">
        <dependency>com.atlassian.jira.plugins.jira-bitbucket-connector-plugin:add-user-bitbucket-access-components
        </dependency>

        <resource type="download" name="add-user-bitbucket-access-extension.css"
                  location="css/add-user-bitbucket-access-extension.css"/>
        <resource type="download" name="add-user-bitbucket-access-extension.js"
                  location="javascripts/add-user-bitbucket-access-extension.js"/>
    </web-resource>

    <web-panel key="app-role-defaults-bitbucket-invite-message-panel" weight="2000"
               location="webpanels.admin.defaultapp.selector">
        <conditions type="AND">
            <condition class="com.atlassian.jira.software.api.conditions.SoftwareGlobalAdminCondition"/>
            <condition
                    class="com.atlassian.jira.plugins.dvcs.bitbucket.access.conditions.BitbucketAccessExtensionCondition"/>
        </conditions>
        <context-provider
                class="com.atlassian.jira.plugins.dvcs.bitbucket.access.BitbucketInviteMessagePanelContextProvider"/>
        <resource name="view" type="velocity" location="/templates/dvcs/bitbucket-invite-message-panel.vm"/>
    </web-panel>

    <web-resource key="bitbucket-invite-message-panel-component">
        <resource type="download" name="bitbucket-invite-message-panel-view.js"
                  location="javascripts/bitbucket-invite-message-panel-view.js"/>
    </web-resource>
    <resource type="qunit" name="javascripts/bitbucket-invite-message-panel-view-test.js"
              location="javascripts/bitbucket-invite-message-panel-view-test.js"/>

    <web-resource key="bitbucket-invite-message-panel-resources">
        <dependency>
            com.atlassian.jira.plugins.jira-bitbucket-connector-plugin:bitbucket-invite-message-panel-component
        </dependency>

        <transformation extension="less">
            <transformer key="lessTransformer"/>
        </transformation>

        <resource type="download" name="bitbucket-invite-message-panel.css"
                  location="css/bitbucket-invite-message-panel.less"/>
        <resource type="download" name="bitbucket-invite-message-panel-app-role-defaults.css"
                  location="css/bitbucket-invite-message-panel-app-role-defaults.less"/>
        <resource type="download" name="bitbucket-invite-message-panel.js"
                  location="javascripts/bitbucket-invite-message-panel.js"/>
    </web-resource>

    <web-panel key="jim-user-access-bitbucket-invite-message-panel" weight="2000"
               location="jim.webpanels.after.user.access.picker">
        <conditions type="AND">
            <condition class="com.atlassian.jira.software.api.conditions.SoftwareGlobalAdminCondition"/>
            <condition
                    class="com.atlassian.jira.plugins.dvcs.bitbucket.access.conditions.BitbucketAccessExtensionCondition"/>
        </conditions>
        <context-provider
                class="com.atlassian.jira.plugins.dvcs.bitbucket.access.JIMBitbucketInviteMessagePanelContextProvider"/>
        <resource name="view" type="velocity" location="/templates/dvcs/bitbucket-invite-message-panel.vm"/>
    </web-panel>

    <web-resource key="jim-bitbucket-invite-message-panel-resources">
        <dependency>
            com.atlassian.jira.plugins.jira-bitbucket-connector-plugin:bitbucket-invite-message-panel-component
        </dependency>

        <transformation extension="less">
            <transformer key="lessTransformer"/>
        </transformation>

        <resource type="download" name="bitbucket-invite-message-panel.css"
                  location="css/bitbucket-invite-message-panel.less"/>
        <resource type="download" name="bitbucket-invite-message-panel-jim-user-access.css"
                  location="css/bitbucket-invite-message-panel-jim-user-access.less"/>
        <resource type="download" name="bitbucket-invite-message-panel.js"
                  location="javascripts/jim-bitbucket-invite-message-panel.js"/>
    </web-resource>

    <soy-function key="help-url-soy-function" class="com.atlassian.jira.plugins.dvcs.soy.impl.functions.HelpUrlSoyFunction" />

</atlassian-plugin>
