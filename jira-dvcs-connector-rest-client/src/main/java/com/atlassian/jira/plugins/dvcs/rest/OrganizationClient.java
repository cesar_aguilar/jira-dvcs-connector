package com.atlassian.jira.plugins.dvcs.rest;

import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.ClientResponse;

import javax.annotation.Nonnull;
import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.Optional;

import static com.sun.jersey.api.client.ClientResponse.Status.OK;
import static com.sun.jersey.api.client.ClientResponse.Status.fromStatusCode;

public class OrganizationClient extends DvcsRestClient<OrganizationClient> {

    // The resource requires an orgId value of zero to indicate that it does not yet exist.
    private static final int NON_EXISTING_ORG_ID = 0;

    public OrganizationClient(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * Deletes all Organizations, including the integrated account if any.
     */
    public void deleteAll() {
        createResource().path("organization").delete();
    }

    /**
     * Adds an Organization (account) to those managed by the DVCS Connector.
     */
    @Nonnull
    public Organization addOrganizationForApprovedAccount(@Nonnull final String accountName,
                                                          @Nonnull final String dvcsType,
                                                          @Nonnull final String oauthKey,
                                                          @Nonnull final String oauthSecret,
                                                          @Nonnull final String hostUrl,
                                                          final boolean autosync,
                                                          final boolean smartcommitsOnNewRepos) {

        Organization organization = new Organization(NON_EXISTING_ORG_ID, hostUrl, accountName,
                dvcsType, autosync, CredentialFactory.create2LOCredential(oauthKey, oauthSecret), null,
                smartcommitsOnNewRepos, Collections.emptySet());

        organization = createResource().path("organization").type(MediaType.APPLICATION_JSON_TYPE).post(Organization.class, organization);

        final ClientResponse response = createResource()
                .path("organization")
                .path("" + organization.getId())
                .path("credential")
                .type(MediaType.APPLICATION_JSON_TYPE)
                .post(ClientResponse.class, ImmutableMap.of("key", oauthKey, "secret", oauthSecret));

        switch (fromStatusCode(response.getStatus())) {
            case OK:
                return organization;
            default:
                throw new IllegalStateException("Unexpected response status actual: " + response.getStatus()
                        + ", expected: " + OK.getStatusCode());
        }
    }

    /**
     * Returns the Organization with the given type and name.
     *
     * @param type the account type
     * @param name the account name
     * @return empty if it doesn't exist, otherwise an Organization populated with: id, hostUrl, name, dvcsType,
     * autolinkNewRepos, smartcommitsOnNewRepos, organizationUrl, and approvalState
     */
    public Optional<Organization> findOrganization(@Nonnull final String type, @Nonnull final String name) {
        final ClientResponse response = createResource()
                .path("organization")
                .path("find")
                .queryParam("name", name)
                .queryParam("type", type)
                .get(ClientResponse.class);
        switch (response.getClientResponseStatus()) {
            case OK:
                return Optional.of(response.getEntity(Organization.class));
            case NOT_FOUND:
                return Optional.empty();
            case UNAUTHORIZED:
                throw new IllegalStateException("Need to be logged in");
            case FORBIDDEN:
                throw new IllegalStateException("Need to be logged in as admin");
            default:
                throw new IllegalStateException("Unexpected status " + response.getClientResponseStatus());
        }
    }
}
