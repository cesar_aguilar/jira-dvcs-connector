package com.atlassian.jira.plugins.dvcs.pageobjects.component;

import com.atlassian.jira.plugins.dvcs.pageobjects.page.RepositoriesPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.AccountControlsDialog;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.MultiSelector;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.pageobjects.elements.timeout.TimeoutType.DIALOG_LOAD;
import static com.atlassian.pageobjects.elements.timeout.TimeoutType.PAGE_LOAD;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

/**
 * @deprecated see deprecation note on {@link RepositoriesPage}
 */
@Deprecated
@SuppressWarnings("deprecation")
public class OrganizationDiv {
    private static final String DYNAMIC_REPOSITORIES_PREFIX = "it.restart";
    private final PageElement controlsButton;
    private final PageElement organizationName;
    private final PageElement organizationType;
    private final PageElement repositoriesTable;
    private final PageElement rootElement;

    @Inject
    private PageBinder pageBinder;
    @Inject
    private PageElementFinder elementFinder;
    private MultiSelector multiSelector;

    public OrganizationDiv(final PageElement row) {
        this.rootElement = row;
        this.repositoriesTable = rootElement.find(By.tagName("table"));
        this.organizationType = rootElement.find(By.cssSelector("div h4"));
        this.organizationName = rootElement.find(By.className("h4-text"));
        this.controlsButton = rootElement.find(By.xpath(".//button[contains(concat(' ', @class, ' '), ' aui-dropdown2-trigger ')]"));
    }

    /**
     * Deletes this repository from the list
     */
    public void delete() {
        // add marker to wait for post complete
        final PageElement ddButton = rootElement.find(By.className("aui-dropdown2-trigger"));
        ddButton.click();
        final String dropDownMenuId = ddButton.getAttribute("aria-owns");
        final PageElement deleteLink = elementFinder.find(By.id(dropDownMenuId)).find(By.className("dvcs-control-delete-org"));
        deleteLink.click();

        final ConfirmationDialog dialog = elementFinder.find(By.id("confirm-dialog"), ConfirmationDialog.class, DIALOG_LOAD);
        dialog.confirm();
        dialog.waitUntilVisible();
    }

    public List<RepositoryDiv> getRepositories() {
        return getRepositories(false);
    }

    public List<RepositoryDiv> getRepositories(final boolean filterDynamicRepositories) {
        if (!repositoriesTable.isPresent()) {
            return emptyList();
        }
        return repositoriesTable.findAll(By.xpath("//table/tbody/tr[contains(concat(@class, ' '), 'dvcs-repo-row')]"))
                .stream()
                .map(tr -> pageBinder.bind(RepositoryDiv.class, tr))
                .filter(repo -> !filterDynamicRepositories || !repo.getRepositoryName().startsWith(DYNAMIC_REPOSITORIES_PREFIX))
                .collect(toList());
    }

    /**
     * Returns the ID of the repository with the given name.
     *
     * @param name the name for which to look, case-sensitive
     * @return none if there is no such repo
     */
    @Nonnull
    public Optional<String> findRepositoryId(@Nonnull final String name) {
        return getRepositories().stream()
                .filter(repo -> name.equals(repo.getRepositoryName()))
                .map(RepositoryDiv::getRepositoryId)
                .findFirst();
    }

    public String getOrganizationType() {
        // <h4 class="aui bitbucketLogo">
        return organizationType.getAttribute("class").replaceAll(".*aui (.*)Logo.*", "$1");
    }

    public String getOrganizationName() {
        return organizationName.getText();
    }

    public void refresh() {
        controlsButton.click();
        findControlDialog().refresh();
        // wait for popup to show up
        try {
            waitUntilTrue(elementFinder.find(By.id("refreshing-account-dialog")).timed().isVisible());
        } catch (AssertionError e) {
            // ignore, the refresh was probably very quick and the popup has been already closed.
        }
        waitUntilFalse(elementFinder.find(By.id("refreshing-account-dialog")).withTimeout(PAGE_LOAD).timed().isVisible());
    }

    private AccountControlsDialog findControlDialog() {
        final String dropDownMenuId = controlsButton.getAttribute("aria-owns");
        return elementFinder.find(By.id(dropDownMenuId), AccountControlsDialog.class);
    }

    public void sync() {
        enableAllRepos();
        // click sync icon
        for (final RepositoryDiv repoDiv : getRepositories()) {
            repoDiv.sync();
        }
    }

    public void enableAllRepos() {
        getMultiSelector().getUnSyncedRepos().stream().forEach(repo -> multiSelector.enableRepo(repo));
    }

    private MultiSelector getMultiSelector() {
        if (multiSelector == null) {
            multiSelector = pageBinder.bind(MultiSelector.class, this.rootElement.find(By.cssSelector("form")),
                    this.repositoriesTable);
        }
        return multiSelector;
    }

    public RepositoryDiv enableRepoAndGetRepoDiv(final String repoName) {
        getMultiSelector().enableRepoByName(repoName);
        final PageElement row = rootElement.find(By.cssSelector("tr[data-name=\"" + repoName +"\"]"));
        waitUntilTrue("repo row should be added", row.timed().isVisible());
        return pageBinder.bind(RepositoryDiv.class, row);
    }
}
