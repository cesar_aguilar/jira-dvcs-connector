package com.atlassian.jira.plugins.dvcs.pageobjects.page;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Represents the page to link repositories to projects
 */
public class GithubConfigureOrganizationsPage extends BaseConfigureOrganizationsPage {
    @ElementBy(id = "login_field")
    private PageElement githubWebLoginField;

    @ElementBy(id = "password")
    private PageElement githubWebPasswordField;

    @ElementBy(name = "authorize")
    private PageElement githubWebAuthorizeButton;

    @ElementBy(name = "commit")
    private PageElement githubWebSubmitButton;

    @ElementBy(id = "oauthClientId")
    private PageElement oauthClientId;

    @ElementBy(id = "oauthSecret")
    private PageElement oauthSecret;


    @Override
    public GithubConfigureOrganizationsPage addOrganizationSuccessfully(final String organizationAccount,
                                                                        final OAuthCredentials oAuthCredentials, final boolean autoSync, final String username, final String password) {
        getLinkGithubAccountButton().click();
        waitFormBecomeVisible();

        selectDvcsType("github");

        organization.clear().type(organizationAccount);
        oauthClientId.clear().type(oAuthCredentials.key);
        oauthSecret.clear().type(oAuthCredentials.secret);

        setPageAsOld();
        if (!autoSync) {
            autoLinkNewRepos.click();
        }

        addOrgButton.click();
        checkAndDoGithubLogin(username, password);

        String currentUrl = authorizeGithubAppIfRequired();
        if (!currentUrl.contains("jira")) {
            throw new AssertionError("Expected was Valid OAuth login and redirect to JIRA!");
        }

        waitUntilTrue(getLinkGithubAccountButton().timed().isPresent());

        if (autoSync) {
            JiraPageUtils.checkSyncProcessSuccess(pageBinder);
        }

        return this;
    }

    private String checkAndDoGithubLogin(final String username, final String password) {
        waitWhileNewPageLaoded();

        String currentUrl = jiraTestedProduct.getTester().getDriver().getCurrentUrl();
        if (currentUrl.contains("https://github.com/login?")) {
            githubWebLoginField.type(username);
            githubWebPasswordField.type(password);
            setPageAsOld();
            githubWebSubmitButton.click();
        }
        return jiraTestedProduct.getTester().getDriver().getCurrentUrl();
    }

    protected void waitWhileNewPageLaoded() {
        jiraTestedProduct.getTester().getDriver().waitUntilElementIsNotLocated(By.id("old-page"));
    }

    protected void setPageAsOld() {
        final String script =
                "var bodyElm = document.getElementsByTagName('body')[0];" +
                        "var oldPageHiddenElm = document.createElement('input');" +
                        "oldPageHiddenElm.setAttribute('id','old-page');" +
                        "oldPageHiddenElm.setAttribute('type','hidden');" +
                        "bodyElm.appendChild(oldPageHiddenElm);";
        jiraTestedProduct.getTester().getDriver().executeScript(script);
    }

    private String authorizeGithubAppIfRequired() {
        waitWhileNewPageLaoded();

        String currentUrl = jiraTestedProduct.getTester().getDriver().getCurrentUrl();
        if (currentUrl.contains("/github.com/login/oauth")) {
            githubWebAuthorizeButton.click();
        }
        return jiraTestedProduct.getTester().getDriver().getCurrentUrl();
    }
}
