package com.atlassian.jira.plugins.dvcs.spi.bitbucket.message;

import com.atlassian.jira.plugins.dvcs.model.Progress;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.message.BaseProgressEnabledMessage;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints.PullRequestRemoteRestpoint.FIRST_PAGE;

/*
    Message for synchronizing pull requests
 */
public class BitbucketSynchronizeActivityMessage extends BaseProgressEnabledMessage implements Serializable {
    private static final long serialVersionUID = -4361088769277502144L;

    private Set<Integer> processedPullRequests;
    private Set<Integer> processedPullRequestsLocal;
    private Date lastSyncDate;
    @Deprecated private int pageNum = 0;
    private String pageUrl = null;

    @Deprecated
    public BitbucketSynchronizeActivityMessage(Repository repository,
                                               Progress progress,
                                               boolean softSync,
                                               int pageNum,
                                               Set<Integer> processedPullRequests,
                                               Set<Integer> processedPullRequestsLocal,
                                               Date lastSyncDate,
                                               int syncAuditId,
                                               boolean webHookSync) {
        super(progress, syncAuditId, softSync, repository, webHookSync);
        this.pageNum = pageNum;
        this.processedPullRequests = processedPullRequests;
        this.processedPullRequestsLocal = processedPullRequestsLocal;
        this.lastSyncDate = lastSyncDate;
    }

    public BitbucketSynchronizeActivityMessage(Repository repository,
                                               Progress progress,
                                               boolean softSync,
                                               @Nonnull String pageUrl,
                                               Set<Integer> processedPullRequests,
                                               Set<Integer> processedPullRequestsLocal,
                                               Date lastSyncDate,
                                               int syncAuditId,
                                               boolean webHookSync) {
        super(progress, syncAuditId, softSync, repository, webHookSync);
        this.pageUrl = pageUrl;
        this.processedPullRequests = processedPullRequests;
        this.processedPullRequestsLocal = processedPullRequestsLocal;
        this.lastSyncDate = lastSyncDate;
    }


    public BitbucketSynchronizeActivityMessage(Repository repository, boolean softSync, Date lastSyncDate, int syncAuditId, boolean webHookSync) {
        this(repository, null, softSync, FIRST_PAGE, new HashSet<Integer>(), new HashSet<Integer>(), lastSyncDate, syncAuditId, webHookSync);
    }

    /**
     * @return set of pull requests that were processed during synchronization
     */
    public Set<Integer> getProcessedPullRequests() {
        return processedPullRequests;
    }

    @Deprecated
    public int getPageNum() {
        return pageNum;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    /**
     * @return set of pull requests for which commits were updated
     */
    public Set<Integer> getProcessedPullRequestsLocal() {
        return processedPullRequestsLocal;
    }

    public Date getLastSyncDate() {
        return lastSyncDate;
    }
}
